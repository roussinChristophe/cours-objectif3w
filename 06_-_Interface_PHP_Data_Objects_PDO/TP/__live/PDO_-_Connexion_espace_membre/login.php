<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Administration</title>
    </head>
    <body>
        <?php
        if(isset($_GET['_err'])) {
            switch($_GET['_err']) {
                case '403':
                    echo '<p>Vous devez vous connecter !</p>';
                    break;
                case 'empty':
                    if(isset($_GET['field'])) {
                        switch($_GET['field']) {
                            case 'login':
                                echo '<p>Vous devez saisir un identifiant !</p>';
                                break;
                            case 'pwd':
                                echo '<p>Vous devez saisir un mot de passe !</p>';
                                break;
                            default:
                                echo '<p>Vous devez saisir tous les champs !</p>';
                        }
                    }
                    break;
                case 'connect':
                    echo '<p>Mauvais identifiant/mot de passe !</p>';
                    break;
                case 'hack':
                    echo 'Nope nope nope !!! Ca ne fonctionne pas !!!';
                    break;
            }
        }
        ?>
        <form action="admin/" method="post">
            <input type="text" name="login" aria-label="Identifiant" placeholder="Identifiant"<?php if(isset($_GET['login'])) echo ' value="'. $_GET['login'] .'"'; ?>>
            <input type="password" name="password" aria-label="Mot de passe" placeholder="Mot de passe">
            <button type="submit">Se connecter</button>
        </form>
    </body>
</html>