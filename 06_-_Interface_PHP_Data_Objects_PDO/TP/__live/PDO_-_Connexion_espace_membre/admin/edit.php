<?php
session_start();
require('db.conf');
require('app.conf');
require('functions.php');


redirectNotAllowed($_SESSION[APP_TAG]['connected'], 4);

$dsn = DB_ENGINE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET;

try {
    $db = new PDO($dsn, DB_USER, DB_PWD, array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));

    if(($requete = $db->query('SELECT `id`,`login`,`user`.`role` FROM `user`'))!==false) {
        $users = $requete->fetchAll(PDO::FETCH_ASSOC);
        $requete->closeCursor();
    }
} catch(PDOException $e) {
    die($e->getMessage());
}
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Éditer un utilisateur</title>
    </head>
    <body>
        <header>
            <h1>Éditer un utilisateur</h1>
        </header>

        <table border="1">
            <thead>
                <tr>
                    <th>Login</th>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($users as $user) {
                ?>
                <tr>
                    <td><?php echo $user['login']; ?></td>
                    <td>
                        <?php
                        if(hasPower($db, $user['role'], $_SESSION[APP_TAG]['connected']['pouvoir'])) {
                        ?>
                        <a href="modif.php?user=<?php echo $user['id']; ?>">Modifier</a>
                        <a href="delete.php?user=<?php echo $user['id']; ?>">Supprimer</a>
                        <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </body>
</html>