<?php
/**
 * hasPower Checks if user has power to use this role
 *
 * @param PDO $db
 * @param integer $role
 * @param integer $userPower
 * @return boolean
 */
function hasPower(PDO $db, int $role, int $userPower): bool {
    if(($requete = $db->prepare('SELECT `pouvoir` FROM `role` WHERE `id`=?'))!==false) {
        if($requete->bindValue(1, $role)) {
            if($requete->execute()) {
                if(($reponse = $requete->fetch(PDO::FETCH_ASSOC))!==false) {
                    if($reponse['pouvoir']>=$userPower) {
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

/**
 * redirectNotAllowed
 *
 * @param array $user
 * @param integer $auth
 * @return void
 */
function redirectNotAllowed(array $user, int $auth): void {
    if(empty($user)) {
        header('Location:../login.php?_err=403');
        exit;
    }
    
    if(!($user['role']==1 || in_array($auth, $user['autorisations']))) {
        header('Location:./dashboard.php?_err=403');
        exit;
    }
}

/**
 * getRole
 *
 * @param PDO $db
 * @param integer $user
 * @return integer
 */
function getRole(PDO $db, int $user): int {
    if(($requete = $db->prepare('SELECT `role` FROM `user` WHERE `id`=?'))!==false) {
        if($requete->bindValue(1, $user)) {
            if($requete->execute()) {
                if(($reponse = $requete->fetch(PDO::FETCH_ASSOC))!==false) {
                    return $reponse['role'];
                }
            }
        }
    }

    return 0;
}