<?php
session_start();
require('db.conf');
require('app.conf');
require('functions.php');

redirectNotAllowed($_SESSION[APP_TAG]['connected'], 1);

$dsn = DB_ENGINE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET;

try {
    $db = new PDO($dsn, DB_USER, DB_PWD, array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));

    if(($request = $db->query('SELECT * FROM `role` ORDER BY `pouvoir` ASC'))!==false) {
        $roles = $request->fetchAll(PDO::FETCH_ASSOC);
    }

    if(!empty($_POST['login']) && !empty($_POST['password']) && !empty($_POST['role'])) {
        if(ctype_digit($_POST['role'])) { 
            if(hasPower($db, (int)$_POST['role'], $_SESSION[APP_TAG]['connected']['pouvoir'])) {
                if(($requete = $db->prepare('INSERT INTO `user` (`login`, `password`, `role`) VALUES (?, ?, ?)'))!==false) {
                    if($requete->bindValue(1, $_POST['login']) && $requete->bindValue(2, $_POST['password']) && $requete->bindValue(3, $_POST['role'])) {
                        if($requete->execute()) {
                            $msg = 'Utilisateur ajouté !';
                        } else {
                            $msg = 'Erreur durant la création !';
                        }
                        $requete->closeCursor();
                    }
                }
            } else {
                $msg = 'Au revoir Président !!!';
            }
        }
    }
} catch(PDOException $e) {
    die($e->getMessage());
}
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ajouter un utilisateur</title>
    </head>
    <body>
        <header>
            <h1>Ajouter un utilisateur</h1>
        </header>

        <?php
        if(isset($msg)) {
            echo '<p>'. $msg .'</p>';
        }
        ?>
        <form action="" method="post">
            <input type="text" name="login" id="">
            <input type="password" name="password" id="">
            <?php if(isset($roles)) { ?>
            <select name="role" id="">
                <?php
                foreach($roles as $role) {
                    if($role['pouvoir']>=$_SESSION[APP_TAG]['connected']['pouvoir']) {
                ?>
                <option value="<?php echo $role['id']; ?>"><?php echo $role['libelle']; ?></option>
                <?php
                    }    
                }
                ?>
            </select>
            <?php } ?>
            <button type="submit">Ajouter</button>
        </form>
    </body>
</html>