<?php
session_start();
require('db.conf');
require('app.conf');

if(isset($_GET['deconnect'])) {
    unset($_SESSION[APP_TAG]['connected']);
    header('Location:../login.php');
    exit;
}

if(!isset($_POST['login'], $_POST['password'])) {
    header('Location:../login.php?_err=403');
    exit;
}
if(empty($_POST['login']) || empty($_POST['password'])) {
    $fields = 'all';
    if(!empty($_POST['login'])) {
        $fields = 'pwd';
    }
    if(!empty($_POST['password'])) {
        $fields = 'login';
    }
    header('Location:../login.php?_err=empty&field=' . $fields . '&login=' . $_POST['login']);
    exit;
}



$dsn = DB_ENGINE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET;

try {
    $db = new PDO($dsn, DB_USER, DB_PWD, array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));

    if(($request = $db->prepare('SELECT `user`.`id`,`login`,`user`.`role`,`pouvoir`,GROUP_CONCAT(`autorisation`) AS `autorisations`
    FROM `user`
    LEFT JOIN `role` ON `user`.`role`=`role`.`id`
    LEFT JOIN `posseder` ON `role`.`id`=`posseder`.`role`
    WHERE `login`=? AND `password`=?'))!==false) {
        // if($request->execute(array(
        //     1=>$_POST['login'],
        //     2=>$_POST['password']
        // ))) {
        if($request->bindValue(1, $_POST['login']) && $request->bindValue(2, $_POST['password'])) {
            if($request->execute()) {
                if(($reponse = $request->fetch(PDO::FETCH_ASSOC))!==false) {
                    $reponse['autorisations'] = explode(',',$reponse['autorisations']);
                    $_SESSION[APP_TAG]['connected'] = $reponse;

                    $request->closeCursor();
                } else {
                    $request->closeCursor();

                    header('Location:../login.php?_err=connect&login=' . $_POST['login']);
                    exit;
                }
            }
        }
    }
} catch(PDOException $e) {
    die($e->getMessage());
}


if(empty($_SESSION[APP_TAG]['connected'])) {
    header('Location:../login.php?_err=hack');
    exit;
}

header('Location:dashboard.php');