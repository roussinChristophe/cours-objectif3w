<?php
session_start();
require('db.conf');
require('app.conf');
require('functions.php');

redirectNotAllowed($_SESSION[APP_TAG]['connected'], 3);

if(empty($_GET['user'])) {
    header('Location:dashboard.php?_err=403');
    exit;
}

$dsn = DB_ENGINE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET;

try {
    $db = new PDO($dsn, DB_USER, DB_PWD, array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));

    if(!empty($_POST['login']) && !empty($_POST['role'])) {
        if(ctype_digit($_POST['role'])) { 
            if(hasPower($db, (int)$_POST['role'], $_SESSION[APP_TAG]['connected']['pouvoir'])) {
                if(($requete = $db->prepare('UPDATE `user` SET `login`=:login,' . (!empty($_POST['password']) ? ' `password`=:pwd,' : '') . ' `role`=:role WHERE `id`=:id'))!==false) {
                    if($requete->bindValue('login', $_POST['login']) && $requete->bindValue('role', $_POST['role']) && $requete->bindValue('id', $_GET['user'])) {
                        $ctrl = true;
                        if(!empty($_POST['password'])) {
                            $ctrl = $requete->bindValue('pwd', $_POST['password']);
                        }
                        if($ctrl) {
                            if($requete->execute()) {
                                $msg = 'Utilisateur modifié !';
                            } else {
                                $msg = 'Erreur durant la modification !';
                            }
                        } else {
                            $msg = 'Erreur durant la modification !';
                        }
                    }
                }
            } else {
                $msg = 'Au revoir Président !!!';
            }
        }
    }

    if(($request = $db->query('SELECT * FROM `role` ORDER BY `pouvoir` ASC'))!==false) {
        $roles = $request->fetchAll(PDO::FETCH_ASSOC);
        $request->closeCursor();
    }

    if(($requete = $db->prepare('SELECT `id`,`login`,`role` FROM `user` WHERE `id`=?'))!==false) {
        if($requete->bindValue(1, $_GET['user'])) {
            if($requete->execute()) {
                $user = $requete->fetch(PDO::FETCH_ASSOC);

                $requete->closeCursor();
                if($user===false) {
                    header('Location:edit.php?_err=403');
                    exit;
                }
            }
        }
    }
} catch(PDOException $e) {
    die($e->getMessage());
}
?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Modifier un utilisateur</title>
    </head>
    <body>
        <header>
            <h1>Modifier un utilisateur</h1>
        </header>

        <?php
        if(isset($msg)) {
            echo '<p>'. $msg .'</p>';
        }
        ?>
        <form action="" method="post">
            <input type="text" name="login" value="<?php echo $user['login']; ?>">
            <input type="password" name="password" placeholder="Si aucun changement, laissez vide">
            <?php if(isset($roles)) { ?>
            <select name="role">
                <?php
                foreach($roles as $role) {
                    if($role['pouvoir']>=$_SESSION[APP_TAG]['connected']['pouvoir']) {
                        if($user['role']==$role['id']) {
                ?>
                <option selected value="<?php echo $role['id']; ?>"><?php echo $role['libelle']; ?></option>
                <?php
                        } else {
                ?>
                <option value="<?php echo $role['id']; ?>"><?php echo $role['libelle']; ?></option>
                <?php
                        }
                    }    
                }
                ?>
            </select>
            <?php } ?>
            <button type="submit">Modifier</button>
        </form>
    </body>
</html>