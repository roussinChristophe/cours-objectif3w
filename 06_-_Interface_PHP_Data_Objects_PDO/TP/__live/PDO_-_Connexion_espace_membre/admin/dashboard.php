<?php
session_start();
require('app.conf');

if(empty($_SESSION[APP_TAG]['connected'])) {
    header('Location:../login.php?_err=403');
    exit;
}

var_dump($_SESSION[APP_TAG]['connected']);

?><!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Administration</title>
    </head>
    <body>
        <header>
            <h1>Bonjour <?php echo $_SESSION[APP_TAG]['connected']['login']; ?></h1>
            <a href=".?deconnect">Se déconnecter</a>
        </header>
        <nav>
            <ul>
                <?php
                if($_SESSION[APP_TAG]['connected']['role']==1 || in_array(1, $_SESSION[APP_TAG]['connected']['autorisations'])) {
                ?>
                <li><a href="ajout.php" title="Ajouter un utilisateur">Ajouter un utilisateur</a></li>
                <?php
                }
                ?>
                <?php
                if($_SESSION[APP_TAG]['connected']['role']==1 || in_array(4, $_SESSION[APP_TAG]['connected']['autorisations'])) {
                ?>
                <li><a href="edit.php" title="Éditer un utilisateur">Éditer un utilisateur</a></li>
                <?php
                }
                ?>
            </ul>
        </nav>
        <?php
        if(isset($_GET['_err'])) {
            switch($_GET['_err']) {
                case '403':
                    echo '<p>Vous devez vous connecter !</p>';
                    break;
                case 'empty':
                    if(isset($_GET['field'])) {
                        switch($_GET['field']) {
                            case 'login':
                                echo '<p>Vous devez saisir un identifiant !</p>';
                                break;
                            case 'pwd':
                                echo '<p>Vous devez saisir un mot de passe !</p>';
                                break;
                            default:
                                echo '<p>Vous devez saisir tous les champs !</p>';
                        }
                    }
                    break;
                case 'connect':
                    echo '<p>Mauvais identifiant/mot de passe !</p>';
                    break;
                case 'hack':
                    echo 'Nope nope nope !!! Ca ne fonctionne pas !!!';
                    break;
            }
        }
        ?>
    </body>
</html>