<?php
session_start();
require('db.conf');
require('app.conf');
require('functions.php');


redirectNotAllowed($_SESSION[APP_TAG]['connected'], 2);

if(empty($_GET['user'])) {
    header('Location:dashboard.php?_err=403');
    exit;
}

$dsn = DB_ENGINE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET;

try {
    $db = new PDO($dsn, DB_USER, DB_PWD, array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
    
    if(hasPower($db, getRole($db, $_GET['user']), $_SESSION[APP_TAG]['connected']['pouvoir'])) {
        if(($requete = $db->prepare('DELETE FROM `user` WHERE `id`=?'))!==false) {
            if($requete->bindValue(1, $_GET['user'])) {
                if($requete->execute()) {
                    $requete->closeCursor();
                    header('Location:edit.php?_success=delete');
                    exit;
                }
            }
            $requete->closeCursor();
        }
    } else {
        header('Location:edit.php?_err=403');
        exit;
    }
} catch(PDOException $e) {
    die($e->getMessage());
}