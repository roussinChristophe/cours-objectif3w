#CREATE DATABASE IF NOT EXISTS `espace_administration` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `espace_administration`;

DROP TABLE IF EXISTS `capability`;
CREATE TABLE IF NOT EXISTS `capability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lbl` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `capability`;
INSERT INTO `capability` (`id`, `lbl`) VALUES(1, 'Lister');
INSERT INTO `capability` (`id`, `lbl`) VALUES(2, 'Ajouter');
INSERT INTO `capability` (`id`, `lbl`) VALUES(3, 'Editer');
INSERT INTO `capability` (`id`, `lbl`) VALUES(4, 'Modifier');
INSERT INTO `capability` (`id`, `lbl`) VALUES(5, 'Supprimer');
INSERT INTO `capability` (`id`, `lbl`) VALUES(6, 'Publier');

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lbl` varchar(50) NOT NULL,
  `power` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `role`;
INSERT INTO `role` (`id`, `lbl`, `power`) VALUES(1, 'Super Admin', 1);
INSERT INTO `role` (`id`, `lbl`, `power`) VALUES(2, 'Admin', 10);
INSERT INTO `role` (`id`, `lbl`, `power`) VALUES(3, 'Invité', 100);
INSERT INTO `role` (`id`, `lbl`, `power`) VALUES(4, 'Éditeur', 50);

DROP TABLE IF EXISTS `rel_role_capability`;
CREATE TABLE IF NOT EXISTS `rel_role_capability` (
  `role` int(11) NOT NULL,
  `capability` int(11) NOT NULL,
  PRIMARY KEY (`role`,`capability`),
  INDEX (`capability`, `role`),
  CONSTRAINT `rel_role_capability_role_ibfk` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `rel_role_capability_capability_ibfk` FOREIGN KEY (`capability`) REFERENCES `capability` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `rel_role_capability`;
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(1, 1);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(1, 2);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(1, 3);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(1, 4);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(1, 5);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(1, 6);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(2, 1);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(2, 3);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(2, 4);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(3, 1);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(4, 1);
INSERT INTO `rel_role_capability` (`role`, `capability`) VALUES(4, 3);

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX (`role`),
  CONSTRAINT `user_role_ibfk` FOREIGN KEY (`role`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

TRUNCATE TABLE `user`;
INSERT INTO `user` (`id`, `login`, `pwd`, `lastname`, `firstname`, `role`) VALUES(1, 'su', 'su@pwd', 'Objectif 3W', 'Webmaster', 1);
INSERT INTO `user` (`id`, `login`, `pwd`, `lastname`, `firstname`, `role`) VALUES(2, 'admin', 'admin@pwd', 'Nebuchadnezzar', 'Morpheus', 2);
INSERT INTO `user` (`id`, `login`, `pwd`, `lastname`, `firstname`, `role`) VALUES(3, 'user', 'user@pwd', 'Anderson', 'Thomas A.', 3);
INSERT INTO `user` (`id`, `login`, `pwd`, `lastname`, `firstname`, `role`) VALUES(4, 'editeur', 'edit@pwd', 'Objectif 3W', 'Éditeur', 4);