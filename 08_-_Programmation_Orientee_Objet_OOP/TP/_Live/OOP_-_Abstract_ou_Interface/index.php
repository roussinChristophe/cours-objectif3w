<?php
function loadClass(string $className) {
  if(file_exists('Classes/' . $className . '.php')) {
    require_once('Classes/' . $className . '.php');
  }
}

function loadInterface(string $className) {
  if(file_exists('Classes/Interfaces/' . $className . '.php')) {
    require_once('Classes/Interfaces/' . $className . '.php');
  }
}

spl_autoload_register('loadClass');
spl_autoload_register('loadInterface');

// $vehicule1 = new Vehicle;
// $vehicule1->move();

echo '<hr>';
$velo1 = new Cycle;
if(method_exists($velo1, 'open')) {
  $velo1->open();
}
$velo1->move();
$velo1->load();
$velo1->unload();

echo '<hr>';
$voiture1 = new Car;
$voiture1->open();
$voiture1->move();
// $voiture1->load();
// $voiture1->unload();

echo '<hr>';
$camion1 = new Truck;
$camion1->open();
$camion1->move();
$camion1->load();
$camion1->unload();