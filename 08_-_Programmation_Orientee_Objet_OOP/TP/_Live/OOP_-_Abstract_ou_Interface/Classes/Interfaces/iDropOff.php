<?php
interface iDropOff {
  public function load();
  public function unload();
}