<?php
class Truck extends Vehicle implements iOpening, iDropOff {
  public function open() {
    echo 'Je suis un camion donc je m\'ouvre avec une clé<br>';
  }
  public function load() {
    echo 'Je charge le camion<br>';
  }
  public function unload() {
    echo 'Je décharge le camion<br>';
  }
}