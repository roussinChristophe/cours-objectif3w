<?php
class Cycle extends Vehicle implements iDropOff {
  public function load() {
    echo 'Je charge la remorque du vélo<br>';
  }
  public function unload() {
    echo 'Je décharge la remorque du vélo<br>';
  }
}