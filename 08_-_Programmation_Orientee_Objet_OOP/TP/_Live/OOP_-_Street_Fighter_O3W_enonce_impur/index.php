<?php
session_start();

require_once('classes/Player.php');
require_once('models/Model.php');
require_once('models/PlayerModel.php');

if(isset($_GET['deconnect'])) {
  if(isset($_SESSION['streetfighter'])) {
    unset($_SESSION['streetfighter']);
  }
  header('Location:.');
  exit;
}

if(!empty($_SESSION['streetfighter']['player'])) {
  $player = unserialize($_SESSION['streetfighter']['player']);
}

if(isset($_POST['signin']) || isset($_POST['signup'])) {
  try {
    $playerModel = new PlayerModel('mysql', 'localhost', 'streetfighter', 'root', '');

    if(isset($_POST['signin']) && !empty($_POST['login'])) {
      if(($data = $playerModel->signin(htmlentities($_POST['login'])))!==false) {
        $player = new Player($data);

        $_SESSION['streetfighter']['player'] = serialize($player);
        header('Location:game.php');
        exit;
      } else {
        $message = 'Mauvais identifiant';
      }
    }

    if(isset($_POST['signup']) && !empty($_POST['email']) && !empty($_POST['alias'])) {
      if(($id = $playerModel->create(htmlentities($_POST['email']), htmlentities($_POST['firstname']), htmlentities($_POST['lastname']), htmlentities($_POST['alias'])))!==false) {
        $player = new Player($_POST);
        $player->setId($id);

        $_SESSION['streetfighter']['player'] = serialize($player);
        header('Location:game.php');
        exit;
      } else {
        $message = 'Problème de connexion avec la base de données. Nous vous présentons nos excuses. Veuillez réessayer ultérieurement';
      }
    }
  } catch(Exception $e) {
    die($e->getMessage());
  }
}
?><!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Street fighter O3W | Account</title>
  </head>
  <body>
    <?php
    if(!empty($message)) {
      echo '<p>' . $message . '</p>';
    }
    ?>
    <?php if(!empty($player)) { ?>
    <p>Bonjour <?php echo $player->getAlias(); ?></p>
    <a href=".?deconnect">Se déconnecter</a>
    <?php } else { ?>
    <form action="." method="post">
      <input type="text" name="login" placeholder="* Votre email ou votre pseudo" required aria-required="true">
      <button name="signin" type="submit">Se connecter</button>
      <span>(*) champs obligatoire</span>
    </form>
    <form action="." method="post">
      <input type="email" name="email" placeholder="* Saisissez votre email" aria-label="Saisissez votre email" required aria-required="true">
      <input type="text" name="firstname" placeholder="Saisissez votre prénom" aria-label="Saisissez votre prénom">
      <input type="text" name="lastname" placeholder="Saisissez votre nom" aria-label="Saisissez votre nom">
      <input type="text" name="alias" placeholder="* Saisissez votre pseudo" aria-label="Saisissez votre pseudo" required aria-required="true">
      <button name="signup" type="submit">Créer un compte</button>
      <span>(*) champs obligatoire</span>
    </form>
    <?php
    }
    ?>
  </body>
</html>