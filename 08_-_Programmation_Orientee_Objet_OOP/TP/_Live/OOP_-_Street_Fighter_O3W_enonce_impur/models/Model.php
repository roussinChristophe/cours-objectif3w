<?php
class Model {
  protected $_engine;
  protected $_host;
  protected $_dbname;
  protected $_user;
  protected $_pwd;
  protected $_charset;
  protected $_collate;
  /**
   * The instance of PDO
   * 
   * @since 1.0.0
   *
   * @var PDO
   */
  protected $_pdo;



  /**
   * Constructor
   * 
   * @since 1.0.0
   *
   * @param string $engine The DB engine
   * @param string $host The hostname
   * @param string $dbname The name of the database
   * @param string $user An account allowed to connect to the database
   * @param string $pwd The password of an account allowed to connect to the database
   * @param string $charset (optional) The charset encoding
   * @param string $collate (optional) The collate encoding
   * 
   * @throws Exception if an error occured
   */
  public function __construct(string $engine, string $host, string $dbname, string $user, string $pwd, string $charset = 'utf8mb4', string $collate = 'utf8mb4_general_ci') {
    try {
      $this->_engine = $engine;
      $this->_host = $host;
      $this->_dbname = $dbname;
      $this->_user = $user;
      $this->_pwd = $pwd;
      $this->_charset = $charset;
      $this->_collate = $collate;
      $this->_pdo = new PDO($engine . ':host=' . $host . ';dbname=' . $dbname . ';charset=' . $charset, $user, $pwd, array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
    } catch(PDOException $e) {
      throw new Exception('Can not connect to the database', 11, $e);
    }
  }
}