<?php
if(empty($_GET['conv']) || !ctype_digit($_GET['conv'])) {
  header('Location: .?_err=404');
  exit;
}

require_once('config/app.php');
require_once('config/db.php');
require_once('lib/functions.php');


$currentPage = 1;
if(!empty($_GET['page']) && ctype_digit($_GET['page'])) {
  $currentPage = $_GET['page'];
}

$pagination = PAGINATION;
if(!empty($_REQUEST['pagination']) && ctype_digit($_REQUEST['pagination'])) {
  $pagination = $_REQUEST['pagination'];
}

$orderby = 'date';
$order = 'DESC';
if(!empty($_REQUEST['orderby'])) {
  $orderby = $_REQUEST['orderby'];
}
if(!empty($_REQUEST['order'])) {
  $order = $_REQUEST['order'];
}

try {
  $msgModel = new MessageModel(DB_ENGINE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PWD);
  $messages = $msgModel->readAll($_GET['conv'], $pagination, $pagination*($currentPage-1), $orderby, $order);
} catch(Exception $e) {
  header('Location: .?_err=500');
  exit;
}

if(!$messages) {
  header('Location: .?_err=404');
  exit;
}
?><!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Messages de la conversation <?php echo $_GET['conv']; ?> | Forum</title>
    
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <a href="." title="Retour aux conversations">Retour</a>
    <form action="?conv=<?php echo $_GET['conv']; ?>" method="post">
      <select name="pagination">
        <option<?php if($pagination==10) echo ' selected="selected"'; ?>>10</option>
        <option<?php if($pagination==20) echo ' selected="selected"'; ?>>20</option>
        <option<?php if($pagination==50) echo ' selected="selected"'; ?>>50</option>
      </select>
      
      <select name="orderby">
        <option<?php if($orderby=='id') echo ' selected="selected"'; ?> value="id">ID</option>
        <option<?php if($orderby=='date') echo ' selected="selected"'; ?> value="date">Date</option>
        <option<?php if($orderby=='author') echo ' selected="selected"'; ?> value="author">Auteur</option>
      </select>
      <select name="order">
        <option<?php if($order=='ASC') echo ' selected="selected"'; ?> value="ASC">Croissant</option>
        <option<?php if($order=='DESC') echo ' selected="selected"'; ?> value="DESC">Décroissant</option>
      </select>
      <button type="submit">Trier</button>
    </form>
    <?php if(!empty($messages) && !is_null($messages[0]['m_id'])) { ?>
    <table border="1" style="border-collapse: collapse;">
      <thead>
        <tr>
          <th><a href="?conv=<?php echo $_GET['conv']; ?><?php if(isset($_REQUEST['pagination'])) echo '&pagination=' . $_REQUEST['pagination'] ?>&orderby=date<?php if(isset($_REQUEST['order']) && $_REQUEST['order']=='ASC') { echo '&order=DESC'; } else { echo '&order=ASC'; } ?>">Date du message</a></th>
          <th>Heure du message</th>
          <th><a href="?conv=<?php echo $_GET['conv']; ?><?php if(isset($_REQUEST['pagination'])) echo '&pagination=' . $_REQUEST['pagination'] ?>&orderby=author<?php if(isset($_REQUEST['order']) && $_REQUEST['order']=='ASC') { echo '&order=DESC'; } else { echo '&order=ASC'; } ?>">Nom Prénom</a></th>
          <th>Message</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td colspan="4">
            <?php if($currentPage>1) { ?>
            <a href="?conv=<?php echo $_GET['conv']; ?>&page=<?php echo $currentPage - 1; ?><?php if(isset($_REQUEST['pagination'])) echo '&pagination=' . $_REQUEST['pagination'] ?><?php if(isset($_REQUEST['orderby'])) echo '&orderby=' . $_REQUEST['orderby'] ?><?php if(isset($_REQUEST['order'])) echo '&order=' . $_REQUEST['order'] ?>" title="Page précédente">Page précédente</a>
            <?php } ?>
            <?php if(ceil($messages[0]['nbMsg']/$pagination)>$currentPage) { ?>
            <a href="?conv=<?php echo $_GET['conv']; ?>&page=<?php echo $currentPage + 1; ?><?php if(isset($_REQUEST['pagination'])) echo '&pagination=' . $_REQUEST['pagination'] ?><?php if(isset($_REQUEST['orderby'])) echo '&orderby=' . $_REQUEST['orderby'] ?><?php if(isset($_REQUEST['order'])) echo '&order=' . $_REQUEST['order'] ?>" title="Page suivante">Page suivante</a>
            <?php } ?>
          </td>
        </tr>
      </tfoot>
      <tbody>
        <?php
        foreach($messages as $data) {
          $msg = new Message($data);
          $msg->showTr();
        }
        ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p>Aucun message</p>
    <?php } ?>
  </body>
</html>