<?php
class Message {
  private $_date;
  private $_hour;
  private $_author;
  private $_message;

  public function __construct(array $data) {
    $this->hydrate($data);
  }

  private function setDate(string $date) {
    $this->_date = $date;
  }

  private function setHour(string $hour) {
    $this->_hour = $hour;
  }

  private function setAuthor(string $author) {
    $this->_author = $author;
  }

  private function setMessage(string $message) {
    $this->_message = $message;
  }

  public function getDate(): string {
    return $this->_date;
  }

  public function getHour(): string {
    return $this->_hour;
  }

  public function getAuthor(): string {
    return $this->_author;
  }

  public function getMessage(): string {
    return $this->_message;
  }

  private function hydrate(array $data) {
    foreach($data as $prop=>$value) {
      $setter = 'set' . ucfirst($prop);
      if(method_exists($this, $setter)) {
        $this->$setter($value);
      }
    }
  }

  public function showTr() {
    echo '
        <tr>
          <td>' . $this->getDate() . '</td>
          <td>' . $this->getHour() . '</td>
          <td>' . $this->getAuthor() . '</td>
          <td>' . $this->getMessage() . '</td>
        </tr>';
  }
}