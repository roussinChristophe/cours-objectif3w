<?php
class MessageModel extends CoreModel {
  public function readAll(int $conversation, int $pagination, int $limit = 0, string $orderby = 'date', string $order = 'DESC') {
    if(strtoupper($order)!='ASC' && strtoupper($order)!='DESC') {
      $order = 'DESC';
    }
    switch($orderby) {
      case 'id':
        $orderby = 'm_id ' . $order;
        break;
      case 'author':
        $orderby = 'u_nom ' . $order . ', u_prenom ' . $order;
        break;
      default:
        $orderby = 'm_date ' . $order;
    }
    
    try {
      if(($req = $this->getDb()->prepare('SELECT `m_id`, DATE_FORMAT(`m_date`, "%d/%m/%Y") AS `date`,
                              DATE_FORMAT(`m_date`, "%T") AS `hour`,
                              CONCAT(`u_nom`, " ", `u_prenom`) AS `author`,
                              `m_contenu` AS `message`,
                              (
                                SELECT COUNT(`m_id`)
                                FROM `message`
                                RIGHT JOIN `conversation` ON `message`.`m_conversation_fk`=`conversation`.`c_id`
                                WHERE `c_id`=:conv
                              ) AS `nbMsg`
                              FROM `message`
                              RIGHT JOIN `user` ON `message`.`m_auteur_fk`=`user`.`u_id`
                              RIGHT JOIN `conversation` ON `message`.`m_conversation_fk`=`conversation`.`c_id`
                              WHERE `c_id`=:conv
                              ORDER BY ' . $orderby . '
                              LIMIT :limit, :pagination'))!==false) {
        if($req->bindValue('conv', $conversation, PDO::PARAM_INT) && $req->bindValue('limit', $limit, PDO::PARAM_INT) && $req->bindValue('pagination', $pagination, PDO::PARAM_INT)) {
          if($req->execute()) {
            $messages = $req->fetchAll(PDO::FETCH_ASSOC);
            $req->closeCursor();
            
            if(!empty($messages)) {
              return $messages;
            }
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception($e->getMessage(), 99, $e);
    }
  }
}

