<?php
/**
 * Manages the data for the character, according to the CRUD model
 * 
 * @license MIT
 * 
 * @since 1.0.0
 * 
 * @category PHP
 * @package StreetFighterO3W
 * @subpackage Character
 * @copyright 2018 Objectif 3W - all rights reserved
 * @author Damien TIVELET <d.tivelet@objectif3w.com>
 */
class CharacterModel {
  /**
   * The query string to create the table
   * 
   * @since 1.0.0
   *
   * @const string
   */
  const QUERY_CREATE_TABLE = 'CREATE TABLE IF NOT EXISTS `character` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(75) NOT NULL,
  `damages` INT(3) NOT NULL DEFAULT 0,
  PRIMARY KEY(`id`),
  CONSTRAINT `character_name` UNIQUE (`name`)
) ENGINE=InnoDB charset=utf8mb4;';

  /**
   * The instance of PDO
   * 
   * @since 1.0.0
   *
   * @var PDO
   */
  private $_pdo;



  /**
   * Constructor
   * 
   * @since 1.0.0
   *
   * @param string $engine The DB engine
   * @param string $host The hostname
   * @param string $dbname The name of the database
   * @param string $user An account allowed to connect to the database
   * @param string $pwd The password of an account allowed to connect to the database
   * @param string $charset (optional) The charset encoding
   * @param string $collate (optional) The collate encoding
   * 
   * @throws Exception if an error occured
   */
  public function __construct(string $engine, string $host, string $dbname, string $user, string $pwd, string $charset = 'utf8mb4', string $collate = 'utf8mb4_general_ci') {
    try {
      $this->_pdo = new PDO($engine . ':host=' . $host . ';dbname=' . $dbname . ';charset=' . $charset, $user, $pwd, array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
    } catch(PDOException $e) {
      throw new Exception('Can not connect to the database', 11, $e);
    }
  }



  /**
   * Creates a character in the database
   *
   * @param string $name The name of the character
   * @param integer $damages The damages of the character
   * 
   * @throws Exception if an error occured
   * 
   * @return integer The last inserted ID
   */
  public function create(string $name, int $damages): int {
    try {
      if(($req = $this->_pdo->prepare('INSERT INTO `character` (`name`, `damages`) VALUES (?,?)'))!==false) {
        if($req->bindValue(1, $name) && $req->bindValue(2, $damages)) {
          if($req->execute()) {
            $res = $this->_pdo->lastInsertId();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not insert into the database', 12, $e);
    }
  }

  /**
   * Selects one or more character
   *
   * @param string $name (optional) The name of a character
   * 
   * @throws Exception if an error occured
   * 
   * @return array The data of one or more character
   */
  public function read(string $name = null): array {
    try {
      if(is_null($name)) {
        if(($req = $this->_pdo->query('SELECT * FROM `character` ORDER BY `name` ASC'))!==false) {
          $res = $req->fetchAll(PDO::FETCH_ASSOC);
          $req->closeCursor();
          return $res;
        }
      } else {
        if(($req = $this->_pdo->prepare('SELECT * FROM `character` WHERE `name`=?'))!==false) {
          if($req->bindValue(1, $name)) {
            if($req->execute()) {
              $res = $req->fetchAll(PDO::FETCH_ASSOC);
              $req->closeCursor();
              return $res;
            }
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not select in the database', 13, $e);
    }
  }

  /**
   * Updates a character
   *
   * @param string $name The name of a character
   * @param integer $damages The damages of the character
   * 
   * @throws Exception if an error occured
   * 
   * @return integer The number of rows affected
   */
  public function update(string $name, int $damages): int {
    try {
      if(($req = $this->_pdo->prepare('UPDATE `character` SET `damages`=? WHERE `name`=?'))!==false) {
        if($req->bindValue(1, $damages) && $req->bindValue(2, $name)) {
          if($req->execute()) {
            $res = $req->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not update in the database', 14, $e);
    }
  }

  /**
   * Deletes a character
   * 
   * @param string $name The name of a character
   * 
   * @throws Exception if an error occured
   * 
   * @return integer The number of rows affected
   */
  public function delete(string $name): int {
    try {
      if(($req = $this->_pdo->prepare('DELETE FROM `character` WHERE `name`=?'))!==false) {
        if($req->bindValue(1, $name)) {
          if($req->execute()) {
            $res = $req->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not delete in the database', 15, $e);
    }
  }
}