<?php
/**
 * Class representing a character
 * 
 * Class with properties for name and damages, and methods to hit and receiveDamages
 * 
 * @license MIT
 * 
 * @since 1.0.0
 * 
 * @category PHP
 * @package StreetFighterO3W
 * @subpackage Character
 * @copyright 2018 Objectif 3W - all rights reserved
 * @author Damien TIVELET <d.tivelet@objectif3w.com>
 */
class Character {
  /**
   * The minimum damage of the character
   * 
   * @since 1.0.0
   *
   * @const integer
   */
  const MIN_DAMAGES = 0;
  /**
   * The maximum damage of the character
   * 
   * @since 1.0.0
   *
   * @const integer
   */
  const MAX_DAMAGES = 100;

  /**
   * The id of the character
   *
   * @var integer
   */
  private $_id;
  /**
   * The name of the character
   * 
   * @since 1.0.0
   *
   * @var string
   */
  private $_name;
  /**
   * The damages suffered by the character
   *
   * @var integer
   */
  private $_damages;



  /**
   * Set the value of _id
   * 
   * @since 1.0.0
   *
   * @param integer $id
   * 
   * @return self
   */
  public function setId(int $id): self {
    $this->_id = $id;
    return $this;
  }

  /**
   * Set the value of _name
   * 
   * @since 1.0.0
   *
   * @param string $name
   * 
   * @return self
   */
  public function setName(string $name): self {
    $this->_name = $name;
    return $this;
  }
  
  /**
   * Set the value of _damages
   * 
   * @since 1.0.0
   *
   * @param integer $damages
   * 
   * @return self
   */
  public function setDamages(int $damages): self {
    $this->_damages = $damages;
    return $this;
  }



  /**
   * Get the value of _id
   * 
   * @since 1.0.0
   *
   * @return integer
   */
  public function getId(): int {
    return $this->_id;
  }

  /**
   * Get the value of _name
   * 
   * @since 1.0.0
   *
   * @return string
   */
  public function getName(): string {
    return $this->_name;
  }

  /**
   * Get the value of _damages
   * 
   * @since 1.0.0
   *
   * @return integer
   */
  public function getDamages(): int {
    return $this->_damages;
  }



  /**
   * Undocumented function
   * 
   * @since 1.0.0
   *
   * @param array $data
   */
  public function __construct(array $data) {
    // if(isset($data['name'])) {
    //   $this->setName($data['name']);
    // }
    // if(isset($data['damages'])) {
    //   $this->setDamages($data['damages']);
    // }
    $this->hydrate($data);
  }



  /**
   * Hydrates the properties with the data
   * 
   * @since 1.0.0
   *
   * @param array $data
   * 
   * @return void
   */
  public function hydrate(array $data): void {
    foreach($data as $key=>$value) {
      $method = 'set' . ucfirst(strtolower($key));
      if(method_exists($this, $method)) {
        $this->$method($value);
      }
    }
  }



  /**
   * Hits a target
   * 
   * @since 1.0.0
   *
   * @param Character $target The target
   * 
   * @return mixed The result of the hit
   */
  public function hit(Character $target) {
    if($target!=$this) {
      $hit = mt_rand(1,25);
      return array('hit'=>$hit, 'receive'=>$target->receiveDamages($hit));
    }
    return 'self';
  }

  /**
   * Receives damages
   * 
   * @since 1.0.0
   *
   * @param integer $hit The power of the hit
   * 
   * @return mixed The result after damages
   */
  public function receiveDamages(int $hit) {
    // $this->setDamages($this->getDamages() + $hit);
    $hit = mt_rand(0, $hit);
    $this->_damages += $hit;
    if($this->_damages>=static::MAX_DAMAGES) {
      return 'KO';
    }
    return $hit;
  }
}