<?php
session_start();

require_once('classes/Character.php');
require_once('models/CharacterModel.php');

if(isset($_GET['del'])) {
  if(isset($_SESSION['streetfighter'])) {
    unset($_SESSION['streetfighter']);
  }
  header('Location:.');
  exit;
}

try {
  $characterModel = new CharacterModel('mysql', 'localhost', 'streetfighter', 'root', '');

  if(!empty($_SESSION['streetfighter']['fighter'])) {
    $fighter = unserialize($_SESSION['streetfighter']['fighter']);
  }

  if(!empty($_POST['name'])) {
    if(isset($_POST['create'])) {
      if(strtoupper($_POST['name'])=='CHUCK NORRIS') {
        $message = 'Personne ne peut créer Chuck Norris car c\'est lui qui vous crée';
      } else {
        if(($id = $characterModel->create(htmlentites($_POST['name']), Character::MIN_DAMAGES))!==false) {
          $fighter = new Character($_POST);
          $fighter->setId($id);
          $fighter->setDamages(Character::MIN_DAMAGES);
        }
      }
    } elseif(isset($_POST['choose'])) {
      $data = $characterModel->read($_POST['name']);
      if(count($data)>0) {
        $fighter = new Character($data[0]);
      }
    }

    if(!empty($fighter)) {
      $_SESSION['streetfighter']['fighter'] = serialize($fighter);
    }
  }

  if(!empty($fighter) && !empty($_GET['target'])) {
    $data = $characterModel->read($_GET['target']);
    if(count($data)>0) {
      $target = new Character($data[0]);

      if(strtoupper($target->getName())=='CHUCK NORRIS') {
        $message = 'Chuck Norris ne perd jamais !!!';
      } else {
        $hit = $fighter->hit($target);
        switch($hit) {
          case 'self':
            $message = 'Ne te frappe pas toi-même idiot !';
            break;
          default:
            if($hit['receive']==='KO') {
              if($characterModel->delete($target->getName())>0) {
                $message = $fighter->getName() . ' a éliminé ' . $target->getName();
              } else {
                $message = 'Un problème est survenue ... Action annulée';
              }
            } else {
              if($characterModel->update($target->getName(), $target->getDamages())>0) {
                $message = $fighter->getName() . ' a infligé ' . $hit['hit'] . ' point(s) de dégât(s) à ' . $target->getName() . ' qui a encaissé ' . $hit['receive'];
              } else {
                $message = 'Un problème est survenue ... Action annulée';
              }
            }
        }
      }
    } else {
      if(strtoupper($_GET['target'])=='CHUCK NORRIS') {
        $message = 'Il n \'y a que dans Google qu\'on peut taper Chuck Norris';
      } else {
        $message = 'Tu frappes dans le vent minus !';
      }
    }
  }

  $characters = $characterModel->read();
} catch(Exception $e) {
  $message = $e->getMessage();
}
?><!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Street fighter O3W</title>
  </head>
  <body>
    <a href=".?del">Réinitialiser</a>

    <aside>
      <form action="." method="post">
        <input type="text" name="name" placeholder="Nom du personnage" aria-label="Nom du personnage">
        <button name="create" type="submit">Créer le personnage</button>
      </form>
      <?php
      if(isset($fighter)) {
        echo '<p>Vous incarnez : ' . $fighter->getName() . ' (' . $fighter->getDamages() . ')</p>';
      }
      ?>
      <?php if(!empty($characters) && count($characters)>1) { ?>
      <form action="." method="post">
        <select name="name">
          <option></option>
          <?php
          foreach($characters as $data) {
            $character = new Character($data);
            if($character!=$fighter) {
          ?>
          <option value="<?php echo $character->getName(); ?>"><?php echo $character->getName(); ?> (<?php echo $character->getDamages(); ?>)</option>
          <?php
            }
          }
          ?>
        </select>
        <button name="choose" type="submit">Changer de personnage</button>
      </form>
      <?php } ?>
    </aside>

    <?php if(!empty($characters) && !empty($fighter)) { ?>
    <aside>
      <h2>Liste de vos adversaires possibles : </h2>
      <ul>
        <?php
        foreach($characters as $data) {
          $character = new Character($data);
          if($character!=$fighter) {
        ?>
        <li><a href=".?target=<?php echo urlencode($character->getName()); ?>"><?php echo $character->getName(); ?> (<?php echo $character->getDamages(); ?>)</a></li>
        <?php
          }
        }
        ?>
      </ul>
    </aside>
    <?php } ?>
    
    <?php
    if(!empty($message)) {
      echo '<p>' . $message . '</p>';
    }
    ?>
  </body>
</html>