<?php
/**
 * --------------------------------------------------
 * CLASSES
 * --------------------------------------------------
**/

/**
 * Définir une voiture.
 * Description :
 *     Classe héritée de Vehicles et implémentant l'interface iCabin.
 * Méthodes :
 *     - move : écrit le mouvement du véhicule.
 *     - unlockCabin : écrit le comportement d'ouverture du véhicule.
**/
class Car extends Vehicles implements iCabin {
    /**
     * move Déplacer la voiture
     * @return void
     */
    public function move() {
        echo '<li>Accélérer ... à fond à fond à FOOOOOOOOONNNNNNNDDDDDD !!!</li>';
    }

    /**
     * unlockCabin Déverrouiller la voiture
     * @return void
     */
    public function unlockCabin() {
        echo '<li>Appuyer sur la télécommande</li>';
    }
    /**
     * sit S'assoir dans la voiture
     * @return void
     */
    public function sit() {
        echo '<li>Cale toi bien dans le siège baquet</li>';
    }
}