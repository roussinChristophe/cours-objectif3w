<?php
/**
 * --------------------------------------------------
 * CLASSES
 * --------------------------------------------------
**/

/**
 * Définir un vélo avec une remorque.
 * Description :
 *     Classe héritée de Vehicles et implémentant l'interface iPacking.
 * Méthodes :
 *     - move : écrit le mouvement du véhicule.
 *     - pack : écrit le comportement de chargement véhicule.
 *     - unpack : écrit le comportement de déchargement du véhicule.
**/
class Cycle extends Vehicles implements iPacking {
    /**
     * move Déplacer le vélo
     * @return void
     */
    public function move() {
        echo '<li>Pédaler ... à fond à fond à FOOOOOOOOONNNNNNNDDDDDD !!!</li>';
    }

    /**
     * sit S'assoir sur le vélo
     * @return void
     */
    public function sit() {
        echo '<li>S\'assoir : aïe, ça fait toujours aussi mal au ...</li>';
    }

    /**
     * pack Charger la remorque du vélo
     * @return void
     */
    public function pack() {
        echo '<li>Pour charger : tout jeter dans la remorque</li>';
    }

    /**
     * unpack Décharger la remorque du vélo
     * @return void
     */
    public function unpack() {
        echo '<li>Marre de pédaler : renverser la remorque</li>';
    }
}