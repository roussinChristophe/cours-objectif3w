<?php
function loadClass(string $className) {
  require_once('Classes/' . $className . '.php');
}
spl_autoload_register('loadClass');


// echo '<hr>';
// $vehicule1 = new Vehicle(12, 2, 'Noir');
// echo '<p><strong>' . Vehicle::MY_CLASS . ' 1 : Noir</strong></p>';
// echo 'État du véhicule : ' . $vehicule1->getEtat() . '%<br>';
// $vehicule1->move();
// echo 'État du véhicule : ' . $vehicule1->getEtat() . '%<br>';

echo '<hr>';
$voiture1 = new Car('Essence', 5, 'Rouge');
echo '<p><strong>' . Car::MY_CLASS . ' 1 : Rouge</strong></p>';
echo 'État de la voiture : ' . $voiture1->getEtat() . '%<br>';
$voiture1->move();
echo 'État de la voiture : ' . $voiture1->getEtat() . '%<br>';
$voiture1->klaxon();
echo 'État de la voiture : ' . $voiture1->getEtat() . '%<br>';

echo '<hr>';
$cycle1 = new Cycle(2, 1, 'Gris');
echo '<p><strong>' . Cycle::MY_CLASS . ' 1 : Gris</strong></p>';
echo 'État du vélo : ' . $cycle1->getEtat() . '%<br>';
$cycle1->move();
echo 'État du vélo : ' . $cycle1->getEtat() . '%<br>';
$cycle1->klaxon();
echo 'État du vélo : ' . $cycle1->getEtat() . '%<br>';

echo '<hr>';
$cycle2 = new Cycle(2, 1, 'Bleu');
echo '<p><strong>' . Cycle::MY_CLASS . ' 2 : Bleu</strong></p>';
echo 'État du vélo : ' . $cycle2->getEtat() . '%<br>';
$cycle2->move();
echo 'État du vélo : ' . $cycle2->getEtat() . '%<br>';
$cycle2->klaxon();
echo 'État du vélo : ' . $cycle2->getEtat() . '%<br>';

echo '<hr><p>Il y a un total de ' . Vehicle::getCompteur() . ' véhicule(s) dont ' . Car::$compteur . ' voiture(s) et ' . Cycle::$compteur . ' vélo(s)</p>';