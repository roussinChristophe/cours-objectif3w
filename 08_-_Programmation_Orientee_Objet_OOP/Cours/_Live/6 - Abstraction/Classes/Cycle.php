<?php
class Cycle extends Vehicle {
  const MY_CLASS = 'Cycle';

  static $compteur = 0;

  public function __construct(int $roues, int $passagers, string $couleur) {
    parent::__construct($roues, $passagers, $couleur);

    self::$compteur++;
    echo 'Je suis le vélo numéro : ' . self::$compteur . ' parmis ' . Vehicle::$compteur . ' véhicules<br>';

    echo 'Je suis de la classe mère : ' . parent::MY_CLASS . '<br>';
    echo 'Je suis de la classe fille : ' . self::MY_CLASS . '<br>';
  }

  public function klaxon() {
    echo 'Pouet pouet<br>';
  }
}