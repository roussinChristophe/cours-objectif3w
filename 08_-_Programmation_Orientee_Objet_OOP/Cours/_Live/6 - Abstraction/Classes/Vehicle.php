<?php
// On contraint les développeur(euse)s à instancier uniquement des classes filles
abstract class Vehicle {
  private $nbRoues;
  private $nbPassagers;
  private $couleur;
  protected $etat;

  const MY_CLASS = 'Vehicle';

  static protected $compteur = 0;

  public function __construct(int $roues, int $passagers, string $couleur) {
    $this->nbRoues = $roues;
    $this->nbPassagers = $passagers;
    $this->setCouleur($couleur);

    $this->etat = 100;

    self::$compteur++;
    echo 'Je suis le véhicule numéro : ' . self::$compteur . '<br>';

    echo 'Je suis de la classe mère : ' . self::MY_CLASS . '<br>';
    echo 'Je suis de la classe fille : ' . static::MY_CLASS . '<br>';
  }

  public function setCouleur(string $nouvelleCouleur) {
    $this->couleur = $nouvelleCouleur;
		return $this;
  }

  public function getNbRoues() : string {
    return $this->nbRoues;
  }

  public function getNbPassagers() : int {
    return $this->nbPassagers;
  }

  public function getCouleur() : string {
    return $this->couleur;
  }

  public function getEtat() : string {
    return $this->etat;
  }

  public function move() {
    echo 'Le vehicule a bougé!<br/>';
  }

  public static function getCompteur() {
    return self::$compteur;
  }

  // La fonctionnalité de klaxon étant différente en fonction des classes filles mais étant obligatoire pour toutes ces dernières,
  // on contraint les développeur(euse)s à la définir selon la même déclaration que celle de la classe mère
  abstract public function klaxon();
}