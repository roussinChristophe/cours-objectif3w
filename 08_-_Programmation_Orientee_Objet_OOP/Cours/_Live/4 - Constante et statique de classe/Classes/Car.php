<?php
class Car extends Vehicle {
  private $moteur;
  private $tailleCoffre;
  
  const MY_CLASS = 'Car';

  static $compteur = 0;

  public function __construct(string $moteur, int $tailleCoffre, string $couleur) {
    parent::__construct(4, 5, $couleur);
    $this->moteur = $moteur;
    $this->tailleCoffre = $tailleCoffre;

    self::$compteur++;
    echo 'Je suis la voiture numéro : ' . self::$compteur . ' parmis ' . Vehicle::$compteur . ' véhicules<br>';

    echo 'Je suis de la classe mère : ' . parent::MY_CLASS . '<br>';
    echo 'Je suis de la classe fille : ' . self::MY_CLASS . '<br>';
  }

  public function klaxon() {
    echo 'Tut tut<br>';
    $this->etat -= 0.0001;
  }

  public function move() {
    parent::move();
    $this->etat -= 10;
  }
}