<?php
abstract class Personnage implements iMouvement, iInteraction {
  /**
   * $nom Nom du personnage
   * 
   * @var string
   */
  private $nom;


  /**
   * setNom
   * 
   * @param string $nom Le nom du personnage
   * @return void
   */
  public function setNom( $nom ) {
    $this->nom = $nom;
  }

  /**
   * getNom
   * 
   * @return string Le nom du personnage
   */
  public function getNom() : string {
    return $this->nom;
  }
}