<?php
function loadClass(string $className) {
  if(file_exists('Classes/' . $className . '.php')) {
    require_once('Classes/' . $className . '.php');
  }
}
spl_autoload_register('loadClass');

function loadInterfaces(string $className) {
  if(file_exists('Interfaces/' . $className . '.php')) {
    require_once('Interfaces/' . $className . '.php');
  }
}
spl_autoload_register('loadInterfaces');



$monPerso1 = new Magicien;
$monPerso1->setNom( 'Glandalf' );
echo 'Mon nom est : ' . $monPerso1->getNom();
echo '<br>';
$monPerso1->parle();
echo '<br>';
$monPerso1->seDeplace();

echo '<hr>';
$monKangourou1 = new Kangourou;
$monKangourou1->setNom( 'Jumpy' );
echo 'Mon nom est : ' . $monKangourou1->getNom();
echo '<br>';
$monKangourou1->parle();
echo '<br>';
$monKangourou1->seDeplace();

echo '<hr>';
$rocher = new RocherQuiParle;
$rocher->setNom( 'Suchard' );
echo 'Mon nom est ' . $rocher->getNom() . ' ... Rocher ' . $rocher->getNom();
echo '<br>';
$rocher->parle();
echo '<br>';
$rocher->seDeplace();

echo '<hr>';
$armoire = new Armoire;
$armoire->seDeplace();