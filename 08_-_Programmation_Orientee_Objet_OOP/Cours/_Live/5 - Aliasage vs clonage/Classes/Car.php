<?php
class Car extends Vehicle {
  private $moteur;
  private $tailleCoffre;

  public function __construct(string $moteur, int $tailleCoffre, string $couleur) {
    parent::__construct(4, 5, $couleur);
    $this->moteur = $moteur;
    $this->tailleCoffre = $tailleCoffre;
  }

  public function klaxon() {
    echo 'Tut tut<br>';
    $this->etat -= 0.0001;
  }

  public function move() {
    parent::move();
    $this->etat -= 10;
  }
}