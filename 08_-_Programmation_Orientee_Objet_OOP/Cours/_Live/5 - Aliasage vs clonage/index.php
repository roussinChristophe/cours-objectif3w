<?php
require_once('Classes/Vehicle.php');
require_once('Classes/Car.php');
require_once('Classes/Cycle.php');

echo '<hr><p><strong>Voiture 1 : Rouge</strong></p>';
$voiture1 = new Car('Essence', 5, 'Rouge');
echo 'État de la voiture : ' . $voiture1->getEtat() . '%<br>';
$voiture1->move();
echo 'État de la voiture : ' . $voiture1->getEtat() . '%<br>';
$voiture1->klaxon();
echo 'État de la voiture : ' . $voiture1->getEtat() . '%<br>';

echo '<hr><p><strong>Velo 1 : Gris</strong></p>';
$cycle1 = new Cycle(2, 1, 'Gris');
echo 'État du vélo : ' . $cycle1->getEtat() . '%<br>';
$cycle1->move();
echo 'État du vélo : ' . $cycle1->getEtat() . '%<br>';
$cycle1->klaxon();
echo 'État du vélo : ' . $cycle1->getEtat() . '%<br>';

echo '<hr><p><strong>Vélo 2 : Bleu</strong></p>';
$cycle2 = new Cycle(2, 1, 'Bleu');
echo 'État du vélo : ' . $cycle2->getEtat() . '%<br>';
$cycle2->move();
echo 'État du vélo : ' . $cycle2->getEtat() . '%<br>';
$cycle2->klaxon();
echo 'État du vélo : ' . $cycle2->getEtat() . '%<br>';

echo '<hr><br><p><strong>Copie d\'objet</strong></p>';
echo '<p>Avant changement de couleur de l\'un des 2 objets</p>';
$cycle3 = $cycle1;
var_dump($cycle1);
var_dump($cycle3);
$cycle3->setCouleur('Blouge');
echo '<p>Après changement de couleur de l\'un des 2 objets</p>';
var_dump($cycle1);
var_dump($cycle3);

echo '<hr><br><p><strong>Clonage d\'objet</strong></p>';
echo '<p>Avant changement de couleur de l\'un des 2 objets</p>';
$cycle4 = clone $cycle1;
var_dump($cycle1);
var_dump($cycle4);
$cycle4->setCouleur('Purple');
echo '<p>Après changement de couleur de l\'un des 2 objets</p>';
var_dump($cycle1);
var_dump($cycle4);

echo '<hr><br><p><strong>Test égalité simple (== structure et valeurs) entre 2 objets</strong></p>';
var_dump($cycle1);
var_dump($cycle2);
if($cycle1==$cycle2) {
  echo '<p>Velo 1 == Velo 2</p>';
} else {
  echo '<p>Velo 1 != Velo 2</p>';
}
var_dump($cycle1);
var_dump($cycle3);
if($cycle1==$cycle3) {
  echo '<p>Velo 1 == Velo 3</p>';
} else {
  echo '<p>Velo 1 != Velo 3</p>';
}
$cycle5 = clone $cycle1;
var_dump($cycle1);
var_dump($cycle5);
if($cycle1==$cycle5) {
  echo '<p>Velo 1 == Velo 5</p>';
} else {
  echo '<p>Velo 1 != Velo 5</p>';
}

echo '<hr><br><p><strong>Test égalité complexe (=== structure et valeurs ET instance) entre 2 objets</strong></p>';
var_dump($cycle1);
var_dump($cycle2);
if($cycle1===$cycle2) {
  echo '<p>Velo 1 === Velo 2</p>';
} else {
  echo '<p>Velo 1 !== Velo 2</p>';
}
var_dump($cycle1);
var_dump($cycle3);
if($cycle1===$cycle3) {
  echo '<p>Velo 1 === Velo 3</p>';
} else {
  echo '<p>Velo 1 !== Velo 3</p>';
}
var_dump($cycle1);
var_dump($cycle5);
if($cycle1===$cycle5) {
  echo '<p>Velo 1 === Velo 5</p>';
} else {
  echo '<p>Velo 1 !== Velo 5</p>';
}