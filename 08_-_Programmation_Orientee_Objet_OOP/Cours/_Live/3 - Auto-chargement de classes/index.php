<?php
function loadClass(string $className) {
  require_once('Classes/' . $className . '.php');
}
spl_autoload_register('loadClass');


echo '<hr><p><strong>Voiture 1 : Rouge</strong></p>';
$voiture1 = new Car('Essence', 5, 'Rouge');
echo 'État de la voiture : ' . $voiture1->getEtat() . '%<br>';
$voiture1->move();
echo 'État de la voiture : ' . $voiture1->getEtat() . '%<br>';
$voiture1->klaxon();
echo 'État de la voiture : ' . $voiture1->getEtat() . '%<br>';

echo '<hr><p><strong>Vélo 1 : Gris</strong></p>';
$cycle1 = new Cycle(2, 1, 'Gris');
echo 'État du vélo : ' . $cycle1->getEtat() . '%<br>';
$cycle1->move();
echo 'État du vélo : ' . $cycle1->getEtat() . '%<br>';
$cycle1->klaxon();
echo 'État du vélo : ' . $cycle1->getEtat() . '%<br>';

echo '<hr><p><strong>Vélo 2 : Bleu</strong></p>';
$cycle2 = new Cycle(2, 1, 'Bleu');
echo 'État du vélo : ' . $cycle2->getEtat() . '%<br>';
$cycle2->move();
echo 'État du vélo : ' . $cycle2->getEtat() . '%<br>';
$cycle2->klaxon();
echo 'État du vélo : ' . $cycle2->getEtat() . '%<br>';
