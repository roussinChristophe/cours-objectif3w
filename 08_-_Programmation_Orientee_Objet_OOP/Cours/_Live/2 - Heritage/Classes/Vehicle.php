<?php
class Vehicle {
  private $nbRoues;
  private $nbPassagers;
  private $couleur;
  protected $etat;

  public function __construct(int $roues, int $passagers, string $couleur) {
    $this->nbRoues = $roues;
    $this->nbPassagers = $passagers;
    $this->setCouleur($couleur);

    $this->etat = 100;
  }

  public function setCouleur(string $nouvelleCouleur) {
    $this->couleur = $nouvelleCouleur;
		return $this;
  }

  public function getNbRoues() : string {
    return $this->nbRoues;
  }

  public function getNbPassagers() : int {
    return $this->nbPassagers;
  }

  public function getCouleur() : string {
    return $this->couleur;
  }

  public function getEtat() : string {
    return $this->etat;
  }

  public function move() {
    echo 'Le vehicule a bougé!<br/>';
  }
}