<?php
require_once('Classes/Personne.php');

function Foo() {
  $test = new Personne('Jean-Marie DELACOURS', 63, 'Homme', '?');
  $test->parler();

  // Respectant la portée des variables, l'espace mémoire créé dans la fontion est libéré à la fin de cette dernière
  // Le destructeur de l'objet contenu dans cet espace mémoire est donc appelé
}


$pedro = new Personne('Pedro DE LA VEGA', 33, 'Homme', 'es'); // Pour instancier une classe, on utilise new et on stocke l'objet créé
$pedro->parler(); // Toujours la syntaxe $sujet->verbe([$complement])
$francoise = new Personne('Françoise DESBOIS', 26, 'Femme', 'fr'); // Les paramètres sont ceux du constructeur
$francoise->parler();

echo '<hr>';
Foo();
echo '<hr>';

$john = new Personne('John CENA', 39, 'Homme', 'en');
$john->parler();

unset($pedro); // Met fin prématurément à la vie de l'objet $pedro
echo $john; // Pour écrire directement un objet, nous devons définir une méthode __toString dans la classe

// Tous les destructeurs sont appelés à la fin du script, lors de l'opération de vidage mémoire de PHP