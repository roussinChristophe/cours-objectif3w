<?php
// Déclaration d'une classe nommée Personne
class Personne {
  // Définition des données (variables membres ou propriétés) de Personne
  // Chaque (instance de) Personne en possèdera une copie
  /**
   * Nom de la personne
   *
   * @var string
   */
  private $nom; // private interdit l'accès depuis l'extérieur de la classe
  /**
   * Age de la personne
   *
   * @var int
   */
  private $age;
  /**
   * Sexe de la personne
   *
   * @var string
   */
  private $sexe;
  /**
   * Nationalité de la personne
   *
   * @var string
   */
  private $nationalite;



  /**
   * __construct Constructeur : fonction chargée d'initialiser l'objet. Appellée automatiquement lors de la création
   *
   * @param string $nom
   * @param integer $age
   * @param string $sexe
   * @param string $nationalite
   * 
   * @return void
   */
  public function __construct(string $nom, int $age, string $sexe, string $nationalite = '?') {
    // Les membres (variables ou fonctions) sont utilisés par la biais de l'opérateur ->
    // L'objet contenant est à gauche de l'opérateur.
    // Dans les fonctions membres, il s'agit typiquement de $this : faisant référence à l'espace mémoire dans lequel se trouve l'objet actuel
    $this->nom = $nom;
    $this->age = $age;
    $this->sexe = $sexe;
    $this->setNationalite($nationalite);

    echo '<hr>';
    echo 'Debut de vie de ' . $this->getNom();
    echo '<br>';
  }

  /**
   * __destruct Destructeur : fonction appellée lors de la fin de vie de l'objet
   * 
   * @return void
   */
  public function __destruct() {
    echo '<hr>';
    echo 'Fin de vie de ' . $this->getNom();
    echo '<br>';
  }

  /**
   * __toString Fonction appellée lors de la conversion en chaîne de caractères
   *
   * @return void
   */
  public function __toString() {
    return $this->getNom() . ', ' . $this->getAge() . ' ans<br>';
  }



  // Les fonctions permettant de modifier des membres sont appellées setters sont généralement de la forme setNomDonnée($nouvelleValeur)
  // C'est ici que l'on peut vérifier que la nouvelle valeur est valide
  /**
   * setNationalite Affecte une valeur à la propriété $nationalite
   *
   * @param string $value
   * @return void
   */
  public function setNationalite(string $value) {
    $this->nationalite = $value;
  }



  // Les fonctions permettant de lire des membres sont appellées getters sont généralement de la forme getNomDonnée()
  /**
   * getNationalite Retourne la valeur de la propriété $nationalite
   *
   * @return string
   */
  public function getNationalite() : string {
    return $this->nationalite;
  }

  /**
   * getNom Retourne la valeur de la propriété $nom
   *
   * @return string
   */
  public function getNom() : string {
    return $this->nom;
  }

  /**
   * getAge Retourne la valeur de la propriété $age
   *
   * @return integer
   */
  public function getAge() : int {
    return $this->age;
  }

  /**
   * getSexe Retourne la valeur de la propriété $sexe
   *
   * @return string
   */
  public function getSexe() : string {
    return $this->sexe;
  }



  // Définition des méthodes (ou fonctions membres) de Personne
  // Ce sont des actions disponibles pour chaque instance. Leur nom contient presque toujours un verbe
  /**
   * parler Dire bonjour
   *
   * @return void
   */
  public function parler() { // Public autorise l'accès depuis n'importe où
    switch ($this->getNationalite())
    {
      case 'fr':
        echo 'Bonjour';
        break;
      case 'es':
        echo 'Hola';
        break;
      case 'it':
        echo 'Ma qué';
        break;
      default:
        echo 'Hello';
        break;
    }
    echo ', j\'ai ' . $this->getAge() . ' ans<br>';
  }

  /**
   * vieillir Augmenter de 1 la valeur de la propriété $age
   *
   * @return void
   */
  public function vieillir() {
    $this->age++;
  }
}