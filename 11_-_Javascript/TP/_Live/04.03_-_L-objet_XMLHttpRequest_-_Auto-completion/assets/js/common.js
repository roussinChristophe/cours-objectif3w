/**
 * getXHR -
 * @returns
 */
function getXHR() {
  var xhr;

  if(window.XMLHttpRequest) {
    return new XMLHttpRequest();
  } else if(window.ActiveXObject) {
    try {
      return new ActiveXObject('Msxml2.XMLHTTP');
    } catch(e) {
      return new ActiveXObject('Microsoft.XMLHTTP');
    }
  } else {
    return false;
  }
}