window.addEventListener('load', function(e) { // On attend le chargement complet de la page
  var inputs = document.querySelectorAll('.input'); // On récupère tous les éléments de classe "input"
  for(var i = 0; i<inputs.length; i++) { // Pour chaque élément de la collection,
    placeholder(inputs[i]);

    // switch(inputs[i].nodeName) {
    //     case 'SELECT': // Cas des listes déroulantes
    //         break;
    //     default:
    //         inputs[i].addEventListener('focus', listenerFct); // On ajoute l'écouteur d'événement appliqué au déclencheur "focus"
    // }
    inputs[i].addEventListener('focus', listenerFct); // On ajoute l'écouteur d'événement appliqué au déclencheur "focus"
    /**
    * La méthode "select" ne fonctionnant pas sur tous les champs, on va supprimer le listener dans certains cas
    **/
    switch(inputs[i].nodeName) {
      case 'SELECT': // Cas des listes déroulantes
        inputs[i].removeEventListener('focus', listenerFct); // On supprime l'écouteur d'événement appliqué au déclencheur "focus"
        break;
    }

    inputs[i].addEventListener('blur', function (e) { // On ajoute l'écouteur d'événement appliqué au déclencheur "blur"
      placeholder(this);
      
      if(e.target.hasAttribute('required')) {
        var err = e.target.parentNode.querySelector('.error');
        console.log(err);
        if(err) {
          err.parentNode.removeChild(err);
        }

        var xhr = getXHR();
        xhr.open(e.target.parentNode.parentNode.parentNode.getAttribute('method'), e.target.parentNode.parentNode.parentNode.getAttribute('action'));
        xhr.onreadystatechange = function () {
          switch(xhr.readyState) {
            case 4:
              var msg = document.createElement('div');
              msg.classList = 'error';
              msg.style.marginTop = '50px';
              
              if(xhr.status==200) {
                msg.innerHTML = xhr.responseText;
              } else {
                msg.innerHTML = xhr.statusText;
              }

              if(msg.innerHTML!='') {
                e.target.parentNode.appendChild(msg);
              }
              break;
          }
        };
      }

      if(e.target.parentNode.parentNode.parentNode.getAttribute('method').toUpperCase() == 'POST') {
        let frm = new FormData();
        frm.append(e.target.name, e.target.value);
        frm.append('blur', e.target.name);
        xhr.send(frm);
      }
    });
  }

  var forms = document.getElementsByTagName('form'); // On récupère tous les éléments de type "form"

  for(var i = 0; i<forms.length; i++) { // Pour chaque élément de la collection,
    forms[i].addEventListener('submit', function (e) { // On ajoute l'écouteur d'événement appliqué au déclencheur "submit"
      e.preventDefault();

      var xhr = getXHR();
      xhr.open(e.target.getAttribute('method'), e.target.getAttribute('action'));
      xhr.onreadystatechange = function () {
        switch(xhr.readyState) {
          case 4:
            if(xhr.status==200) {
              e.target.querySelector('.response').innerHTML = xhr.responseText;
            } else {
              e.target.querySelector('.response').innerHTML = xhr.statusText;
            }
            break;
          default:
            e.target.querySelector('.response').innerHTML = 'Envoi en cours ...';
        }
      };

      if(e.target.getAttribute('method').toUpperCase() == 'POST') {
        xhr.send(new FormData(e.target));
      }
    });
  }
});