/**
 * placeholder -
 * @param node input
 * @return
 */
function placeholder(input) {
  var classes = input.getAttribute('class').split(' '); // On sépare les valeurs de l'attribut "class" dans un tableau
  var selected = classes.indexOf('selected'); // On récupère l'indice pour lequel on trouve la valeur "selected"

  if(input.value!='' && selected==-1) // Si la valeur du champs est différente de vide et que la class "selected" n'existe pas encore,
    classes.push('selected'); // On ajoute la class dans le tableau
  else if(input.value=='' && selected!=-1) // Sinon, si la valeur du champs est vide et que la class "selected" existe,
    classes.splice(selected, 1); // On supprime la class du tableau

  input.setAttribute('class', classes.join(' ')); // On recompose la chaîne de caractère depuis le tableau pour l'affecter à l'attribut "class"
}

/**
 * listenerFct -
 * @param event e
 * @return
 */
var listenerFct = function (e) {
  this.select();
  placeholder(this);
};

/**
 * getXHR -
 * @returns
 */
function getXHR() {
  var xhr;

  if(window.XMLHttpRequest) {
    return new XMLHttpRequest();
  } else if(window.ActiveXObject) {
    try {
      return new ActiveXObject('Msxml2.XMLHTTP');
    } catch(e) {
      return new ActiveXObject('Microsoft.XMLHTTP');
    }
  } else {
    return false;
  }
}