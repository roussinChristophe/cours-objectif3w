<?php
function verifRequired($send, $required) {
  $errors = array();
  foreach($required as $field) {
    if(!isset($send[$field]) || empty($send[$field])) {
      $errors[] = $field;
    }
  }

  return $errors;
}

if(!empty($_POST['blur'])) {
  $key = $_POST['blur'];
  if(empty($_POST[$key])) {
    echo 'Veuillez remplir ce champ';
  } else {
    echo '';
  }
} else {
  $required = [
    'user_nom',
    'user_email',
    'user_tel',
    'user_age',
    'user_role',
    'user_news',
    'user_choice'
  ];

  $err = verifRequired($_POST, $required);
  if(empty($err)) {
    if(filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
      if(mail('webmaster@objectif3w.com', 'Contact depuis le formulaire', 'Message')) {
        echo 'Message envoyé';
      } else {
        echo 'Erreur dans l\'envoi du message';
      }
    } else {
      echo 'Mauvais format d\'email';
    }
  } else {
    echo 'Veuillez remplir tous les champs obligatoires : ' . implode(',', $err);
  }
}
