window.addEventListener('load', function () {
  var list = this.document.querySelector('#list-countries');

  var option = document.createElement('option');
  option.innerText = 'Chargement en cours ...';
  list.appendChild(option);
  list.disabled = "disabled";

  var xhr = getXHR();
  xhr.open('GET', 'http://localhost/www/201805/_Live/04.02_-_L-objet_XMLHttpRequest_-_Liste_chainee/dist/api.php?what=countries');
  xhr.onreadystatechange = function () {
    switch(xhr.readyState) {
      case 4:
        if(xhr.status==200) {
          var result = JSON.parse(xhr.responseText);
          switch(result._err) {
            case 'db':
              alert(result.msg);
              break;
            case 'ok':
              for(let i = 0; i < result.msg.length; i++) {
                let option = document.createElement('option');
                option.value = result.msg[i].pays_id;
                option.innerText = result.msg[i].pays_nom;
                list.appendChild(option);
              }
              list.removeChild(list.firstElementChild);
              list.disabled = false;
              break;
          }
        } else {
          alert(xhr.statusText);
        }
        break;
    }
  };
  xhr.send();

  list.addEventListener('change', function (ev) {
    if(document.querySelector('#list-cities')) {
      document.querySelector('#list-cities').parentNode.removeChild(document.querySelector('#list-cities').previousElementSibling);
      document.querySelector('#list-cities').parentNode.removeChild(document.querySelector('#list-cities'));
    }
    if(document.querySelector('#list-counties')) {
      document.querySelector('#list-counties').parentNode.removeChild(document.querySelector('#list-counties').previousElementSibling);
      document.querySelector('#list-counties').parentNode.removeChild(document.querySelector('#list-counties'));
    }

    var label = document.createElement('label');
    label.for = 'list-counties';
    label.innerText = JSON.parse(ev.target.getAttribute('data-target')).label;

    var select = document.createElement('select');
    select.classList = 'list';
    select.id = 'list-counties';
    select.name = 'counties';
    select.setAttribute('data-target', '{"name":"city","label":"Villes"}');

    ev.target.parentNode.appendChild(label);
    ev.target.parentNode.appendChild(select);

    var option = document.createElement('option');
    option.innerText = 'Chargement en cours ...';
    select.appendChild(option);
    select.disabled = "disabled";

    var xhr = getXHR();
    xhr.open('GET', 'http://localhost/www/201805/_Live/04.02_-_L-objet_XMLHttpRequest_-_Liste_chainee/dist/api.php?what=counties&id=' + ev.target.value);
    xhr.onreadystatechange = function () {
      switch(xhr.readyState) {
        case 4:
          if(xhr.status==200) {
            var result = JSON.parse(xhr.responseText);
            switch(result._err) {
              case 'db':
                alert(result.msg);
                break;
              case 'ok':
                for(let i = 0; i < result.msg.length; i++) {
                  let option = document.createElement('option');
                  option.value = result.msg[i].departement_code;
                  option.innerText = result.msg[i].departement_nom;
                  select.appendChild(option);
                }
                select.removeChild(select.firstElementChild);
                select.disabled = false;
                break;
            }
          } else {
            alert(xhr.statusText);
          }
          break;
      }
    };
    xhr.send();

    select.addEventListener('change', function (ev) {
      if(document.querySelector('#list-cities')) {
        document.querySelector('#list-cities').parentNode.removeChild(document.querySelector('#list-cities').previousElementSibling);
        document.querySelector('#list-cities').parentNode.removeChild(document.querySelector('#list-cities'));
      }

      var label = document.createElement('label');
      label.for = 'list-cities';
      label.innerText = JSON.parse(ev.target.getAttribute('data-target')).label;

      var select = document.createElement('select');
      select.classList = 'list';
      select.id = 'list-cities';
      select.name = 'cities';

      ev.target.parentNode.appendChild(label);
      ev.target.parentNode.appendChild(select);

      var option = document.createElement('option');
      option.innerText = 'Chargement en cours ...';
      select.appendChild(option);
      select.disabled = "disabled";

      var xhr = getXHR();
      xhr.open('GET', 'http://localhost/www/201805/_Live/04.02_-_L-objet_XMLHttpRequest_-_Liste_chainee/dist/api.php?what=cities&id=' + ev.target.value);
      xhr.onreadystatechange = function () {
        switch(xhr.readyState) {
          case 4:
            if(xhr.status==200) {
              var result = JSON.parse(xhr.responseText);
              switch(result._err) {
                case 'db':
                  alert(result.msg);
                  break;
                case 'ok':
                  for(let i = 0; i < result.msg.length; i++) {
                    let option = document.createElement('option');
                    option.value = result.msg[i].ville_id;
                    option.innerText = result.msg[i].ville_nom_reel;
                    select.appendChild(option);
                  }
                  select.removeChild(select.firstElementChild);
                  select.disabled = false;
                  break;
              }
            } else {
              alert(xhr.statusText);
            }
            break;
        }
      };
      xhr.send();
    });
  });
});