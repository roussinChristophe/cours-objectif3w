<?php
if(!empty($_GET['what'])) {
  try {
    $db = new PDO('mysql:host=127.0.0.1;dbname=location;charset=utf8mb4', 'root', '', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
    switch($_GET['what']) {
      case 'countries':
        if(($req = $db->query('SELECT * FROM `pays` ORDER BY `pays_nom` ASC'))!==false) {
          echo json_encode(['_err'=>'ok', 'msg'=>$req->fetchAll(PDO::FETCH_ASSOC)]);
        } else {
          echo json_encode(['_err'=>'db', 'msg'=>'Data request']);
        }
        break;
      case 'counties':
        if(!empty($_GET['id']) && ctype_digit($_GET['id'])) {
          if(($req = $db->prepare('SELECT * FROM `departement` WHERE `departement_pays`=? ORDER BY `departement_nom` ASC'))!==false) {
            if($req->bindValue(1, $_GET['id'])) {
              if($req->execute()) {
                echo json_encode(['_err'=>'ok', 'msg'=>$req->fetchAll(PDO::FETCH_ASSOC)]);
              } else {
                echo json_encode(['_err'=>'db', 'msg'=>'Request execute']);
              }
            } else {
              echo json_encode(['_err'=>'db', 'msg'=>'Data binding']);
            }
          } else {
            echo json_encode(['_err'=>'db', 'msg'=>'Data request']);
          }
        }
        break;
      case 'cities':
        if(!empty($_GET['id']) && ctype_digit($_GET['id'])) {
          if(($req = $db->prepare('SELECT * FROM `ville` WHERE `ville_departement`=? ORDER BY `ville_nom_reel` ASC'))!==false) {
            if($req->bindValue(1, $_GET['id'])) {
              if($req->execute()) {
                echo json_encode(['_err'=>'ok', 'msg'=>$req->fetchAll(PDO::FETCH_ASSOC)]);
              } else {
                echo json_encode(['_err'=>'db', 'msg'=>'Request execute']);
              }
            } else {
              echo json_encode(['_err'=>'db', 'msg'=>'Data binding']);
            }
          } else {
            echo json_encode(['_err'=>'db', 'msg'=>'Data request']);
          }
        }
        break;
    }
  } catch(PDOException $e) {
    echo json_encode(['_err'=>'db', 'msg'=>'Connection']);
  }
}