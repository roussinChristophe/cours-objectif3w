<?php
require_once( '../lib/SRequest.php' ); // On charge le singleton de requête GET/POST.

/**
 * isRequiredPassed -
 * @param   array   $required
 *          array   $fields
 * @return  bool
**/
function isRequiredPassed( $required, $fields ) {
    foreach( $required as $item ) :
        if( !( isset( $fields[$item] ) && !empty( $fields[$item] ) ) )
            return false;
    endforeach;

    return true;
}

/**
 * On définit le caractère de saut de ligne en fonction du noyau de système (Pour rappel : le saut de ligne est différent entre les systèmes Windows et UNIX.).
 * La constante PHP_EOL n'existant que depuis PHP5, il faut donc tester avant tout si elle est définie et la définir si ce n'est pas le cas.
**/
if( !defined( 'PHP_EOL') ) : // Si la constante PHP_EOL n'existe pas (http://php.net/manual/fr/reserved.constants.php),
    if( strtoupper( substr( PHP_OS, 0, 3 ) ) == 'WIN' ) : // Si la version du système d'exploitation (fournie par la constantes pré-définie PHP_OS) correspond à un noyau Windows,
        define( 'PHP_EOL', "\r\n" ); // On définit la constante avec les caractères Windows.
    else : // Sinon,
        define( 'PHP_EOL', "\n" ); // On définit la constante avec les caractères UNIX.
    endif;
endif;

/* ---------- Initialisation des messages ---------- */
$out['_err'] = 'unknown';
$out['message'] = '<span class="title">Erreur inconnue !</span><br />Veuillez réessayer dans un moment ou contacter le service technique';
/* ---------- Initialisation des variables de résultats ---------- */
$datas = array();
$_SEND = count( SRequest::getInstance()->get() )>0 ? SRequest::getInstance()->get() : ( count( SRequest::getInstance()->post() )>0 ? SRequest::getInstance()->post() : null );

/* ---------- Tests de validité ---------- */
$required = ['user_lastname', 'user_mail', 'message_object', 'user_news'];

if( isRequiredPassed( $required, $_SEND ) ) : // Si tous les champs requis sont présents,
    if( filter_var( $_SEND['user_mail'], FILTER_VALIDATE_EMAIL ) ) : // Si l'e-mail est au bon format,
        if( isset( $_SEND['spam_gender'] ) && $_SEND['spam_gender']=='Ne pas remplir' && isset( $_SEND['spam_url'] ) && $_SEND['spam_url']=='' ) : // Si le message n'est pas un spam,
            // On renseigne les messages de suivi et de log avec les valeurs d'erreur.
            $out['_err'] = 'sendmail_error';
            $out['message'] = '<span class="title">Echec de l\'envoi !</span><br />Un problème s\'est produit pendant l\'envoi du message !<br />Veuillez nous excuser pour ce désagrément';

            /* ---------- Traitement de la requête ---------- */
            $contenuTxt = 'Vous avez reçu un message depuis votre formulaire le ' . date( 'd/m/Y' ) . ' à ' . date( 'H:i:s' ) . PHP_EOL . '------------------------------------------------------------------' . PHP_EOL . PHP_EOL . 'Objet : ' . htmlentities( $_SEND['message_object'] ) . PHP_EOL . htmlentities( $_SEND['message_body'] );
            $contenuHtml = '<html><head></head><body><table style="width:100%;"><tr><td colspan="2">Vous avez reçu un message depuis votre formulaire le ' . date( 'd/m/Y' ) . ' à ' . date( 'H:i:s' ) . '<br /><hr /><br /></td></tr><tr><td width="50"><strong>Expéditeur</strong></td><td>' . ( isset( $_SEND['user_firstname'] ) ? htmlentities( ucfirst( $_SEND['user_firstname'] ) ) . ' ' : '' ) . htmlentities( strtoupper( $_SEND['user_lastname'] ) ) . '<br /></td></tr><tr><td width="50"><strong>Objet</strong></td><td>' . htmlentities( $_SEND['message_object'] ) . '<br /></td></tr><tr><td colspan="2">' . nl2br( $_SEND['message_body'] ) . '</td></tr><tr><td colspan="2"><hr /></td></tr>' . ( isset( $_SEND['user_news'] ) && $_SEND['user_news']=='Oui' ? '<tr><td colspan="2">Souhaite recevoir la newsletter</td></tr>' : '<tr><td colspan="2">Ne souhaite pas recevoir la newsletter</td></tr>' ) . '</table></body></html>';

            $boundary = '-----=' . md5( uniqid( microtime(), TRUE ) );

            $headers = 'From: noreply@objectif3w.com' . PHP_EOL;
            $headers .= 'MIME-Version: 1.0' . PHP_EOL;
            $headers .= 'Priority: normal' . PHP_EOL;
            $headers .= 'Reply-To: ' . $_SEND['user_mail'] . PHP_EOL;
            $headers .= 'X-Mailer: PHP/' . phpversion() . PHP_EOL;
            $headers .= 'X-Priority: 3' . PHP_EOL;

            $headers .= "Content-Type: multipart/alternative; boundary=\"$boundary\"" . PHP_EOL;

            $message = PHP_EOL . '--' . $boundary . PHP_EOL;
            $message .= 'Content-Type: text/html; charset=utf-8' . PHP_EOL;
            $message .= 'Content-Transfer-Encoding: 8bit' . PHP_EOL;
            $message .= PHP_EOL . $contenuHtml . PHP_EOL;
            $message .= PHP_EOL . '--' . $boundary . '--' . PHP_EOL;
            $message .= 'Content-Type: text/plain; charset=utf-8' . PHP_EOL;
            $message .= 'Content-Transfer-Encoding: quoted-printable' . PHP_EOL;
            $message .= PHP_EOL . $contenuTxt . PHP_EOL;
            $message .= PHP_EOL . '--' . $boundary . '--' . PHP_EOL;
            $message .= PHP_EOL . '--' . $boundary . '--' . PHP_EOL;

            // if( mail( 'd.tivelet@objectif3w.com', htmlentities( $_SEND['message_object'] ), $message, $headers ) ) :
                // On renseigne les messages de suivi et de log avec les valeurs de réussite.
                $out['_err'] = 'reponse_ok';
                $out['message'] = '<span class="title">E-mail envoyé !</span><br />Votre message a été envoyé correctement !<br />Nous vous répondrons dans les plus brefs délais';
            // endif;
        else :
            // On renseigne les messages de suivi et de log avec les valeurs d'erreur.
            $out['_err'] = 'spam';
            $out['message'] = '<span class="title">Envoi interrompu !</span><br />Vous avez été identifié comme spam ! Votre e-mail n\'a pas été envoyé.<br />Si cette identification est incorrecte, veuillez nous en excuser et revérifier votre saisie';
        endif;
    else :
        // On renseigne les messages de suivi et de log avec les valeurs d'erreur.
        $out['_err'] = 'wrong_format';
        $out['message'] = '<span class="title">Envoi interrompu !</span><br />Veuillez renseigner une adresse e-mail correcte';
    endif;
else :
    // On renseigne les messages de suivi et de log avec les valeurs d'erreur.
    $out['_err'] = 'no_param';
    $out['message'] = '<span class="title">Requête incomplète !</span><br />Veuillez renseigner correctement tous les champs obligatoires';

    if( !isset( $_SEND['global'] ) ) :
        foreach( $_SEND as $key => $value) :
            switch( $key ) :
                case 'ajax':
                case 'global':
                    break;
                case 'tocheck':
                    if( $value==true ) :
                        // On renseigne les messages de suivi et de log avec les valeurs d'erreur.
                        $out['_err'] = 'required';
                        $out['message'] = '<span class="title">Champ obligatoire !</span><br />Veuillez renseigner une valeur';
                    else :
                        // On renseigne les messages de suivi et de log avec les valeurs de réussite.
                        $out['_err'] = 'reponse_ok';
                    endif;
                    break;
                case 'user_mail' :
                    if( $value=='' ) :
                        // On renseigne les messages de suivi et de log avec les valeurs d'erreur.
                        $out['_err'] = 'required';
                        $out['message'] = '<span class="title">Champ obligatoire !</span><br />Veuillez renseigner une valeur';
                    elseif( !filter_var( $value, FILTER_VALIDATE_EMAIL ) ) : // Si l'e-mail n'est au bon format,
                        // On renseigne les messages de suivi et de log avec les valeurs d'erreur.
                        $out['_err'] = 'format';
                        $out['message'] = '<span class="title">Format non valide !</span><br />Veuillez renseigner une valeur correcte';
                    else :
                        // On renseigne les messages de suivi et de log avec les valeurs de réussite.
                        $out['_err'] = 'reponse_ok';
                    endif;
                    break;
                default :
                    if( $value=='' ) :
                        // On renseigne les messages de suivi et de log avec les valeurs d'erreur.
                        $out['_err'] = 'required';
                        $out['message'] = '<span class="title">Champ obligatoire !</span><br />Veuillez renseigner une valeur';
                    else :
                        // On renseigne les messages de suivi et de log avec les valeurs de réussite.
                        $out['_err'] = 'reponse_ok';
                    endif;
            endswitch;
        endforeach;
    endif;
endif;
/* -------------------------------------------------------- */

if( isset( $_SEND['ajax'] ) ) :
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode( $out ); // On transforme les données en flux json.
else :
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0" />
        <title>Le Javascript - Mise en pratique</title>

        <style type="text/css">
            *,*::before,*::after {
                -webkit-box-sizing:border-box;
                -moz-box-sizing:border-box;
                -ms-box-sizing:border-box;
                -o-box-sizing:border-box;
                box-sizing:border-box;
            }
            :hover,:focus,:active { outline:none; }

            html { font-size:62.5%; }
            body{
                font-family:'Verdana';
                font-size:12px;font-size:1.2rem;
            }

            /* ------    formulaires    ------ */
                [data-id="result"] {
                    clear:both;
                    display:block;
                    margin:0;
                    z-index:10;
                }
                    [data-id="result"].block-alert {
                        display:block;
                        padding:15px 7px;padding:1.5rem .7rem;
                        margin-bottom:10px;margin-bottom:1rem;
                    }
                        [data-id="result"] .title {
                            color:rgb(199,4,119);
                            display:block;
                            font-size:16px;font-size:1.6rem;
                            font-weight:bolder;
                            margin-bottom:5px;margin-bottom:.5rem;
                            text-transform:uppercase;
                        }
        </style>
    </head>
    <body>
        <h1>Le Javascript - Mise en pratique</h1>
        <p><em></em></p>
        <hr />
        <h2>L'objet XMLHttpRequest</h2>
        <div class="block-alert" data-id="result"><?php echo $out['message']; ?></div>
        <a href="../../04_-_L_objet_XMLHttpRequest_-_Script_envoi_email.html">Retour au formulaire</a>
    </body>
</html>
<?php
endif;