/*
 * Propriétés de relation
 * ----------------
 * parentNode : renvoie le noeud parent
 * firstChild : renvoie le premier enfant d'un noeud
 * lastChild : renvoie le dernier enfant d'un noeud
 * childNodes : renvoie une liste de tous les noeuds enfant
 * children : renvoie une liste de tous les noeuds enfant n'étant pas de type texte
 * previousSibling : renvoie le noeud frère/soeur précédent d'un noeud
 * nextSibling : renvoie le noeud frère/soeur suivant d'un noeud
 * 
 * Propriétés d'état
 * ----------------
 * nodeName : indique le nom  du noeud sélectionné
 * nodeType : indique le type du noeud sélectionné
 * nodeValue : permet d'obtenir ou de changer la valeur d'un noeud de type texte
 */

// console.log(document.firstChild.nextSibling.children[0].firstChild.nextSibling.nextSibling.nextSibling);

/*
 * Méthodes d'accès au DOM
 */
// console.log(document.firstChild.nextSibling.childNodes[2].childNodes[1].firstChild.nextSibling.firstChild.nodeValue);
// document.firstChild.nextSibling.childNodes[2].childNodes[1].firstChild.nextSibling.firstChild.nodeValue = "Mon nouveau titre";
console.log(document.getElementById('title-lvl1').firstChild.nodeValue);
document.getElementById('title-lvl1').firstChild.nodeValue = "<span>Mon nouveau titre</span>";
console.log(document.getElementsByName('txt-test'));
console.log(document.getElementsByTagName('meta'));
console.log(document.querySelector('article h2'));
console.log(document.querySelector('#title-lvl2'));
console.log(document.querySelector('.mon-entete'));
console.log(document.querySelector('form[method="post"]'));
console.log(document.querySelectorAll('form[method="post"]'));
console.log(document.querySelectorAll('form[method="post"]:not(.none)'));
console.log(document.querySelector('form[method="post"]').querySelectorAll('[type="radio"]'));
console.log(document.querySelector('[data-toto="Tata"]'));

/*
 * Création d'élément
 */
// Insérer à la fin : conteneur.appendChild(ajout)
let p = document.createElement('p');
p.innerText = 'Ceci est un <b>paragraphe 1</b>';
p.id = 'monP';
// console.dir(p);
document.body.appendChild(p);

let p2 = document.createElement('p');
let txt = document.createTextNode('Ceci est un ');
p2.appendChild(txt);
let b = document.createElement('b');
let bTxt = document.createTextNode('paragraphe 2');
b.appendChild(bTxt);
p2.appendChild(b);
p2.id = 'monP';
// console.dir(p2);
document.body.appendChild(p2);

let p3 = document.createElement('p');
p3.innerHTML = 'Ceci est un <b>paragraphe 3</b>';
p3.id = 'monP3';
// console.dir(p3);
document.body.appendChild(p3);

// Insérer avant un autre élément à l'intérieur d'un conteneur : conteneur.insertBefore(ajout, avant)
let p4 = document.createElement('p');
p4.innerText = 'Je me place où je veux';
console.log(document.querySelector('.none input:first-child'));
document.querySelector('.none').insertBefore(p4, document.querySelector('.none input:last-child'));

/*
 * Suppression d'élément
 */
toRemove = document.querySelector('[data-toto="Tata"]');
myParent = toRemove.parentNode;
myParent.removeChild(toRemove);