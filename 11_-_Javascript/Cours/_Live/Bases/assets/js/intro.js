// maVariable = 15;

// console.log(maVariable);

// function maFct() {
//   var maVariable = 123;
//   // console.log(maVariable);
//   maFct2();
//   // console.log(maVariable);
// }
// function maFct2() {
//   var maVariable = 999;
//   // console.log(maVariable);
//   for(let maVariable = 0; maVariable <= 10; maVariable++) {
//     // console.log(maVariable);
//   }
  
//   for(let i=0; i<=6; i++) {
//     console.log(i);
//   }
//   // console.log(maVariable);
// }

// console.group('Mon groupe de test');


// var MaChaineDeCaracteres = 'c';
// var MaChaineDeCaracteres = "c";
// var MaChaineDeCaracteres = 'str';
// var MaChaineDeCaracteres = 'La valeur de ma variable est ' + MaChaineDeCaracteres;
// console.log(MaChaineDeCaracteres);
// var MaChaineDeCaracteres = 'str';
// var MaChaineDeCaracteres = "La valeur de ma variable est " + MaChaineDeCaracteres;
// console.log(MaChaineDeCaracteres);
// var MaChaineDeCaracteres = 'str';
// var MaChaineDeCaracteres = `La valeur de ma variable est ${MaChaineDeCaracteres}`;
// console.dir(MaChaineDeCaracteres);
// console.log(MaChaineDeCaracteres.length);


// console.groupEnd();

// var MonNombre = 3;
// if(isNaN("fxkhgfdlkhfd")) {
//   console.log('Not a number');
// } else {
//   console.log(MonNombre + parseInt("fxkhgfdlkhfd"));
// }
// var MonNombre = 15.4;

// var MonObjet = {};
// console.log(MonObjet);

// var MonBooleen = false;

// var MonNull = null;

// var MonUndefined;
// MonUndefined = undefined;


/**
 * Opérateurs arithmétiques
 * 
 * + : addition ... mais /!\ aussi pour la concaténation
 * -
 * *
 * /
 * %
 * ++
 * --
 * 
 * 
 * 
 * Opérateurs logiques
 * <
 * >
 * <=
 * >=
 * == : égalité de valeur
 * != : inégalité de valeur
 * ! : NON logique
 * && : ET logique
 * || : OU logique
 * ?: : opérateur conditionnel
 * === : égalité de valeur ET de type (identité)
 * !== : inégalité de valeur OU de type (non identité)
 * 
 * 
 * 
 * Opérateurs associatifs
 * = : affectation
 * x += y : x = x + y
 * x -= y : x = x - y
 * x *= y : x = x * y
 * x /= y : x = x / y
 * 
 * 
 * 
 * Opérateurs divers
 * void 
 * delete
 * typeof
 */

// if(typeof MonBooleen == 'boolean') {

// } else if(typeof MonBooleen == 'integer') {

// } else {
  
// }

// switch (typeof MonBooleen) {
//   case 'boolean':
//     break;
//   default:
// }

// for(i = 0; i <= 10; i + 2) {
  
// }

// a = 0;
// b = 0;
// while( a < 5 ) {
//   b = b + a;
//   a++;
// }

// a = 1;
// b = 1;
// do {
//   b = b + a;
// } while( b < 2 );


// var monTableau = [12, 15, 16];
// for(item in monTableau) {
//   console.log(item);
//   console.log(monTableau[item]);
// }

// for(item of monTableau) {
//   console.log(item);
// }

// for(item in navigator) {
//   console.log(item);
// }
// for(item in window) {
//   console.log(item);
// }
// for(item in document) {
//   console.log(item);
// }

// console.dir(document.forms[0]);


// var obj1 = new Object();
// obj1.prop1 = 'Prop 1';
// obj1.maProp2 = 'Prop 2';
// obj1.method1 = function () { alert(this.prop1); };
// console.dir(obj1);

// var obj2 = {
//   prop21: 'Prop 21',
//   maProp22: 'Prop 22',
//   method1: function () {
//     alert(this.maProp22);
//   },
//   monObj1 : obj1
// };
// console.dir(obj2);

// obj2.monObj1.method1();

// console.dir(window);


// maChaine = 'https://www.objectif3w.com'; // let maChaine = new String
// console.log(maChaine.length);
// console.log(maChaine.substring(0,5));
// console.log(maChaine.indexOf('www', 0));

// monTableau = [45, 'foo', false, null, 15.4]; // let monTableau = new Array
// console.log(monTableau);
// console.log(monTableau.push(12, 15, 'bla'));
// console.log(monTableau);
// last = monTableau.pop();
// console.log(last);
// console.log(monTableau);
// first = monTableau.shift();
// console.log(first);
// console.log(monTableau);
// monTableau.unshift('premier', 'deuxieme');
// console.log(monTableau);
// monTableau.sort();
// console.log(monTableau);

// for(let i=0; i < monTableau.length; i++) {
//   console.log(monTableau[i]);
// }

// for(let item in monTableau) {
//   console.log(monTableau[item]);
// }

// for(let item of monTableau) {
//   console.log(item);
// }

// console.log(typeof monTableau);

// monTableauAss = {
//   jb: null,
//   alpha: '#000',
//   damien: 'pas drole'
// };
// for(let item in monTableauAss) {
//   console.log(item);
//   console.log(monTableauAss[item]);
// }
// console.log(typeof monTableauAss);


if(confirm('Voulez-vous continuer ?')) {
  let nombre = prompt('Indiquez un nombre :', 0);
  if(nombre!==null) {
    if(!isNaN(nombre)) {
      alert('Voici un message dans une boite de dialogue : votre nombre est ' + nombre);
    } else {
      alert('J\'avais dit un nombre !!!');
    }
  }
}
