<?php
header( 'Content-Type: application/json' );

$_codeErr = 'unknown';
$_err = 'Une erreur est survenue pendant la requête !';
$login = 'admin';
$pwd = 'motdepasse';

if( isset( $_POST ) && array_key_exists( 'login', $_POST ) && array_key_exists( 'password', $_POST ) ) :
    if( isset( $_POST ) && array_key_exists( 'status', $_POST ) && $_POST['status']=='actif' ) :
        if( $_POST['login']!='' && $_POST['password']!='' ) :
            if( $_POST['login']==$login && $_POST['password']==$pwd ) :
                $_codeErr = 'ok';
                $_err = 'Utilisateur connecté';
            else :
                $_codeErr = 'id';
                $_err = 'Mauvais couple identifiant/mot de passe';
            endif;
        else :
            $_err = '';
            if( $_POST['login']=='' ) :
                $_codeErr = 'login';
                $_err .= 'Veuillez saisir un nom d\'utilisateur<br />';
            endif;
            if( $_POST['password']=='' ) :
                if( $_codeErr!='unknown' ) $_codeErr = 'all';
                else $_codeErr = 'password';
                $_err .= 'Veuillez saisir un mot de passe<br />';
            endif;
        endif;
    else :
        $_err = 'Connexion désactivée';
    endif;
else :
    if( isset( $_GET ) && array_key_exists( 'login', $_GET ) && array_key_exists( 'password', $_GET ) ) :
        if( isset( $_GET ) && array_key_exists( 'status', $_GET ) && $_GET['status']=='actif' ) :
            if( $_GET['login']!='' && $_GET['password']!='' ) :
                if( $_GET['login']==$login && $_GET['password']==$pwd ) :
                    $_codeErr = 'ok';
                    $_err = 'Utilisateur connecté';
                else :
                    $_err = 'Mauvais couple identifiant/mot de passe';
                endif;
            else :
                $_err = '';
                if( $_GET['login']=='' ) :
                    $_codeErr = 'login';
                    $_err .= 'Veuillez saisir un nom d\'utilisateur<br />';
                endif;
                if( $_GET['password']=='' ) :
                    $_codeErr = 'password';
                    $_err .= 'Veuillez saisir un mot de passe<br />';
                endif;
            endif;
        else :
            $_err = 'Connexion désactivée';
        endif;
    endif;
endif;

$_err = array( 'code'=>$_codeErr, 'msg'=>$_err );

echo json_encode( $_err );