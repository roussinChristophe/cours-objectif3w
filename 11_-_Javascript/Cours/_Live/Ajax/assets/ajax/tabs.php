<?php
$out['tab'] = array();

switch( $_POST['tab'] ) :
    case 1:
        $out['tab'] = array(
            'id' => 1,
            'title' => 'Onglet 1',
            'description' => 'Lorem ipsum dolor sit amet'
        );
        break;
    case 2:
        $out['tab'] = array(
            'id' => 2,
            'title' => 'Onglet 2',
            'description' => 'Lorem ipsum dolor sit amet'
        );
        break;
    case 3:
        $out['tab'] = array(
            'id' => 3,
            'title' => 'Onglet 3',
            'description' => 'Lorem ipsum dolor sit amet'
        );
        break;
endswitch;

echo json_encode( $out );