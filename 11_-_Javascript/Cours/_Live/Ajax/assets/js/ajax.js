function ajax() {
  let xhr;

  if(window.XMLHttpRequest) {
    xhr = new XMLHttpRequest();
  } else if(window.ActiveXObject) {
    try {
      xhr = new ActiveXObject('Msxml2.XMLHTTP');
    } catch(e) {
      xhr = new ActiveXObject('Microsoft.XMLHTTP');
    }
  } else {
    alert('Votre navigateur ne gère pas le concept AJAX');
    return false;
  }

  return xhr;
}

/*
 * Propriétés
 * ----------
 * readyState : retourne l'état de la requète
 * onreadystatechange : gestionnaire d'événements lié à la requète HTTP
 * status : renvoie le code numérique de la réponse liée à la requète HTTP
 * statusText : renvoie le message de la réponse liée à la requète HTTP
 * responseText : réponse du serveur sous forme de chaîne de caractères
 * responseXML : réponse du serveur sous forme d'un document XML
 * 
 * Méthodes
 * ----------
 * open() : initialise la requète
 * send() : effectue la requète (avec éventuellement avec un envoi de données)
 * getAllResponseHeaders() : renvoie l'ensemble de l'en-tête de la réponse HTTP (sous forme de chaîne de caractères)
 * abort() : annule la requète 
 */

document.getElementById('load').addEventListener('click', function (ev) {
  var xhr = ajax();
  xhr.onreadystatechange = function() {
    console.log(xhr);
    if(xhr.readyState==4 && xhr.status==200) {
      var resp = JSON.parse(xhr.responseText);
      console.log(resp);
      alert('Mon controlleur est : ' + resp.c);
    }
  };

  ev.preventDefault();


  xhr.open('POST', 'http://localhost/www/201805/_Live/Ajax/api/test.php?toto=tata');
  // xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  // xhr.send('c=user&a=login');
  let form = document.getElementById('frm');
  // let inputs = form.getElementsByTagName('input');
  // let str = '';
  // for(input in inputs) {
  //   str = str + inputs[input].name + '=' + inputs[input].value . '&';
  // }
  let fd = new FormData(form);
  xhr.send(fd);
});