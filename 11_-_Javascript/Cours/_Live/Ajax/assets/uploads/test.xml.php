<?php
header( 'Content-Type: text/xml' );

echo '<?xml version="1.0" encoding="utf-8" ?>';
echo '<root>';
echo '    <item name="Item 1">Blabla 1</item>';
echo '    <item name="Item 2">Blabla 2</item>';
echo '    <item name="Item 3">Blabla 3</item>';
echo '    <item name="Item 4">Blabla 4</item>';
echo '    <item name="Item 5">Blabla 5</item>';
echo '</root>';