<?php
header( 'Content-Type: text/plain' );

$str = '';

/**
 * --------------------------------------------------
 * ENVOI GET
 * --------------------------------------------------
**/
if( isset( $_GET['action'] ) ) {
    $str = $_GET['action'];
}
if( isset( $_GET['prenom'] ) ) {
    if( $str=='' ) {
        $str = $_GET['prenom'];
    } else {
        $str .= ' ' . $_GET['prenom'];
    }
}
if( isset( $_GET['nom'] ) ) {
    if( $str=='' ) {
        $str = $_GET['nom'];
    } else {
        $str .= ' ' . $_GET['nom'];
    }
}
/** -------------------------------------------------- **/

/**
 * --------------------------------------------------
 * ENVOI POST
 * --------------------------------------------------
**/
if( isset( $_POST['action'] ) ) {
    $str = $_POST['action'];
}
if( isset( $_POST['prenom'] ) ) {
    if( $str=='' ) {
        $str = $_POST['prenom'];
    } else {
        $str .= ' ' . $_POST['prenom'];
    }
}
if( isset( $_POST['nom'] ) ) {
    if( $str=='' ) {
        $str = $_POST['nom'];
    } else {
        $str .= ' ' . $_POST['nom'];
    }
}
/** -------------------------------------------------- **/

echo $str;