window.addEventListener('load', function() {
  var link = document.createElement('a');
  link.href = 'https://www.google.fr';
  link.id = 'lien';
  link.innerText = 'Cliquez ici';

  link.addEventListener('click', function(ev) {
    if(!confirm('Etes vous sûr de vouloir aller ailleurs ?')) {
      alert('Ok');
      ev.preventDefault();
    }
  });

  document.body.appendChild(link);

  document.getElementById('lvl1').addEventListener('click', function(ev) {
    console.log(ev.target);
  });

  for(let div of document.querySelectorAll('div')) {
    div.addEventListener('click', function(ev) {
      alert('Cliqué');
      console.log(ev.target);
      this.style.backgroundColor = "red";
      console.log(this);
      // ev.stopPropagation();
    }, true);
  }
});
