function getXHR() {
    var xhr = null;

    if( window.XMLHttpRequest )
        xhr = new XMLHttpRequest;
    else if( window.ActiveXObject )
        try {
            xhr = new ActiveXObject( 'Msxml2.XMLHTTP' );
        } catch( e ) { // Sinon,
            xhr = new ActiveXObject( 'Microsoft.XMLHTTP' );
        }
    else
        alert( 'Votre navigateur ne supporte pas la technologie AJAX !\nVeuillez le mettre à jour pour profiter de nos fonctionnalités' );

    return xhr;
}



window.addEventListener( 'load', function () {
    // for( var i = 0; i<document.querySelectorAll( 'div[id^="tab"]' ).length; i++ ) {
    //     if( i!==0 )
    //         document.querySelectorAll( 'div[id^="tab"]' )[i].style.display = 'none';
    // }
    for( var tab of document.querySelectorAll( 'a[href^="#tab"]' ) ) {
        tab.addEventListener( 'click', function ( e ) {
            var container = e.target.parentNode;
            var parentId = container.id;
            for( var div of document.querySelectorAll( '#' + parentId + ' div[id^="tab"]' ) ) {
                // div.style.display = 'none';
                div.parentNode.removeChild( div );
            }

            // var content = document.querySelector( e.target.href.substring( e.target.href.search( '#' ), e.target.href.length ) );
            // content.style.display = 'block';

            var xhr = getXHR();
            xhr.onreadystatechange = function () {
                switch( xhr.readyState ) {
                    case 4:
                        if( xhr.status==200 ) {
                            // container.innerHTML += response;
                            // container.innerHTML = container.innerHTML + response;
                            var response = JSON.parse( xhr.responseText );
                            var content = response.tab;
                            var wrapper = document.createElement( 'div' );
                            wrapper.id = "tab' + content.id + '";
                            wrapper.innerHTML += '<h2>' + content.title + '</h2><p>' + content.description + '</p>';

                            container.appendChild( wrapper );
                        }
                        break;
                }
            };
            xhr.open( 'POST', 'assets/ajax/tabs.php', true );
            xhr.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
            xhr.send( 'tab=' + e.target.href.substring( e.target.href.search( '#' ) + 4, e.target.href.length ) );

            e.preventDefault();
        } );
    }
} );