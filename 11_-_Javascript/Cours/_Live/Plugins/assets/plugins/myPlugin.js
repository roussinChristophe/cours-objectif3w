(function () {
  /*
   * Constructor
   */
  this.myPlugin = function (selector, options = {}) {
    /*
     * Private properties
     */
    var defaults = {
      color: '#FF0000',
      text: 'Texte par défaut'
    };

    options = (options && typeof options==='object' ? extendedDefault(defaults, options) : defaults);

    /*
     * Privileged methods ... /!\ NOT PROTECTED : PUBLIC WITH PRIVATE PROPERTIES ACCESS
     */
    this.getOptions = function () {
      return options;
    };

    /*
     * Private methods
     */
    function extendedDefault(source, properties) {
      for(let property in properties) {
        if(properties.hasOwnProperty(property)) {
          source[property] = properties[property];
        }
      }

      return source;
    }

    /*
     * Instructions
     */
    this.node = document.querySelector(selector);
    
    for(let opt in options) {
      if(opt == 'text') {
        this.node.innerText = options[opt];
      } else {
        this.node.style[opt] = options[opt];
      }
    }
  };

  /*
   * Public methods
   */
  myPlugin.prototype.getRandomNumber = function () {
    return Math.random();
  };

  myPlugin.prototype.myFunctionText = function () {
    this.node.innerText = this.getOptions().text + ' : ' + this.getRandomNumber();
    return this;
  };

  myPlugin.prototype.myFunctionFinal = function () {
    this.node.innerText += prompt('Texte à ajouter');
    return this;
  };
})();
