<?php
class TunnelModel extends CoreModel {
  const TABLE = 'tunnel';
  const PREFIX = 't_';

  public function create(float $progres, int $ville_depart, int $ville_arrivee) {
    try {
      if(($req = $this->_db->prepare('INSERT INTO `tunnel`(`t_progres`, `t_villedepart_fk`, `t_villearrivee_fk`) VALUES (?, ?, ?)'))!==false) {
        if($req->bindValue(1, $progres)
          && $req->bindValue(2, $ville_depart, PDO::PARAM_INT)
          && $req->bindValue(3, $ville_arrivee, PDO::PARAM_INT)) {
            if($req->execute()) {
              $res = $this->_db->lastInsertId();
              $req->closeCursor();
              return $res;
            }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not create into the database', 10, $e);
    }
  }

  public function read(int $id = null) {
    try {
      if(empty($id)) {
        if(($req = $this->_db->query('SELECT * FROM `' . static::TABLE . '` ORDER BY `' . static::PREFIX . 'id` ASC'))!==false) {
          $res = $req->fetchAll(PDO::FETCH_ASSOC);
          $req->closeCursor();
          return $res;
        }
      } else {
        if(($req = $this->_db->prepare('SELECT * FROM `' . static::TABLE . '` WHERE `' . static::PREFIX . 'id`=?'))!==false) {
          if($req->bindValue(1, $id, PDO::PARAM_INT)) {
            if($req->execute()) {
              $res = $req->fetchAll(PDO::FETCH_ASSOC);
              $req->closeCursor();
              return $res;
            }
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not read from the database', 10, $e);
    }
  }

  public function update(int $id, float $progres, int $ville_depart, int $ville_arrivee) {
    try {
      if(($req = $this->_db->prepare('UPDATE `tunnel` SET `t_progres`=?, `t_villedepart_fk`=?, `t_villearrivee_fk`=? WHERE `t_id`=?'))!==false) {
        if($req->bindValue(1, $progres)
          && $req->bindValue(2, $ville_depart, PDO::PARAM_INT)
          && $req->bindValue(3, $ville_arrivee, PDO::PARAM_INT)
          && $req->bindValue(4, $id, PDO::PARAM_INT)) {
          if($req->execute()) {
            $res = $this->_db->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not update the database', 10, $e);
    }
  }

  public function readByVille(int $id) {
    try {
      if(($req = $this->_db->prepare('SELECT * FROM `tunnel` WHERE `t_villedepart_fk`=:id OR `t_villearrivee_fk`=:id'))!==false) {
        if($req->bindValue('id', $id, PDO::PARAM_INT)) {
          if($req->execute()) {
            $res = $req->fetchAll(PDO::FETCH_ASSOC);
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not read from the database', 10, $e);
    }
  }
}