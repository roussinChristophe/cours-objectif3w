<?php
class CoreModel {
  protected $_db;



  public function __construct(string $engine, string $host, string $dbname, string $user, string $pwd, string $charset = 'utf8mb4', string $collate = 'utf8mb4_general_ci') {
    try {
      $this->_db = new PDO($engine . ':host=' . $host . ';dbname=' . $dbname . ';charset=' . $charset, $user, $pwd, array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
    } catch(PDOException $e) {
      throw new Exception('Can not access to the database', 10, $e);
    }
  }



  public function read(int $id = null) {
    try {
      if(empty($id)) {
        if(($req = $this->_db->query('SELECT * FROM `' . static::TABLE . '` ORDER BY `' . static::PREFIX . 'nom` ASC'))!==false) {
          $res = $req->fetchAll(PDO::FETCH_ASSOC);
          $req->closeCursor();
          return $res;
        }
      } else {
        if(($req = $this->_db->prepare('SELECT * FROM `' . static::TABLE . '` WHERE `' . static::PREFIX . 'id`=?'))!==false) {
          if($req->bindValue(1, $id, PDO::PARAM_INT)) {
            if($req->execute()) {
              $res = $req->fetchAll(PDO::FETCH_ASSOC);
              $req->closeCursor();
              return $res;
            }
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not read from the database', 10, $e);
    }
  }

  public function delete(int $id) {
    try {
      if(($req = $this->_db->prepare('DELETE FROM `'. static::TABLE .'` WHERE `'. static::PREFIX .'id`=?'))!==false) {
        if($req->bindValue(1, $id, PDO::PARAM_INT)) {
          if($req->execute()) {
            $res = $this->_db->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not delete from the database', 10, $e);
    }
  }
}