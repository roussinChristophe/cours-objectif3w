<?php
require_once('config/db.php');
require_once('autoload.php');


$ctrl = 'TaverneController';
if(!empty($_GET['c'])) {
  $ctrl = ucfirst($_GET['c']) . 'Controller';
}

if(class_exists($ctrl)) {
  $controller = new $ctrl;
} else {
  header('Location:404');
  exit;
}


if(!empty($_GET['a']) && $_GET['a']=='card') {
  $controller->card($_GET['id']);
} else {
  $controller->list();
}
