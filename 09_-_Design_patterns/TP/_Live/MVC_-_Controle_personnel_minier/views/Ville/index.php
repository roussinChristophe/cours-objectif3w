<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Villes</title>
  </head>
  <body>
    <ul>
      <?php foreach($villes as $ville) { ?>
      <li><a href="?c=ville&a=card&id=<?php echo $ville->getId(); ?>"><?php echo $ville->getNom(); ?></a></li>
      <?php } ?>
    </ul>
  </body>
</html>