<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tavernes</title>
  </head>
  <body>
    <ul>
      <?php foreach($tavernes as $taverne) { ?>
      <li><a href="?c=taverne&a=card&id=<?php echo $taverne->getId(); ?>"><?php echo $taverne->getNom(); ?></a></li>
      <?php } ?>
    </ul>
  </body>
</html>