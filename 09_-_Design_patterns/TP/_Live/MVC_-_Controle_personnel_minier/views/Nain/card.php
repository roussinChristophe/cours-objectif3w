<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nain</title>
  </head>
  <body>
    <h1><?php echo $nain->getNom(); ?></h1>
    <ul>
      <li>Longueur de barbe : <?php echo $nain->getBarbe(); ?></li>
      <li>Originaire de : <a href="?c=ville&a=card&id=<?php echo $nain->getVille()->getId(); ?>"><?php echo $nain->getVille()->getNom(); ?></a></li>
      <?php if(isset($taverne)) { ?><li>Boit dans : <a href="?c=taverne&a=card&id=<?php echo $taverne->getId(); ?>"><?php echo $taverne->getNom(); ?></a></li><?php } ?>
      <li>Membre du <?php if($nain->getGroupe()!==null) { ?><a href="?c=groupe&a=card&id=<?php echo $nain->getGroupe()->getId(); ?>">groupe n° <?php echo $nain->getGroupe()->getId(); ?></a></li><?php } else { echo 'aucun groupe'; } ?>
    </ul>
  </body>
</html>