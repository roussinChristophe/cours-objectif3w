<?php
class Groupe extends Core {
  private $_id;
  private $_debuttravail;
  private $_fintravail;
  private $_taverne;
  private $_tunnel;



  public function setId(int $id): self {
    $this->_id = $id;
    return $this;
  }
  
  public function setDebuttravail(string $time): self {
    $this->_debuttravail = $time;
    return $this;
  }
  
  public function setFintravail(string $time): self {
    $this->_fintravail = $time;
    return $this;
  }
  
  public function setTaverne($taverne): self {
    $this->_taverne = $taverne;
    return $this;
  }
  
  public function setTunnel($tunnel): self {
    $this->_tunnel = $tunnel;
    return $this;
  }


  public function getId(): int {
    return $this->_id;
  }
  
  public function getDebuttravail(): string {
    return $this->_debuttravail;
  }
  
  public function getFintravail(): string {
    return $this->_fintravail;
  }
  
  public function getTaverne() {
    return $this->_taverne;
  }
  
  public function getTunnel() {
    return $this->_tunnel;
  }
}