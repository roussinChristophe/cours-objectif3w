<?php
class Taverne extends Core {
  private $_id;
  private $_nom;
  private $_chambres;
  private $_chambresLibres;
  private $_blonde;
  private $_brune;
  private $_rousse;
  private $_ville;

  public function setId(int $id): self {
    $this->_id = $id;
    return $this;
  }

  public function setNom(string $nom): self {
    $this->_nom = $nom;
    return $this;
  }
  
  public function setChambres(int $chambres): self {
    $this->_chambres = $chambres;
    return $this;
  }
  
  public function setChambresLibres(int $chambres): self {
    $this->_chambresLibres = $chambres;
    return $this;
  }
  
  public function setBlonde(bool $blonde): self {
    $this->_blonde = $blonde;
    return $this;
  }
  
  public function setBrune(bool $brune): self {
    $this->_brune = $brune;
    return $this;
  }
  
  public function setRousse(bool $rousse): self {
    $this->_rousse = $rousse;
    return $this;
  }
  
  public function setVille($ville): self {
    $this->_ville = $ville;
    return $this;
  }



  public function getId(): int {
    return $this->_id;
  }
  
  public function getNom(): string {
    return $this->_nom;
  }
  
  public function getChambres(): int {
    return $this->_chambres;
  }
  
  public function getChambresLibres(): int {
    return $this->_chambresLibres;
  }
  
  public function getBlonde(): bool {
    return $this->_blonde;
  }
  
  public function getBrune(): bool {
    return $this->_brune;
  }
  
  public function getRousse(): bool {
    return $this->_rousse;
  }
  
  public function getVille() {
    return $this->_ville;
  }
}