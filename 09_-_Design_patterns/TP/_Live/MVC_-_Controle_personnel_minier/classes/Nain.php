<?php
class Nain extends Core {
  private $_id;
  private $_nom;
  private $_barbe;
  private $_groupe;
  private $_ville;



  public function setId(int $id): self {
    $this->_id = $id;
    return $this;
  }

  public function setNom(string $nom): self {
    $this->_nom = $nom;
    return $this;
  }

  public function setBarbe(float $barbe): self {
    $this->_barbe = $barbe;
    return $this;
  }

  public function setGroupe($groupe): self {
    $this->_groupe = $groupe;
    return $this;
  }

  public function setVille($ville): self {
    $this->_ville = $ville;
    return $this;
  }



  public function getId(): int {
    return $this->_id;
  }
  
  public function getNom(): string {
    return $this->_nom;
  }
  
  public function getBarbe(): float {
    return $this->_barbe;
  }
  
  public function getGroupe() {
    return $this->_groupe;
  }
  
  public function getVille() {
    return $this->_ville;
  }
}