<?php
class Tunnel extends Core {
  private $_id;
  private $_progres;
  private $_villedepart;
  private $_villearrivee;



  public function setId(int $id): self {
    $this->_id = $id;
    return $this;
  }
  
  public function setProgres(int $progres): self {
    $this->_progres = $progres;
    return $this;
  }
  
  public function setVilledepart($ville): self {
    $this->_villedepart = $ville;
    return $this;
  }
  
  public function setVillearrivee($ville): self {
    $this->_villearrivee = $ville;
    return $this;
  }



  public function getId(): int {
    return $this->_id;
  }

  public function getProgres(): int {
    return $this->_progres;
  }

  public function getVilledepart() {
    return $this->_villedepart;
  }

  public function getVillearrivee() {
    return $this->_villearrivee;
  }



  public function getStatus(): string {
    if($this->_progres==100) {
      return 'Ouvert';
    } else {
      return $this->_progres . '%';
    }
  }
}