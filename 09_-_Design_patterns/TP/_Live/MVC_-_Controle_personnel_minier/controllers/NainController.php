<?php
class NainController extends CoreController {
  const MODEL = 'Nain';

  public function list() {
    $class = static::MODEL;

    $datas = $this->_model->read();
    
    foreach($datas as $data) {
      $nain = new $class($data);
      $nains[] = $nain;
    }
    include('views/' . $class . '/index.php');
  }



  public function card(int $id) {
    $class = static::MODEL;

    $datas = $this->_model->read($id);
    if(count($datas)>0) {
      $nain = new Nain($datas[0]);
      $villeModel = new VilleModel(DB_ENGINE, DB_HOST, DB_NAME, DB_USER, DB_PWD, DB_CHARSET, DB_COLLATE);
      $villeDatas = $villeModel->read($nain->getVille());
      if(count($villeDatas)>0) {
        $ville = new Ville($villeDatas[0]);
        $nain->setVille($ville);
      }
      if(!is_null($nain->getGroupe())) {
        $groupeModel = new GroupeModel(DB_ENGINE, DB_HOST, DB_NAME, DB_USER, DB_PWD, DB_CHARSET, DB_COLLATE);
        $groupeDatas = $groupeModel->read($nain->getGroupe());
        if(count($groupeDatas)>0) {
          $groupe = new Groupe($groupeDatas[0]);
          $nain->setGroupe($groupe);
        }

        $taverneModel = new TaverneModel(DB_ENGINE, DB_HOST, DB_NAME, DB_USER, DB_PWD, DB_CHARSET, DB_COLLATE);
        $taverneDatas = $taverneModel->read($nain->getGroupe()->getTaverne());
        if(count($taverneDatas)>0) {
          $taverne = new Taverne($taverneDatas[0]);
        }
      }
    }

    include('views/' . $class . '/card.php');
  }
}