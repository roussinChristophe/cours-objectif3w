<?php
class VilleController extends CoreController {
  const MODEL = 'Ville';

  public function list() {
    $class = static::MODEL;

    $datas = $this->_model->read();
    
    foreach($datas as $data) {
      $ville = new $class($data);
      $villes[] = $ville;
    }
    include('views/' . $class . '/index.php');
  }



  public function card(int $id) {
    $class = static::MODEL;

    $datas = $this->_model->read($id);
    if(count($datas)>0) {
      $ville = new $class($datas[0]);
      $nainModel = new NainModel(DB_ENGINE, DB_HOST, DB_NAME, DB_USER, DB_PWD, DB_CHARSET, DB_COLLATE);
      $dataNains = $nainModel->readByVille($ville->getId());
      foreach($dataNains as $data) {
        $nains[] = new Nain($data);
      }
      $taverneModel = new TaverneModel(DB_ENGINE, DB_HOST, DB_NAME, DB_USER, DB_PWD, DB_CHARSET, DB_COLLATE);
      $dataTavernes = $taverneModel->readByVille($ville->getId());
      foreach($dataTavernes as $data) {
        $tavernes[] = new Taverne($data);
      }
      $tunnelModel = new TunnelModel(DB_ENGINE, DB_HOST, DB_NAME, DB_USER, DB_PWD, DB_CHARSET, DB_COLLATE);
      $dataTunnels = $tunnelModel->readByVille($ville->getId());
      foreach($dataTunnels as $data) {
        $tunnel = new Tunnel($data);
        $villeDepart = $this->_model->read($tunnel->getVilledepart());
        if(count($villeDepart)>0) {
          $villeDepart = new Ville($villeDepart[0]);
          $tunnel->setVilledepart($villeDepart);
        }
        $villeArrivee = $this->_model->read($tunnel->getVillearrivee());
        if(count($villeArrivee)>0) {
          $villeArrivee = new Ville($villeArrivee[0]);
          $tunnel->setVillearrivee($villeArrivee);
        }
        
        $tunnels[] = $tunnel;
      }
    }

    include('views/' . $class . '/card.php');
  }
}