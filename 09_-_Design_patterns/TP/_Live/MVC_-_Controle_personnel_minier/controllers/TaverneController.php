<?php
class TaverneController extends CoreController {
  const MODEL = 'Taverne';

  public function list() {
    $class = static::MODEL;

    $datas = $this->_model->read();
    
    foreach($datas as $data) {
      $taverne = new $class($data);
      $this->generateVille($taverne);
      $tavernes[] = $taverne;
    }
    include('views/' . $class . '/index.php');
  }



  public function card(int $id) {
    $class = static::MODEL;

    $datas = $this->_model->read($id);
    if(count($datas)>0) {
      $taverne = new $class($datas[0]);
      $this->generateVille($taverne);
    }

    include('views/' . $class . '/card.php');
  }

  public function generateVille(Taverne &$taverne) {
    $villeModel = new VilleModel(DB_ENGINE, DB_HOST, DB_NAME, DB_USER, DB_PWD, DB_CHARSET, DB_COLLATE);

    $dataVille = $villeModel->read($taverne->getVille());
    if(count($dataVille)>0) {
      $ville = new Ville($dataVille[0]);
      $taverne->setVille($ville);
    }
  }
}