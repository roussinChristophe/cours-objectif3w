<?php
require_once('config/app.php');
require_once('config/db.php');
require_once('lib/functions.php');

require_once('controllers/ConversationController.php');
require_once('controllers/MessageController.php');

try{
  if(!empty($_GET['c'])) {
    $controllerName = ucfirst($_GET['c']) . 'Controller';
    if(class_exists($controllerName)) {
      $controller = new $controllerName;
    }
  } else {
    $controller = new ConversationController;
  }

  if(!empty($_GET['a'])) {
    $methodName = $_GET['a'] . ucfirst($_GET['c']);
    if(method_exists($controller, $methodName)) {
      $controller->$methodName();
    }
  } else {
    $controller->indexConversation();
  }
} catch(Exception $e) {
  header('Location: .?_err=500');
  exit;
}