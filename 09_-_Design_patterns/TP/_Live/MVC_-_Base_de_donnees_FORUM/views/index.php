<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forum</title>

    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <?php if(!empty($conversations)) { ?>
    <table border="1" style="border-collapse: collapse;">
      <thead>
        <tr>
          <th>ID de la conversation</th>
          <th>Date de la conversation</th>
          <th>Heure de la conversation</th>
          <th>Nombre de messages</th>
          <td></td>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach($conversations as $data) {
          $conv = new Conversation($data);
          $conv->showTr();
        }
        ?>
      </tbody>
    </table>
    <?php } else { ?>
    <p>Aucune conversation</p>
    <?php } ?>
  </body>
</html>