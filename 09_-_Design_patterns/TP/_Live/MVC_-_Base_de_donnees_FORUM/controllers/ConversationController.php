<?php
class ConversationController {
  public function indexConversation() {
    try {
      $convModel = new ConversationModel(DB_ENGINE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PWD);
      $conversations = $convModel->readAll();
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), $e->getCode(), $e);
    }
    include('views/index.php');
  }
}