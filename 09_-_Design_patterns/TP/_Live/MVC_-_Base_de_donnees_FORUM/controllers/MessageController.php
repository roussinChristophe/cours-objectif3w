<?php
class MessageController {
  public function indexMessage() {
    if(empty($_GET['conv']) || !ctype_digit($_GET['conv'])) {
      header('Location: .?_err=404');
      exit;
    }


    $currentPage = 1;
    if(!empty($_GET['page']) && ctype_digit($_GET['page'])) {
      $currentPage = $_GET['page'];
    }

    $pagination = PAGINATION;
    if(!empty($_REQUEST['pagination']) && ctype_digit($_REQUEST['pagination'])) {
      $pagination = $_REQUEST['pagination'];
    }

    $orderby = 'date';
    $order = 'DESC';
    if(!empty($_REQUEST['orderby'])) {
      $orderby = $_REQUEST['orderby'];
    }
    if(!empty($_REQUEST['order'])) {
      $order = $_REQUEST['order'];
    }

    try {
      $msgModel = new MessageModel(DB_ENGINE, DB_HOST, DB_NAME, DB_CHARSET, DB_USER, DB_PWD);
      $messages = $msgModel->readAll($_GET['conv'], $pagination, $pagination*($currentPage-1), $orderby, $order);
    } catch(Exception $e) {
      header('Location: .?_err=500');
      exit;
    }

    if(!$messages) {
      header('Location: .?_err=404');
      exit;
    }
    include('views/messages.php');
  }
}