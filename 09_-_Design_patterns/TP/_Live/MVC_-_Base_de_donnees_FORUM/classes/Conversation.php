<?php
class Conversation {
  private $_id;
  private $_date;
  private $_hour;
  private $_status;
  private $_nbMsg;

  public function __construct(array $data) {
    $this->hydrate($data);
  }

  private function setId(int $id) {
    $this->_id = $id;
  }

  private function setDate(string $date) {
    $this->_date = $date;
  }

  private function setHour(string $hour) {
    $this->_hour = $hour;
  }

  private function setStatus(bool $status) {
    $this->_status = $status;
  }

  private function setNbMsg(int $nb) {
    $this->_nbMsg = $nb;
  }

  public function getId(): int {
    return $this->_id;
  }

  public function getDate(): string {
    return $this->_date;
  }

  public function getHour(): string {
    return $this->_hour;
  }

  public function getStatus(): bool {
    return $this->_status;
  }

  public function getNbMsg(): int {
    return $this->_nbMsg;
  }

  private function hydrate(array $data) {
    foreach($data as $prop=>$value) {
      $setter = 'set' . ucfirst($prop);
      if(method_exists($this, $setter)) {
        $this->$setter($value);
      }
    }
  }

  public function showTr() {
    // if(!$this->getStatus()) {
    //   echo '<tr class="opened">';
    // } else {
    //   echo '<tr class="closed">';
    // }
    // echo '
    //       <td>' . $this->getId() . '</td>
    //       <td>' . $this->getDate() . '</td>
    //       <td>' . $this->getHour() . '</td>
    //       <td>' . $this->getNbMsg() . '</td>
    //       <td></td>
    //     </tr>';
    echo '
        <tr class="' . (!$this->getStatus() ? 'opened' : 'closed') . '">
          <td>' . $this->getId() . '</td>
          <td>' . $this->getDate() . '</td>
          <td>' . $this->getHour() . '</td>
          <td>' . $this->getNbMsg() . '</td>
          <td><a href="?c=message&a=index&conv=' . $this->getId() . '" title="">Voir les messages</a></td>
        </tr>';
  }
}