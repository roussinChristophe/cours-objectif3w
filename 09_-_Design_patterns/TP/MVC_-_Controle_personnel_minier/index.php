<?php
require_once( 'ini.php' );

try {
    /**
     * --------------------------------------------------
     * AUTOROUTING
     * --------------------------------------------------
     */
    if( SRequest::getInstance()->get( 'c' )!==null )
        $bundle = ucwords( strtolower( SRequest::getInstance()->get( 'c' ) ) ); // Defines the controller's name depending on passed value
    else
        $bundle = 'Home';//'Default'; // Defines the default controller's name

    $class = $bundle . 'Controller';

    // if( file_exists( ( defined( 'MODULESPATH' ) ? MODULESPATH : '' ) . $bundle . ( defined( 'DS' ) ? DS : DIRECTORY_SEPARATOR ) . $class . '.php' ) )
        if( class_exists( $class ) ) :
            $ctrl = new $class( SRequest::getInstance() ); // Instantiates the controller

            if( SRequest::getInstance()->get( 'a' )!==null )
                $method = SRequest::getInstance()->get( 'a' ) . 'Action'; // Defines the method's name depending on passed value
            else
                $method = 'defaultAction'; // Defines the default method's name

            if( method_exists( $ctrl, $method ) ) :
                SPDO::getInstance()->init( DB_HOST, DB_NAME, DB_LOGIN, DB_PWD );
                $ctrl->$method( SPDO::getInstance()->getPDO() ); // Calls the method
                exit;
            endif;
        endif;

    if( $class=='DefaultController' && $method=='defaultAction' )
        header( 'Location:' . DOMAIN . '500.php');
    else
        header( 'Location:' . DOMAIN . '404.php');
} catch( Exception $e ) {
    if( defined( 'DEBUG' ) && DEBUG )
        if( get_class( $e )=='KernelException' )
            die( $e );
        else
            die( $e->getMessage() );
    else
        header( 'Location:' . DOMAIN . '500.php');
}