<?php
header( $_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found' );
require_once( 'ini.php' );

$sitename = 'Erreur 404';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title><?php echo ( isset( $sitename ) ? $sitename . ( defined( 'TITLE_SEPARATOR' ) ? TITLE_SEPARATOR : '' ) : '' ) . ( defined( 'SITE_TITLE' ) ? SITE_TITLE : '' ); ?></title>

        <style type="text/css">
            <!--
            @import url(<?php echo STYLESHEET_URL; ?>);
            -->
        </style>
    </head>
    <body>
        <header role="banner">
            <hgroup>
                <h1>Erreur 404</h1>
                <hr>
                <h2>Page non trouvée</h2>
            </hgroup>
        </header>

        <main role="main">
            <article role="article">
                <div class="content">
                    <?php /* ?><img alt="" src="<?php echo THEMES_URL; ?>parent/images/errors/404.png" style="float:left;margin:0 25px 25px 0;margin:0 2.5rem 2.5rem 0;max-width:150px;max-width:15rem;"><?php */ ?>
                    <img alt="" src="<?php echo THEMES_URL; ?>CPMEZDCI/images/errors/404.jpg" style="float:left;margin:0 25px 25px 0;margin:0 2.5rem 2.5rem 0;max-width:150px;max-width:15rem;">
                    <h3>Vous êtes perdu !</h3>
                    <p>Elles ne peuvent pas toutes être gagnantes ... Essayez en une autre.</p>
                    <p><small><?php printf( 'L\'URL demandée n\'a pas pu être trouvée sur ce serveur.<br>Si vous avez saisi l\'URL manuellement, vérifiez-la et réessayez.<br>Si vous pensez qu\'il s\'agit d\'une erreur de serveur, veuillez <a href="mailto:%s">contacter le support technique</a>.', SUPPORT_EMAIL ); ?></small></p>
                </div>

                <footer>
                    <a class="back" href="<?php echo DOMAIN; ?>" title="">Retour à la page d'accueil</a></li>
                </footer>
            </article>
        </main>

        <footer role="contentinfo">
            <span>&copy;<?php echo date( 'Y' ); ?> tous droits réservés<?php echo ( defined( 'AUTHOR_NAME' ) ? ' - ' . AUTHOR_NAME : '' ); ?></span>
        </footer>
    </body>
</html>