<?php
header( $_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden' );
require_once( 'ini.php' );

$sitename = 'Erreur 403';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title><?php echo ( isset( $sitename ) ? $sitename . ( defined( 'TITLE_SEPARATOR' ) ? TITLE_SEPARATOR : '' ) : '' ) . ( defined( 'SITE_TITLE' ) ? SITE_TITLE : '' ); ?></title>

        <style type="text/css">
            <!--
            @import url(<?php echo STYLESHEET_URL; ?>);
            -->
        </style>
    </head>
    <body>
        <header role="banner">
            <hgroup>
                <h1>Erreur 403</h1>
                <hr>
                <h2>Accès refusé/interdit</h2>
            </hgroup>
        </header>

        <main role="main">
            <article role="article">
                <div class="content">
                    <img alt="" src="<?php echo THEMES_URL; ?>parent/images/errors/403.png" style="float:left;margin:0 25px 25px 0;margin:0 2.5rem 2.5rem 0;max-width:150px;max-width:15rem;">
                    <h3>Vous ne passerez pas !</h3>
                    <p>Nous sommes désolés, vous n'avez pas accès à la page que vous avez demandée.</p>
                    <p><small>Pour afficher cette page, vous devrez peut-être vous connecter avec un compte différent.</small></p>
                </div>

                <footer>
                    <a class="back" href="<?php echo DOMAIN; ?>" title="">Retour à la page d'accueil</a></li>
                </footer>
            </article>
        </main>

        <footer role="contentinfo">
            <span>&copy;<?php echo date( 'Y' ); ?> tous droits réservés<?php echo ( defined( 'AUTHOR_NAME' ) ? ' - ' . AUTHOR_NAME : '' ); ?></span>
        </footer>
    </body>
</html>