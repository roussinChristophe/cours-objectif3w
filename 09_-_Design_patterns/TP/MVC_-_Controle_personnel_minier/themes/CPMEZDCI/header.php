<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title><?php echo ( isset( $page_title ) ? $page_title . ( defined( 'TITLE_SEPARATOR' ) ? TITLE_SEPARATOR : '' ) : '' ) . ( defined( 'SITE_TITLE' ) ? SITE_TITLE : '' ); ?></title>

        <style type="text/css">
            <!--
            /*
            @import url(<?php echo STYLESHEET_URL; ?>);
            */
            @import url(<?php echo THEMES_URL; ?>CPMEZDCI/style.css);
            @import url(<?php echo THEMES_URL; ?>CPMEZDCI/responsive.css);
            -->
        </style>
    </head>
    <body>
        <div style="height:96vh;">

            <div class="grid-wrapper">
            <div class="grid12 x3 noprint">
            <header role="banner">
                <nav role="navigation">
                    <a href="<?php echo defined( 'DOMAIN' ) ? DOMAIN : dirname( $_SERVER['PHP_SELF'] ); ?>" id="logo" rel="home" title="<?php echo defined( 'SITE_TITLE' ) ? SITE_TITLE : ''; ?>"><span data-role="logo-wrapper"><img alt="<?php echo defined( 'SITE_TITLE' ) ? SITE_TITLE : ''; ?>" data-role="logo" src="<?php echo defined( 'UPLOADS_URL' ) ? UPLOADS_URL : ''; ?>customize/logo_mine.png" /></span><span data-role="baseline"><?php echo defined( 'SITE_TITLE' ) ? SITE_TITLE : ''; ?></span></a>
                    <ul class="menu-principal">
                        <li<?php if( Srequest::getInstance()->get( 'c' )===null ) echo ' class="active"'; ?>><a href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>" title="">Accueil</a></li>
                        <li<?php if( Srequest::getInstance()->get( 'c' )!==null && Srequest::getInstance()->get( 'c' )=='dwarf' ) echo ' class="active"'; ?>><a href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=dwarf" title="">Nains</a></li>
                        <li<?php if( Srequest::getInstance()->get( 'c' )!==null && Srequest::getInstance()->get( 'c' )=='group' ) echo ' class="active"'; ?>><a href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=group" title="">Groupes</a></li>
                        <li<?php if( Srequest::getInstance()->get( 'c' )!==null && Srequest::getInstance()->get( 'c' )=='tavern' ) echo ' class="active"'; ?>><a href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=tavern" title="">Tavernes</a></li>
                        <li<?php if( Srequest::getInstance()->get( 'c' )!==null && Srequest::getInstance()->get( 'c' )=='city' ) echo ' class="active"'; ?>><a href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=city" title="">Villes</a></li>
                    </ul>
                </nav>
            </header>
            </div>

            <div class="grid12 x9">
                <section role="main"<?php echo ( isset( $page_id ) ? ' id="' . $page_id . '"' : '' ); ?>>
                    <article role="article">
                        <header class="header">
                            <h1 class="title-lvl1">Tableau de bord</h1>
                            <?php echo ( isset( $ariane ) ? $ariane : '' ); ?>
                        </header>