<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title><?php echo ( isset( $sitename ) ? $sitename . ( defined( 'TITLE_SEPARATOR' ) ? TITLE_SEPARATOR : '' ) : '' ) . ( defined( 'SITE_TITLE' ) ? SITE_TITLE : '' ); ?></title>

        <style type="text/css">
            <!--
            @import url(<?php echo STYLESHEET_URL; ?>);
            -->
        </style>
    </head>
    <body>
        <header role="banner">
            <nav role="navigation">
                <a href="<?php echo defined( 'DOMAIN' ) ? DOMAIN : dirname( $_SERVER['PHP_SELF'] ); ?>" id="logo" rel="home" title="<?php echo defined( 'SITE_TITLE' ) ? SITE_TITLE : ''; ?>"><span data-role="logo-wrapper"><img alt="<?php echo defined( 'SITE_TITLE' ) ? SITE_TITLE : ''; ?>" data-role="logo" src="<?php echo defined( 'THEMES_URL' ) ? THEMES_URL : ''; ?>parent/images/logo_default.png" /></span><span data-role="baseline"><?php echo defined( 'SITE_TITLE' ) ? SITE_TITLE : ''; ?></span></a>
                <ul class="menu-principal">
                    <li<?php if( !isset( $_GET['c'] ) ) echo ' class="active"'; ?>><a href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>" title="">Accueil</a></li>
                </ul>
            </nav>
        </header>