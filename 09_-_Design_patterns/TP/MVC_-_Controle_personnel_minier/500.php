<?php
header( $_SERVER['SERVER_PROTOCOL'] . ' 500 Internal server error' );
require_once( 'ini.php' );

$sitename = 'Erreur 500';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title><?php echo ( isset( $sitename ) ? $sitename . ( defined( 'TITLE_SEPARATOR' ) ? TITLE_SEPARATOR : '' ) : '' ) . ( defined( 'SITE_TITLE' ) ? SITE_TITLE : '' ); ?></title>

        <style type="text/css">
            <!--
            @import url(<?php echo STYLESHEET_URL; ?>);
            -->
        </style>
    </head>
    <body>
        <header role="banner">
            <hgroup>
                <h1>Erreur 500</h1>
                <hr>
                <h2>Erreur interne du serveur</h2>
            </hgroup>
        </header>

        <main role="main">
            <article role="article">
                <div class="content">
                    <img alt="" src="<?php echo THEMES_URL; ?>parent/images/errors/500.png" style="float:left;margin:0 25px 25px 0;margin:0 2.5rem 2.5rem 0;max-width:150px;max-width:15rem;">
                    <h3>Désolé, ce n'est pas vous. C'est nous !</h3>
                    <p>Quelque chose s'est mal passé</p>
                    <p><small><?php printf( 'Nous rencontrons un problème de serveur.<br>Veuillez réessayer ultérieurement ou <a href="mailto:%s">contacter le support technique</a>.', SUPPORT_EMAIL ); ?></small></p>
                </div>

                <footer>
                    <a class="back" href="<?php echo DOMAIN; ?>" title="">Retour à la page d'accueil</a></li>
                </footer>
            </article>
        </main>

        <footer role="contentinfo">
            <span>&copy;<?php echo date( 'Y' ); ?> tous droits réservés<?php echo ( defined( 'AUTHOR_NAME' ) ? ' - ' . AUTHOR_NAME : '' ); ?></span>
        </footer>
    </body>
</html>