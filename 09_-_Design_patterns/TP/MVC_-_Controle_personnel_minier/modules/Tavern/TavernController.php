<?php
/**
 * This file is part of the framework
 *
 * The TavernController class is used to manage taverns
 *
 * @package CPMEZDCI
 * @copyright ©2018 tous droits réservés
 * @author Damien TIVELET
 */
class TavernController extends KernelController {
    /**
     * CONSTANTS
     */
    const PAGE_ID = 'tavernes';
    const PAGE_TITLE = 'Tavernes';



    /**
     * --------------------------------------------------
     * ACTIONS
     * --------------------------------------------------
     */
    /**
     * defaultAction By default, launches the display of the list
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function defaultAction( PDO $db = null ) {
        $this->init(  __FILE__, __FUNCTION__, $db );
        $this->setLayout( 'CPMEZDCI' );

        $this->setProperty( 'datas', $this->getModel()->selectAll() );
        $this->setProperty( 'ariane', $this->ariane( self::PAGE_TITLE ) );
        $this->render( true );
    }

    /**
     * cardAction Launches the display of a card for a single registration
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function cardAction( PDO $db = null ) {
        if( !( $this->getRequest()->get( 'id' )!==null && ctype_digit( $this->getRequest()->get( 'id' ) ) ) ) :
            header( 'Location:404.php' ); // On redirige vers la page d'erreur 404.
            exit;
        endif;

        $this->init(  __FILE__, __FUNCTION__, $db );
        $this->setLayout( 'CPMEZDCI' );

        $datas = $this->getModel()->selectById( $this->getRequest()->get( 'id' ) );

        if( count( $datas )<1 ) :
            header( 'Location:404.php' );
            exit;
        endif;

        $this->setProperty( 'page_title', ( isset( $datas['Nom'] ) ? $datas['Nom'] : 'Inconnu' ) );
        $this->setProperty( 'ariane', $this->ariane( array( '<a href="' . ( defined( 'DOMAIN' ) ? DOMAIN : '' ) . '?c=' . strtolower( $this->getBundle() ) . '" title="' . self::PAGE_TITLE . '">' . self::PAGE_TITLE . '</a>', ( $this->getProperties( 'page_title' )!==null ? $this->getProperties( 'page_title' ) : 'Inconnu' ) ) ) );
        $this->setProperty( 'datas', $datas );

        $this->render( true );
    }
}