<?php
/**
 * This file is part of the framework
 *
 * The TavernModel class
 *
 * @package {__PACKAGE_NAME__}
 * @copyright {__PACKAGE_LICENSE__}
 * @author {__PACKAGE_AUTHOR__}
 */
class TavernModel extends KernelModel {
    /**
     * --------------------------------------------------
     * METHODS
     * --------------------------------------------------
     */
    /**
     * selectAll
     * @return mixed    The result of the query
     */
    public function selectAll() {
        $query = 'SELECT `taverne`.`t_id` AS "ID", `taverne`.`t_nom` AS "Nom", `ville`.`v_nom` AS "Ville"
            FROM `taverne`
            INNER JOIN `ville` ON `taverne`.`t_ville_fk`=`ville`.`v_id`
            ORDER BY `taverne`.`t_nom` ASC';

        return $this->query( $query );
    }

    /**
     * selectById
     * @param  integer  $id     The id of a record
     * @return mixed            The result of the query
     */
    public function selectById( $id ) {
        $query = 'SELECT `taverne`.`t_id` AS "ID", `taverne`.`t_nom` AS "Nom", `ville`.`v_id` AS "villeID", `ville`.`v_nom` AS "Ville", CONCAT( "Blonde:", `taverne`.`t_blonde`, ";", "Brune:", `taverne`.`t_brune`, ";", "Rousse:", `taverne`.`t_rousse` ) AS "Bières servies", CONCAT( `taverne`.`t_chambres`, " dont ", (`taverne`.`t_chambres` - COUNT( DISTINCT `n_id` ) ), " libres" ) AS "Nombre de chambres"
            FROM `taverne`
            INNER JOIN `ville` ON `taverne`.`t_ville_fk`=`ville`.`v_id`
            LEFT JOIN `groupe` ON `taverne`.`t_id`=`groupe`.`g_taverne_fk`
            LEFT JOIN `nain` ON `groupe`.`g_id`=`nain`.`n_groupe_fk`
            WHERE `taverne`.`t_id`=:tavern
            GROUP BY `taverne`.`t_id`';

        return $this->query(
            $query,
            array(
                'tavern'   => array(
                    'VAL'   => $id,
                    'TYPE'  => PDO::PARAM_INT
                )
            )
        );
    }

    /**
     * selectRecommendedForGroup
     * @param  integer  $id     The id of a group
     * @return mixed            The result of the query
     */
    public function selectRecommendedForGroup( $id ) {
        $query = 'SELECT `taverne`.`t_id` AS "ID", `taverne`.`t_nom` AS "Nom", ( `taverne`.`t_chambres` - COUNT( DISTINCT `n_id` ) ) AS chambresLibres,`taverne`.`t_ville_fk`,`tunnel`.`t_villedepart_fk`
            FROM `taverne`
            INNER JOIN `ville` ON `taverne`.`t_ville_fk`=`ville`.`v_id`
            INNER JOIN `tunnel` ON `ville`.`v_id`=`tunnel`.`t_villedepart_fk`
            INNER JOIN `groupe` ON `tunnel`.`t_id`=`groupe`.`g_tunnel_fk`
            INNER JOIN `nain` ON `groupe`.`g_id`=`nain`.`n_groupe_fk`
            WHERE `groupe`.`g_id`=:group
            GROUP BY `taverne`.`t_id`
            HAVING chambresLibres>(
                SELECT COUNT( DISTINCT `n_id` )
                FROM `nain`
                WHERE `nain`.`n_groupe_fk`=:group
            )';

        return $this->query(
            $query,
            array(
                'group'   => array(
                    'VAL'   => $id,
                    'TYPE'  => PDO::PARAM_INT
                )
            )
        );
    }
}