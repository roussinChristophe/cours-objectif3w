<ul class="grid-wrapper widgets">
    <li class="grid12 x12 item before" data-id="<?php echo ( isset( $page_title ) ? $page_title : '' ); ?>">
        <ul>
            <?php
            if( isset( $datas ) && is_array( $datas ) && count( $datas )>0 ) :
                foreach( $datas as $key=>$item ) :
                    switch( $key ) :
                        case 'Bières servies':
            ?>
            <li class="text-justify"><span class="lbl"><?php echo $key; ?></span> <span class="value">
            <?php
            foreach( explode( ';', $item ) as $beers ) :
                $beer = explode( ':', $beers );
                if( $beer[1] ) $beverage[] = $beer[0];
            endforeach;
            echo implode( ', ', $beverage );
            ?>
            </span></li>
            <?php
                            break;
                        case 'Ville':
            ?>
            <li class="text-justify"><span class="lbl"><?php echo $key; ?></span> <span class="value"><a class="link" href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=city&a=card&id=<?php echo $datas['villeID']; ?>" title=""><?php echo $item; ?></a></span></li>
            <?php
                            break;
                        case 'villeID':
                            break;
                        default:
            ?>
            <li class="text-justify"><span class="lbl"><?php echo $key; ?></span> <span class="value"><?php echo $item; ?></span></li>
            <?php
                    endswitch;
                endforeach;
            endif;
            ?>
        </ul>
    </li>
</ul>