<?php
/**
 * This file is part of the framework
 *
 * The HomeController class is used to manage homepage
 *
 * @package CPMEZDCI
 * @copyright ©2018 tous droits réservés
 * @author Damien TIVELET
 */
class HomeController extends KernelController {
    /**
     * CONSTANTS
     */
    const PAGE_ID = 'home';



    /**
     * --------------------------------------------------
     * ACTIONS
     * --------------------------------------------------
     */
    /**
     * defaultAction
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function defaultAction( PDO $db = null ) {
        $this->init(  __FILE__, __FUNCTION__ );
        $this->setLayout( 'CPMEZDCI' );

        $this->render( true );
    }
}