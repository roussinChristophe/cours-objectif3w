<ul class="grid-wrapper widgets">
    <li class="grid12 x6 item before" data-id="<?php echo ( isset( $page_title ) ? $page_title : '' ); ?>">
        <ul>
            <?php
            if( isset( $datas ) && is_array( $datas ) && count( $datas )>0 ) :
                foreach( $datas as $key=>$item ) :
                    switch( $key ) :
                        case 'Nains originaires de ':
                        case 'Tavernes se trouvant à ':
                        case 'Depuis ':
                        case 'Reliant ':
                            break;
                        default:
            ?>
            <li class="text-justify"><span class="lbl"><?php echo $key; ?></span> <span class="value"><?php echo $item; ?></span></li>
            <?php
                    endswitch;
                endforeach;
            endif;
            ?>
        </ul>
    </li>
    <li class="grid12 x6 item before" data-id="Reliée à">
        <ul>
            <?php
            if( isset( $datas ) && is_array( $datas ) && count( $datas )>0 ) :
                foreach( $datas as $key=>$item ) :
                    switch( $key ) :
                        case 'Depuis ':
                        case 'Reliant ':
                            foreach( explode( ';', $item ) as $tunnel ) : $tunnel = explode( ',', $tunnel );
            ?>
            <li class="text-justify"><span class="lbl"><a class="link" href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=city&a=card&id=<?php echo $tunnel[0]; ?>" title=""><?php echo $tunnel[1]; ?></a></span> <span class="value"><?php echo $tunnel[2]==100 ? 'Ouvert' : 'Creusé à : ' . $tunnel[2] . '%'; ?></span></li>
            <?php
                            endforeach;
                            break;
                    endswitch;
                endforeach;
            endif;
            ?>
        </ul>
    </li>
</ul>
<ul class="grid-wrapper widgets">
    <li class="grid12 x12 item before" data-id="Nains originaires de <?php echo ( isset( $page_title ) ? $page_title : '' ); ?>">
        <ul>
            <?php
            if( isset( $datas ) && is_array( $datas ) && count( $datas )>0 ) :
                foreach( $datas as $key=>$item ) :
                    switch( $key ) :
                        case 'Nains originaires de ':
                            foreach( explode( ';', $item ) as $nain ) : $nain = explode( ',', $nain );
            ?>
            <li style="display:inline-block;vertical-align:top;"><a class="link" href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=dwarf&a=card&id=<?php echo $nain[0]; ?>" title=""><?php echo $nain[1]; ?></a></li>
            <?php
                            endforeach;
                            break;
                    endswitch;
                endforeach;
            endif;
            ?>
        </ul>
    </li>
</ul>
<ul class="grid-wrapper widgets">
    <li class="grid12 x12 item before" data-id="Tavernes se trouvant à <?php echo ( isset( $page_title ) ? $page_title : '' ); ?>">
        <ul>
            <?php
            if( isset( $datas ) && is_array( $datas ) && count( $datas )>0 ) :
                foreach( $datas as $key=>$item ) :
                    switch( $key ) :
                        case 'Tavernes se trouvant à ':
                            foreach( explode( ';', $item ) as $taverne ) : $taverne = explode( ',', $taverne );
            ?>
            <li style="display:inline-block;vertical-align:top;"><a class="link" href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=tavern&a=card&id=<?php echo $taverne[0]; ?>" title=""><?php echo $taverne[1]; ?></a></li>
            <?php
                            endforeach;
                            break;
                    endswitch;
                endforeach;
            endif;
            ?>
        </ul>
    </li>
</ul>