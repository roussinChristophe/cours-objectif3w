<?php
/**
 * This file is part of the framework
 *
 * The CityModel class
 *
 * @package {__PACKAGE_NAME__}
 * @copyright {__PACKAGE_LICENSE__}
 * @author {__PACKAGE_AUTHOR__}
 */
class CityModel extends KernelModel {
    /**
     * --------------------------------------------------
     * METHODS
     * --------------------------------------------------
     */
    /**
     * selectAll
     * @return mixed    The result of the query
     */
    public function selectAll() {
        $query = 'SELECT `ville`.`v_id` AS "ID", `ville`.`v_nom` AS "Nom", `ville`.`v_superficie` AS "Superficie"
            FROM `ville`
            ORDER BY `ville`.`v_nom` ASC';

        return $this->query( $query );
    }

    /**
     * selectById
     * @param  integer  $id     The id of a record
     * @return mixed            The result of the query
     */
    public function selectById( $id ) {
        $query = 'SELECT `ville`.`v_id` AS "ID", `ville`.`v_nom` AS "Nom", `ville`.`v_superficie` AS "Superficie",
            GROUP_CONCAT( DISTINCT CONCAT( `nain`.`n_id`, ",", `nain`.`n_nom` ) ORDER BY `nain`.`n_nom` ASC SEPARATOR ";") AS "Nains originaires de ",
            GROUP_CONCAT( DISTINCT CONCAT( `taverne`.`t_id`, ",", `taverne`.`t_nom` ) ORDER BY `taverne`.`t_nom` ASC SEPARATOR ";") AS "Tavernes se trouvant à ",
            GROUP_CONCAT( DISTINCT CONCAT( start.`v_id`, ",", start.`v_nom`, ",", tunnelStart.`t_progres` ) ORDER BY tunnelStart.`t_progres` DESC SEPARATOR ";") AS "Reliant ",
            GROUP_CONCAT( DISTINCT CONCAT( finish.`v_id`, ",", finish.`v_nom`, ",", tunnelFinish.`t_progres` ) ORDER BY tunnelFinish.`t_progres` DESC SEPARATOR ";") AS "Depuis "
            FROM `ville`
            LEFT JOIN `nain` ON `ville`.`v_id`=`nain`.`n_ville_fk`
            LEFT JOIN `taverne` ON `ville`.`v_id`=`taverne`.`t_ville_fk`
            LEFT JOIN `tunnel` tunnelStart ON `ville`.`v_id`=tunnelStart.`t_villedepart_fk`
            LEFT JOIN `ville` start ON tunnelStart.`t_villearrivee_fk`=start.`v_id`
            LEFT JOIN `tunnel` tunnelFinish ON `ville`.`v_id`=tunnelFinish.`t_villearrivee_fk`
            LEFT JOIN `ville` finish ON tunnelFinish.`t_villedepart_fk`=finish.`v_id`
            WHERE `ville`.`v_id`=:city
            GROUP BY `ville`.`v_id`';

        return $this->query(
            $query,
            array(
                'city'   => array(
                    'VAL'   => $id,
                    'TYPE'  => PDO::PARAM_INT
                )
            )
        );
    }
}