<?php
/**
 * This file is part of the framework
 *
 * The DefaultController class is used to prevent default actions
 *
 * @package {__PACKAGE_NAME__}
 * @copyright {__PACKAGE_LICENSE__}
 * @author {__PACKAGE_AUTHOR__}
 */
class DefaultController extends KernelController {
    /**
     * --------------------------------------------------
     * ACTIONS
     * --------------------------------------------------
     */
    /**
     * defaultAction
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function defaultAction( PDO $db = null ) {
        $this->init(  __FILE__, __FUNCTION__, $db );
        $this->render( THEMEPARENTPATH );
    }
}