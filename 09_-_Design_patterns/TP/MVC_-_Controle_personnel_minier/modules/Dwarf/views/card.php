<form action="?c=dwarf&a=update&id=<?php echo $id; ?>" data-role="formulaire" method="POST">
<ul class="grid-wrapper widgets">
    <li class="grid12 x12 item before" data-id="<?php echo ( isset( $page_title ) ? $page_title : '' ); ?>">
        <ul>
            <?php
            if( isset( $datas ) && is_array( $datas ) && count( $datas )>0 ) :
                foreach( $datas as $key=>$item ) :
                    switch( $key ) :
                        case 'Originaire de':
            ?>
            <li class="text-justify"><span class="lbl"><?php echo $key; ?></span> <span class="value"><a class="link" href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=city&a=card&id=<?php echo $datas['villeNaissanceID']; ?>" title=""><?php echo $item; ?></a></span></li>
            <?php
                            break;
                        case 'Boit dans':
                            if( !( is_null( $item ) || strtoupper( $item )=='NULL' ) ) :
            ?>
            <li class="text-justify"><span class="lbl"><?php echo $key; ?></span> <span class="value"><a class="link" href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=tavern&a=card&id=<?php echo $datas['taverneID']; ?>" title=""><?php echo $item; ?></a></span></li>
            <?php
                            endif;
                            break;
                        case 'Travaille de':
                            if( !( is_null( $item ) || strtoupper( $item )=='NULL' ) ) :
            ?>
            <li class="text-justify"><span class="lbl"><?php echo $key; ?></span> <span class="value"><?php echo $item; ?><?php echo !( is_null( $datas['tunnelStartID'] ) || strtoupper( $datas['tunnelStartID'] )=='NULL' ) && !( is_null( $datas['tunnelFinishID'] ) || strtoupper( $datas['tunnelFinishID'] )=='NULL' ) ? '<br />dans le tunnel reliant <a class="link" href="' . ( defined( 'DOMAIN' ) ? DOMAIN : '' ) . '?c=city&a=card&id=' . $datas['tunnelStartID'] . '" title="">' . $datas['tunnelStart'] . '</a> à <a class="link" href="' . ( defined( 'DOMAIN' ) ? DOMAIN : '' ) . '?c=city&a=card&id=' . $datas['tunnelFinishID'] . '" title="">' . $datas['tunnelFinish'] . '</a>' : ''; ?></span></li>
            <?php
                            endif;
                            break;
                        case 'Membre du groupe':
                            if( isset( $groups ) && is_array( $groups ) && count( $groups )>0 ) :
            ?>
            <li class="text-justify">
                <label class="lbl" for="list-group"><?php echo $key; ?></label>
                <span class="value">
                    <select dir="rtl" id="list-group" name="group">
                        <option<?php echo isset( $datas['Membre du groupe'] ) && ( is_null( $datas['Membre du groupe'] ) || strtoupper( $datas['Membre du groupe'] )=='NULL' ) ? ' selected="selected"' : ''; ?> value="NULL">En vacances</option>
                        <?php foreach( $groups as $item ) : ?>
                        <option<?php echo isset( $datas['Membre du groupe'] ) && $datas['Membre du groupe']==$item['ID'] ? ' selected="selected"' : ''; ?> value="<?php echo $item['ID']; ?>"><?php echo $item['ID']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </span>
            </li>
            <?php
                            endif;
                            break;
                        case 'villeNaissanceID':
                        case 'taverneID':
                        case 'tunnelStartID':
                        case 'tunnelStart':
                        case 'tunnelFinishID':
                        case 'tunnelFinish':
                            break;
                        default:
            ?>
            <li class="text-justify"><span class="lbl"><?php echo $key; ?></span> <span class="value"><?php echo $item; ?></span></li>
            <?php
                    endswitch;
                endforeach;
            endif;
            ?>
            <li class="text-justify">
                <?php
                if( isset( $message ) )
                    if( $message===true )
                        echo '<span class="lbl alert-block ok">La modification s\'est bien effectuée !</span>';
                    elseif( $message===false )
                        echo '<span class="lbl alert-block warning">Aucune modification effectuée !</span>';
                    else
                        echo '<span class="lbl alert-block error">Une erreur est survenue !</span>';
                else
                    echo '<span class="lbl"></span>';
                ?>
                <span class="value"><input data-role="submit" type="submit" value="Changer de groupe" /></span>
            </li>
        </ul>
    </li>
</ul>
</form>