<?php
/**
 * This file is part of the framework
 *
 * The DwarfController class is used to manage dwarfs
 *
 * @package CPMEZDCI
 * @copyright ©2018 tous droits réservés
 * @author Damien TIVELET
 */
class DwarfController extends KernelController {
    /**
     * CONSTANTS
     */
    const PAGE_ID = 'dwarf';
    const PAGE_TITLE = 'Nains';



    /**
     * --------------------------------------------------
     * ACTIONS
     * --------------------------------------------------
     */
    /**
     * defaultAction By default, launches the display of the list
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function defaultAction( PDO $db = null ) {
        $this->init(  __FILE__, __FUNCTION__, $db );
        $this->setLayout( 'CPMEZDCI' );

        $this->setProperty( 'datas', $this->getModel()->selectAll() );
        $this->setProperty( 'ariane', $this->ariane( self::PAGE_TITLE ) );
        $this->render( true );
    }

    /**
     * cardAction Launches the display of a card for a single registration
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function cardAction( PDO $db = null, $message = null ) {
        if( !( $this->getRequest()->get( 'id' )!==null && ctype_digit( $this->getRequest()->get( 'id' ) ) ) ) :
            header( 'Location:404.php' ); // On redirige vers la page d'erreur 404.
            exit;
        endif;

        $this->init(  __FILE__, __FUNCTION__, $db );
        $this->setLayout( 'CPMEZDCI' );

        $datas = $this->getModel()->selectById( $this->getRequest()->get( 'id' ) );
        if( count( $datas )<1 ) :
            header( 'Location:404.php' );
            exit;
        endif;

        $groupModel = new GroupModel( $db );
        $groups = $groupModel->selectAll();

        $this->setProperty( 'page_title', ( isset( $datas['Nom'] ) ? $datas['Nom'] : 'Inconnu' ) );
        $this->setProperty( 'id', $this->getRequest()->get( 'id' ) );
        $this->setProperty( 'ariane', $this->ariane( array( '<a href="' . ( defined( 'DOMAIN' ) ? DOMAIN : '' ) . '?c=' . strtolower( $this->getBundle() ) . '" title="' . self::PAGE_TITLE . '">' . self::PAGE_TITLE . '</a>', ( $this->getProperties( 'page_title' )!==null ? $this->getProperties( 'page_title' ) : 'Inconnu' ) ) ) );
        $this->setProperty( 'datas', $datas );
        $this->setProperty( 'groups', $groups );
        $this->setProperty( 'message', $message );

        $this->render( true );
    }

    /**
     * updateAction Updates a record
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function updateAction( PDO $db = null ) {
        if( !( $this->getRequest()->get( 'id' )!==null && ctype_digit( $this->getRequest()->get( 'id' ) ) ) ) :
            header( 'Location:404.php' ); // On redirige vers la page d'erreur 404.
            exit;
        endif;

        $this->init(  __FILE__, __FUNCTION__, $db );

        if( ( $this->getRequest()->post( 'group' )!==null && ( ctype_digit( $this->getRequest()->post( 'group' ) ) || ( is_null( $this->getRequest()->post( 'group' ) ) || strtoupper( $this->getRequest()->post( 'group' ) )=='NULL' ) ) ) )
            $update = $this->getModel()->update( $this->getRequest()->get( 'id' ), $this->getRequest()->post() );

        $this->cardAction( $db, $update );
    }
}