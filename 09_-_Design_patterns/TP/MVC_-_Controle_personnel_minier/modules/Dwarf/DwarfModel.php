<?php
/**
 * This file is part of the framework
 *
 * The DwarfModel class
 *
 * @package {__PACKAGE_NAME__}
 * @copyright {__PACKAGE_LICENSE__}
 * @author {__PACKAGE_AUTHOR__}
 */
class DwarfModel extends KernelModel {
    /**
     * --------------------------------------------------
     * METHODS
     * --------------------------------------------------
     */
    /**
     * selectAll
     * @return mixed    The result of the query
     */
    public function selectAll() {
        $query = 'SELECT `nain`.`n_id` AS "ID", `nain`.`n_nom` AS "Nom", `nain`.`n_barbe` AS "Longueur de barbe", `ville`.`v_nom` AS "Ville de naissance", `nain`.`n_groupe_fk` AS "Groupe"
            FROM `nain`
            INNER JOIN `ville` ON `nain`.`n_ville_fk`=`ville`.`v_id`
            ORDER BY `nain`.`n_nom` ASC';

        return $this->query( $query );
    }

    /**
     * selectById
     * @param  integer  $id     The id of a record
     * @return mixed            The result of the query
     */
    public function selectById( $id ) {
        $query = 'SELECT `nain`.`n_id` AS "ID", `nain`.`n_nom` AS "Nom", `nain`.`n_barbe` AS "Longueur de barbe", `ville`.`v_nom` AS "Originaire de", `ville`.`v_id` AS villeNaissanceID, `taverne`.`t_nom` AS "Boit dans", `taverne`.`t_id` AS taverneID, CONCAT(`groupe`.`g_debuttravail`, " à ", `groupe`.`g_fintravail`) AS "Travaille de", start.`v_nom` AS tunnelStart, start.`v_id` AS tunnelStartID, finish.`v_nom` AS tunnelFinish, finish.`v_id` AS tunnelFinishID, `nain`.`n_groupe_fk` AS "Membre du groupe"
            FROM `nain`
            JOIN `ville` ON `nain`.`n_ville_fk`=`ville`.`v_id`
            LEFT JOIN `groupe` ON `nain`.`n_groupe_fk`=`groupe`.`g_id`
            LEFT JOIN `taverne` ON `groupe`.`g_taverne_fk`=`taverne`.`t_id`
            LEFT JOIN `tunnel` ON `groupe`.`g_tunnel_fk`=`tunnel`.`t_id`
            LEFT JOIN `ville` start ON `tunnel`.`t_villedepart_fk`=start.`v_id`
            LEFT JOIN `ville` finish ON `tunnel`.`t_villearrivee_fk`=finish.`v_id`
            WHERE `nain`.`n_id`=:dwarf';

        return $this->query(
            $query,
            array(
                'dwarf'   => array(
                    'VAL'   => $id,
                    'TYPE'  => PDO::PARAM_INT
                )
            )
        );
    }

    /**
     * update
     * @param  integer  $id     The id of a record
     * @param  array    $datas  The posted datas to update
     * @return mixed            The result of the query
     */
    public function update( $id, $datas ) {
        $query = 'UPDATE `nain` SET
            `nain`.`n_groupe_fk`=' . ( is_null( $datas['group'] ) || strtoupper( $datas['group'] )=='NULL' ? 'NULL' : ':group' ) . '
            WHERE `nain`.`n_id`=:dwarf';

        $values = array(
            'dwarf'   => array(
                'VAL'   => $id,
                'TYPE'  => PDO::PARAM_INT
            )
        );
        if( !( is_null( $datas['group'] ) || strtoupper( $datas['group'] )=='NULL' ) )
            $values['group'] = array(
                'VAL'   => $datas['group'],
                'TYPE'  => PDO::PARAM_INT
            );

        return $this->query(
            $query,
            $values
        );
    }
}