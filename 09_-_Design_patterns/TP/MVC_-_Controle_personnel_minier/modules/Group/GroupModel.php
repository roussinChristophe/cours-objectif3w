<?php
/**
 * This file is part of the framework
 *
 * The GroupModel class
 *
 * @package {__PACKAGE_NAME__}
 * @copyright {__PACKAGE_LICENSE__}
 * @author {__PACKAGE_AUTHOR__}
 */
class GroupModel extends KernelModel {
    /**
     * --------------------------------------------------
     * METHODS
     * --------------------------------------------------
     */
    /**
     * selectAll
     * @return mixed    The result of the query
     */
    public function selectAll() {
        $query = 'SELECT `groupe`.`g_id` AS "ID", CONCAT( `groupe`.`g_debuttravail`, " à ", `groupe`.`g_fintravail` ) AS "Horaires", CONCAT( start.`v_nom`, " à ", finish.`v_nom` ) AS "Affecté au tunnel reliant"
            FROM `groupe`
            LEFT JOIN `tunnel` ON `groupe`.`g_tunnel_fk`=`tunnel`.`t_id`
            LEFT JOIN `ville` start ON `tunnel`.`t_villedepart_fk`=start.`v_id`
            LEFT JOIN `ville` finish ON `tunnel`.`t_villearrivee_fk`=finish.`v_id`
            ORDER BY `groupe`.`g_id` ASC';

        return $this->query( $query );
    }

    /**
     * selectById
     * @param  integer  $id     The id of a record
     * @return mixed            The result of the query
     */
    public function selectById( $id ) {
        $query = 'SELECT `groupe`.`g_id` AS "ID", CONCAT( `groupe`.`g_debuttravail`, "-", `groupe`.`g_fintravail` ) AS "Horaires de travail", `taverne`.`t_id` AS "taverneID", `tunnel`.`t_id` AS "tunnelID", `tunnel`.`t_progres` AS "tunnel%",
            GROUP_CONCAT( DISTINCT CONCAT( `nain`.`n_id`, ",", `nain`.`n_nom` ) ORDER BY `nain`.`n_nom` ASC SEPARATOR ";") AS "Nains du groupe"
            FROM `groupe`
            LEFT JOIN `nain` ON `groupe`.`g_id`=`nain`.`n_groupe_fk`
            LEFT JOIN `taverne` ON `groupe`.`g_taverne_fk`=`taverne`.`t_id`
            LEFT JOIN `tunnel` ON `groupe`.`g_tunnel_fk`=`tunnel`.`t_id`
            LEFT JOIN `ville` startCity ON `tunnel`.`t_villearrivee_fk`=startCity.`v_id`
            LEFT JOIN `ville` finishCity ON `tunnel`.`t_villedepart_fk`=finishCity.`v_id`
            WHERE `groupe`.`g_id`=:group
            GROUP BY `groupe`.`g_id`';

        return $this->query(
            $query,
            array(
                'group'   => array(
                    'VAL'   => $id,
                    'TYPE'  => PDO::PARAM_INT
                )
            )
        );
    }

    /**
     * update
     * @param  integer  $id     The id of a record
     * @param  array    $datas  The posted datas to update
     * @return mixed            The result of the query
     */
    public function update( $id, $datas ) {
        $query = 'UPDATE `groupe` SET
            `groupe`.`g_debuttravail`=:hStart,
            `groupe`.`g_fintravail`=:hFinish,
            `groupe`.`g_taverne_fk`=' . ( is_null( $datas['tavern'] ) || strtoupper( $datas['tavern'] )=='NULL' ? 'NULL' : ':tavern' ) . ',
            `groupe`.`g_tunnel_fk`=' . ( is_null( $datas['tunnel'] ) || strtoupper( $datas['tunnel'] )=='NULL' ? 'NULL' : ':tunnel' ) . '
            WHERE `groupe`.`g_id`=:group';

        $values = array(
            'group'   => array(
                'VAL'   => $id,
                'TYPE'  => PDO::PARAM_INT
            ),
            'hStart'    => array(
                'VAL'   => $datas['hStart'],
                'TYPE'  => PDO::PARAM_STR
            ),
            'hFinish'    => array(
                'VAL'   => $datas['hFinish'],
                'TYPE'  => PDO::PARAM_STR
            )
        );
        if( !( is_null( $datas['tunnel'] ) || strtoupper( $datas['tunnel'] )=='NULL' ) )
            $values['tunnel'] = array(
                'VAL'   => $datas['tunnel'],
                'TYPE'  => PDO::PARAM_INT
            );

        if( !( is_null( $datas['tavern'] ) || strtoupper( $datas['tavern'] )=='NULL' ) )
            $values['tavern'] = array(
                'VAL'   => $datas['tavern'],
                'TYPE'  => PDO::PARAM_INT
            );

        return $this->query(
            $query,
            $values
        );
    }
}