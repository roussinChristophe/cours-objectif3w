<ul class="grid-wrapper widgets">
    <li class="grid12 x12 item before" data-id="<?php echo ( isset( $page_title ) ? $page_title : '' ); ?>">
        <?php if( isset( $datas ) && is_array( $datas ) && count( $datas )>0 ) : ?>
        <div class="table">
            <div class="thead">
                <div class="tr">
                    <?php foreach( $datas[0] as $key=>$item ) : ?>
                    <div class="th"><?php echo $key; ?></div>
                    <?php endforeach; ?>
                    <div class="td"></div>
                </div>
            </div>
            <div class="tbody">
                <?php foreach( $datas as $item ) : ?>
                <div class="tr">
                    <?php foreach( $item as $data ) : ?>
                    <div class="td"><?php echo $data; ?></div>
                    <?php endforeach; ?>
                    <div class="td"><a class="link" href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=group&a=card&id=<?php echo $item['ID']; ?>" title="Voir la fiche">Voir la fiche</a></div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endif; ?>
    </li>
</ul>