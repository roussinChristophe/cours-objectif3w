<form action="?c=group&a=update&id=<?php echo $id; ?>" data-role="formulaire" method="POST">
<ul class="grid-wrapper widgets">
    <li class="grid12 x6 item before" data-id="<?php echo ( isset( $page_title ) ? $page_title : '' ); ?>">
        <ul>
            <?php
            if( isset( $datas ) && is_array( $datas ) && count( $datas )>0 ) :
                foreach( $datas as $key=>$item ) :
                    switch( $key ) :
                        case 'Nains du groupe':
                        case 'taverneID':
                        case 'tunnelID':
                        case 'tunnel%':
                        case 'Reliant':
                            break;
                        case 'Horaires de travail':
                            $horaires = explode( '-', $item );
            ?>
            <li class="text-justify"><span class="lbl"><?php echo $key; ?></span> <span class="value">de <input name="hStart" step="1" type="time" value="<?php echo $horaires[0]; ?>" /> à <input name="hFinish" step="1" type="time" value="<?php echo $horaires[1]; ?>" /></span></li>
            <?php
                            break;
                        default:
            ?>
            <li class="text-justify"><span class="lbl"><?php echo $key; ?></span> <span class="value"><?php echo $item; ?></span></li>
            <?php
                    endswitch;
                endforeach;
            endif;
            ?>
        </ul>
    </li>
    <li class="grid12 x6 item before" data-id="Affectations">
        <ul>
            <?php
            if( isset( $datas ) && is_array( $datas ) && count( $datas )>0 ) :
                foreach( $datas as $key=>$item ) :
                    switch( $key ) :
                        case 'taverneID':
                            if( isset( $taverns ) && is_array( $taverns ) && count( $taverns )>0 ) :
            ?>
            <li class="text-justify">
                <label class="lbl" for="list-tavern">QG à la taverne</label>
                <span class="value">
                    <select dir="rtl" id="list-tavern" name="tavern">
                        <option<?php echo isset( $datas['taverneID'] ) && ( is_null( $datas['taverneID'] ) || strtoupper( $datas['taverneID'] )=='NULL' ) ? ' selected="selected"' : ''; ?> value="NULL">Aucune</option>
                        <?php foreach( $taverns as $item ) : ?>
                        <option<?php echo isset( $datas['taverneID'] ) && $datas['taverneID']==$item['ID'] ? ' selected="selected"' : ''; ?> value="<?php echo $item['ID']; ?>"><?php echo $item['Nom']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </span>
            </li>
            <?php
            if( is_null( $datas['taverneID'] ) || strtoupper( $datas['taverneID'] )=='NULL' ) :
                if( isset( $recommendedTaverns ) && is_array( $recommendedTaverns ) && count( $recommendedTaverns )>0 ) :
            ?>
            <li class="text-justify">
                <label class="lbl" for="list-tavern">Taverne(s) recommandée(s)</label>
                <span class="value"></span>
            </li>
            <?php foreach( $recommendedTaverns as $item ) : ?>
            <li style="display:inline-block;vertical-align:top;"><a class="link" href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=tavern&a=card&id=<?php echo $item['ID']; ?>" title=""><?php echo $item['Nom']; ?></a></li>
            <?php endforeach; ?>
            <?php
                endif;
            endif;
            ?>
            <?php
                            endif;
                            break;
                        case 'tunnelID':
                            if( isset( $tunnels ) && is_array( $tunnels ) && count( $tunnels )>0 ) :
            ?>
            <li class="text-justify">
                <label class="lbl" for="list-tunnel"><?php echo $datas['tunnel%']==100 ? 'Entretient' : 'Creuse'; ?> le tunnel reliant</label>
                <span class="value">
                    <select dir="rtl" id="list-tunnel" name="tunnel">
                        <option<?php echo isset( $datas['tunnelID'] ) && ( is_null( $datas['tunnelID'] ) || strtoupper( $datas['tunnelID'] )=='NULL' ) ? ' selected="selected"' : ''; ?> value="NULL">Aucun</option>
                        <?php foreach( $tunnels as $item ) : ?>
                        <option<?php echo isset( $datas['tunnelID'] ) && $datas['tunnelID']==$item['t_id'] ? ' selected="selected"' : ''; ?> value="<?php echo $item['t_id']; ?>"><?php echo $item['Reliant']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </span>
            </li>
            <?php
                            endif;
                            break;
                    endswitch;
                endforeach;
            endif;
            ?>
        </ul>
    </li>
</ul>
<ul class="grid-wrapper widgets">
    <li class="grid12 x12 item">
        <ul>
            <li class="text-justify">
                <?php
                if( isset( $message ) )
                    if( $message===true )
                        echo '<span class="lbl alert-block ok">La modification s\'est bien effectuée !</span>';
                    elseif( $message===false )
                        echo '<span class="lbl alert-block warning">Aucune modification effectuée !</span>';
                    elseif( $message==='taverne' )
                        echo '<span class="lbl alert-block warning">La taverne choisie ne possède pas assez de chambres libres !<br />Aucune modification effectuée !</span>';
                    else
                        echo '<span class="lbl alert-block error">Une erreur est survenue !</span>';
                else
                    echo '<span class="lbl"></span>';
                ?>
                <span class="value"><input data-role="submit" type="submit" value="Modifier" /></span>
            </li>
        </ul>
    </li>
</ul>
</form>
<ul class="grid-wrapper widgets">
    <li class="grid12 x12 item before" data-id="Nains du <?php echo ( isset( $page_title ) ? $page_title : '' ); ?>">
        <ul>
            <?php
            if( isset( $datas ) && is_array( $datas ) && count( $datas )>0 ) :
                foreach( $datas as $key=>$item ) :
                    switch( $key ) :
                        case 'Nains du groupe':
                            foreach( explode( ';', $item ) as $nain ) : $nain = explode( ',', $nain );
            ?>
            <li style="display:inline-block;vertical-align:top;"><a class="link" href="<?php echo ( defined( 'DOMAIN' ) ? DOMAIN : '' ); ?>?c=dwarf&a=card&id=<?php echo $nain[0]; ?>" title=""><?php echo $nain[1]; ?></a></li>
            <?php
                            endforeach;
                            break;
                    endswitch;
                endforeach;
            endif;
            ?>
        </ul>
    </li>
</ul>