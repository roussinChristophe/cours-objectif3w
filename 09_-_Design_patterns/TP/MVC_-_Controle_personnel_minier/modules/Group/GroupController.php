<?php
/**
 * This file is part of the framework
 *
 * The GroupController class is used to manage groups
 *
 * @package CPMEZDCI
 * @copyright ©2018 tous droits réservés
 * @author Damien TIVELET
 */
class GroupController extends KernelController {
    /**
     * CONSTANTS
     */
    const PAGE_ID = 'group';
    const PAGE_TITLE = 'Groupes';



    /**
     * --------------------------------------------------
     * ACTIONS
     * --------------------------------------------------
     */
    /**
     * defaultAction By default, launches the display of the list
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function defaultAction( PDO $db = null ) {
        $this->init(  __FILE__, __FUNCTION__, $db );
        $this->setLayout( 'CPMEZDCI' );

        $this->setProperty( 'datas', $this->getModel()->selectAll() );
        $this->setProperty( 'ariane', $this->ariane( self::PAGE_TITLE ) );
        $this->render( true );
    }

    /**
     * cardAction Launches the display of a card for a single registration
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function cardAction( PDO $db = null, $message = null ) {
        if( !( $this->getRequest()->get( 'id' )!==null && ctype_digit( $this->getRequest()->get( 'id' ) ) ) ) :
            header( 'Location:404.php' ); // On redirige vers la page d'erreur 404.
            exit;
        endif;

        $this->init(  __FILE__, __FUNCTION__, $db );
        $this->setLayout( 'CPMEZDCI' );

        $datas = $this->getModel()->selectById( $this->getRequest()->get( 'id' ) );
        if( count( $datas )<1 ) :
            header( 'Location:404.php' );
            exit;
        endif;

        $tavernModel = new TavernModel( $db );
        $taverns = $tavernModel->selectAll();
        $recommendedTaverns = $tavernModel->selectRecommendedForGroup( $this->getRequest()->get( 'id' ) );
        $tunnelModel = new TunnelModel( $db );
        $tunnels = $tunnelModel->selectAll();

        $this->setProperty( 'page_title', ( isset( $datas['ID'] ) ? 'Groupe ' . $datas['ID'] : 'Inconnu' ) );
        $this->setProperty( 'id', $this->getRequest()->get( 'id' ) );
        $this->setProperty( 'ariane', $this->ariane( array( '<a href="' . ( defined( 'DOMAIN' ) ? DOMAIN : '' ) . '?c=' . strtolower( $this->getBundle() ) . '" title="' . self::PAGE_TITLE . '">' . self::PAGE_TITLE . '</a>', ( $this->getProperties( 'page_title' )!==null ? $this->getProperties( 'page_title' ) : 'Inconnu' ) ) ) );
        $this->setProperty( 'datas', $datas );
        $this->setProperty( 'taverns', $taverns );
        $this->setProperty( 'recommendedTaverns', $recommendedTaverns );
        $this->setProperty( 'tunnels', $tunnels );
        $this->setProperty( 'message', $message );

        $this->render( true );
    }

    /**
     * updateAction Updates a record
     * @param  PDO|null     $db     The database object
     * @return void
     */
    public function updateAction( PDO $db = null ) {
        if( !( $this->getRequest()->get( 'id' )!==null && ctype_digit( $this->getRequest()->get( 'id' ) ) ) ) :
            header( 'Location:404.php' ); // On redirige vers la page d'erreur 404.
            exit;
        endif;

        $this->init(  __FILE__, __FUNCTION__, $db );

        if( ( $this->getRequest()->post( 'tavern' )!==null && ( ctype_digit( $this->getRequest()->post( 'tavern' ) ) || ( is_null( $this->getRequest()->post( 'tavern' ) ) || strtoupper( $this->getRequest()->post( 'tavern' ) )=='NULL' ) ) )
            && ( $this->getRequest()->post( 'tunnel' )!==null && ( ctype_digit( $this->getRequest()->post( 'tunnel' ) ) || ( is_null( $this->getRequest()->post( 'tunnel' ) ) || strtoupper( $this->getRequest()->post( 'tunnel' ) )=='NULL' ) ) )
            && $this->getRequest()->post( 'hStart' )!==null && ( TypeTest::is_valid_date( $this->getRequest()->post( 'hStart' ), 'H:i:s' ) || TypeTest::is_valid_date( $this->getRequest()->post( 'hStart' ), 'H:i' ) )
            && $this->getRequest()->post( 'hFinish' )!==null && ( TypeTest::is_valid_date( $this->getRequest()->post( 'hFinish' ), 'H:i:s' ) || TypeTest::is_valid_date( $this->getRequest()->post( 'hFinish' ), 'H:i' ) ) ) :

            if( is_null( $this->getRequest()->post( 'tavern' ) ) || strtoupper( $this->getRequest()->post( 'tavern' ) )=='NULL' ) :
                $isAGoodTavern = true;
            elseif( ctype_digit( $this->getRequest()->post( 'tavern' ) ) ) :
                $tavernModel = new TavernModel( $db );
                $recommendedTaverns = $tavernModel->selectRecommendedForGroup( $this->getRequest()->get( 'id' ) );
                $isAGoodTavern = false;
                foreach( $recommendedTaverns as $item ) :
                    if( $item['ID']==$this->getRequest()->post( 'tavern' ) )
                        $isAGoodTavern = true;
                endforeach;
            endif;


            if( $isAGoodTavern ) :
                $update = $this->getModel()->update( $this->getRequest()->get( 'id' ), $this->getRequest()->post() );
                if( $update===false ) :
                    $update = 'error';
                endif;
            else :
                $update = 'taverne';
            endif;
        endif;

        $this->cardAction( $db, $update );
    }
}