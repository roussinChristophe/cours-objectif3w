<?php
/**
 * This file is part of the framework
 *
 * The TunnelModel class
 *
 * @package {__PACKAGE_NAME__}
 * @copyright {__PACKAGE_LICENSE__}
 * @author {__PACKAGE_AUTHOR__}
 */
class TunnelModel extends KernelModel {
    /**
     * --------------------------------------------------
     * METHODS
     * --------------------------------------------------
     */
    /**
     * selectAll
     * @return mixed    The result of the query
     */
    public function selectAll() {
        $query = 'SELECT `t_id`, CONCAT( startCity.`v_nom`, " à ", finishCity.`v_nom` ) AS "Reliant"
            FROM `tunnel`
            LEFT JOIN `ville` startCity ON `tunnel`.`t_villedepart_fk`=startCity.`v_id`
            LEFT JOIN `ville` finishCity ON `tunnel`.`t_villearrivee_fk`=finishCity.`v_id`
            ORDER BY `t_id` ASC';

        return $this->query( $query );
    }
}