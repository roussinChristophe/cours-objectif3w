<?php
require_once('Classes/Player.php');
require_once('Classes/Character.php');
require_once('Models/Model.php');
require_once('Models/PlayerModel.php');
require_once('Models/CharacterModel.php');
require_once('Controllers/PlayerController.php');
require_once('Controllers/CharacterController.php');


$ctrl = 'PlayerController';
if(isset($_GET['ctrl'])) {
  $ctrl = ucfirst(strtolower($_GET['ctrl'])) . 'Controller';
}
$method = 'list';
if(isset($_GET['action'])) {
  $method = $_GET['action'];
}

try {
  if(class_exists($ctrl)) {
    $controller = new $ctrl;

    if(!empty($_POST)) {
      if(method_exists($controller, $method)) {
        if(!empty($_GET['id'])) {
          $controller->$method($_GET['id'], $_POST);
        } else {
          $controller->$method($_POST);
        }
      } else {
        header('Location:404');
        exit;
      }
    } else {
      if(method_exists($controller, $method)) {
        if(!empty($_GET['id'])) {
          $controller->$method($_GET['id']);
        } else {
          $controller->$method();
        }
      } else {
        header('Location:404');
        exit;
      }
    }
  } else {
    header('Location:404');
    exit;
  }
} catch(Exception $e) {
  header('Location:500');
  exit;
}