<?php
class PlayerController {
  private $_model;

  public function __construct() {
    try {
      $this->_model = new PlayerModel('mysql', 'localhost', 'streetfighter', 'root', '');
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), 0, $e);
    }
  }

  public function create() {
    include('Views/create.php');
  }

  public function send(array $data) {
    try {
      if(($id = $this->_model->create($data['email'], $data['firstname'], $data['lastname'], $data['alias']))!==false) {
        header('Location:?action=card&id=' . $id);
        exit;
      }
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), 0, $e);
    }
  }

  public function list() {
    try {
      $users = $this->_model->read();
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), 0, $e);
    }

    include('Views/players.php');
  }

  public function card(int $id) {
    try {
      $user = $this->_model->read($id);
      if(count($user)>0) {
        $player = new Player($user[0]);
      }
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), 0, $e);
    }

    include('Views/player.php');
  }

  public function update(int $id, array $data) {
    try {
      if($this->_model->update($id, $_POST['email'], $_POST['firstname'], $_POST['lastname'], $_POST['alias'])) {
        $msg = 'Mise à jour effectuée';
      }
      $user = $this->_model->read($id);
      if(count($user)>0) {
        $player = new Player($user[0]);
      }
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), 0, $e);
    }

    include('Views/player.php');
  }

  public function delete(int $id) {
    try {
      if($this->_model->delete($id)) {
        $msg = 'Suppression effectuée';
      }
      $users = $this->_model->read();
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), 0, $e);
    }

    include('Views/players.php');
  }
}