<?php
class CharacterController {
  private $_model;

  public function __construct() {
    try {
      $this->_model = new CharacterModel('mysql', 'localhost', 'streetfighter', 'root', '');
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), 0, $e);
    }
  }
  
  public function list() {
    try {
      $users = $this->_model->read();
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), 0, $e);
    }

    include('Views/characters.php');
  }

  public function card(string $id) {
    try {
      $character = $this->_model->read($id);
      if(count($character)>0) {
        $character = new Character($character[0]);
      }
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), 0, $e);
    }

    include('Views/character.php');
  }
}