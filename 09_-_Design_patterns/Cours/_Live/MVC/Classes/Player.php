<?php
class Player {
  private $_id;
  private $_email;
  private $_firstname;
  private $_lastname;
  private $_alias;

  public function setId(int $id): self {
    $this->_id = $id;
    return $this;
  }
  
  public function setEmail(string $email): self {
    $this->_email = $email;
    return $this;
  }
  
  public function setFirstname(string $firstname): self {
    $this->_firstname = $firstname;
    return $this;
  }
  
  public function setLastname(string $lastname): self {
    $this->_lastname = $lastname;
    return $this;
  }
  
  public function setAlias(string $alias): self {
    $this->_alias = $alias;
    return $this;
  }

  public function getId(): int {
    return $this->_id;
  }

  public function getEmail(): string {
    return $this->_email;
  }

  public function getFirstname(): string {
    return $this->_firstname;
  }

  public function getLastname(): string {
    return $this->_lastname;
  }

  public function getAlias(): string {
    return $this->_alias;
  }

  public function __construct(array $data) {
    $this->hydrate($data);
  }

  private function hydrate(array $data): void {
    foreach($data as $key=>$value) {
      $method = 'set' . ucfirst(strtolower($key));
      if(method_exists($this, $method)) {
        $this->$method($value);
      }
    }
  }
}