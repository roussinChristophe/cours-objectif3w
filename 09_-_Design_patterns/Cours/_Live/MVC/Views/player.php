<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>
  <body>
    <nav>
      <ul>
        <li><a href=".">Joueurs</a></li>
        <li><a href=".?ctrl=character">Personnages</a></li>
      </ul>
    </nav>
    <h1>Fiche de <?php echo $player->getAlias(); ?></h1>
    <ul>
      <li>Prénom : <?php echo $player->getFirstname(); ?></li>
      <li>Nom : <?php echo $player->getLastname(); ?></li>
    </ul>
    <a href="." title="Liste des joueurs">Retour à la liste des joueurs</a>

    <?php if(isset($msg)) {
      echo '<p>' . $msg . '</p>';
    }
    ?>
    <form action="?action=update&id=<?php echo $_GET['id']; ?>" method="post">
      <input type="email" name="email" placeholder="E-mail" value="<?php echo $player->getEmail(); ?>">
      <input type="text" name="firstname" placeholder="Prénom" value="<?php echo $player->getFirstname(); ?>">
      <input type="text" name="lastname" placeholder="Nom" value="<?php echo $player->getLastname(); ?>">
      <input type="text" name="alias" placeholder="Pseudo" value="<?php echo $player->getAlias(); ?>">
      <button name="update" type="submit">Mettre à jour</button>
    </form>
    <form action="?action=delete&id=<?php echo $_GET['id']; ?>" method="post">
      <button name="delete" type="submit">Supprimer</button>
    </form>
  </body>
</html>