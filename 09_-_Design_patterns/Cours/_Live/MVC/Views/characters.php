<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>
  <body>
    <nav>
      <ul>
        <li><a href=".">Joueurs</a></li>
        <li><a href=".?ctrl=character">Personnages</a></li>
      </ul>
    </nav>
    <?php if(isset($msg)) {
      echo '<p>' . $msg . '</p>';
    }
    ?>
    <ul>
      <?php
      foreach($users as $user) {
        $character = new Character($user);
      ?>
      <li><a href="?ctrl=character&action=card&id=<?php echo $character->getName(); ?>" title="Afficher la fiche de <?php echo $character->getName(); ?>"><?php echo $character->getName(); ?></a></li>
      <?php
      } 
      ?>
    </ul>
    <a href="?ctrl=character&action=create" title="Ajouter un personnage">Ajouter</a>
  </body>
</html>