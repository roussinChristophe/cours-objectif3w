<?php
/**
 * Manages the data for the player, according to the CRUD model
 * 
 * @license MIT
 * 
 * @since 1.0.0
 * 
 * @category PHP
 * @package StreetFighterO3W
 * @subpackage Player
 * @copyright 2018 Objectif 3W - all rights reserved
 * @author Damien TIVELET <d.tivelet@objectif3w.com>
 */
class PlayerModel extends Model {
  /**
   * The query string to create the table
   * 
   * @since 1.0.0
   *
   * @const string
   */
  const QUERY_CREATE_TABLE = 'CREATE TABLE IF NOT EXISTS `player` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(120) NOT NULL,
  `firstname` VARCHAR(75) NOT NULL,
  `lastname` VARCHAR(75) NOT NULL,
  `alias` VARCHAR(75) NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT `player_email` UNIQUE (`email`),
  CONSTRAINT `player_alias` UNIQUE (`alias`)
) ENGINE=InnoDB charset=utf8mb4;';



  /**
   * Creates a player in the database
   *
   * @param string $email
   * @param string $firstname
   * @param string $lastname
   * @param string $alias
   * 
   * @throws Exception if an error occured
   * 
   * @return integer The last inserted ID
   */
  public function create(string $email, string $firstname, string $lastname, string $alias): int {
    try {
      if(($req = $this->_pdo->prepare('INSERT INTO `player` (`email`, `firstname`, `lastname`, `alias`) VALUES (?,?,?,?)'))!==false) {
        if($req->bindValue(1, $email) && $req->bindValue(2, $firstname) && $req->bindValue(3, $lastname) && $req->bindValue(4, $alias)) {
          if($req->execute()) {
            $res = $this->_pdo->lastInsertId();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not insert into the database', 12, $e);
    }
  }

  /**
   * Selects one or more player
   *
   * @param integer $id (optional)
   * 
   * @throws Exception if an error occured
   * 
   * @return array The data of one or more player
   */
  public function read(int $id = null): array {
    try {
      if(is_null($id)) {
        if(($req = $this->_pdo->query('SELECT * FROM `player` ORDER BY `alias` ASC'))!==false) {
          $res = $req->fetchAll(PDO::FETCH_ASSOC);
          $req->closeCursor();
          return $res;
        }
      } else {
        if(($req = $this->_pdo->prepare('SELECT * FROM `player` WHERE `id`=?'))!==false) {
          if($req->bindValue(1, $id)) {
            if($req->execute()) {
              $res = $req->fetchAll(PDO::FETCH_ASSOC);
              $req->closeCursor();
              return $res;
            }
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not select in the database', 13, $e);
    }
  }

  /**
   * Updates a player
   *
   * @param integer $id
   * @param string $email
   * @param string $firstname
   * @param string $lastname
   * @param string $alias
   * 
   * @throws Exception if an error occured
   * 
   * @return integer The number of rows affected
   */
  public function update(int $id, string $email, string $firstname, string $lastname, string $alias): int {
    try {
      if(($req = $this->_pdo->prepare('UPDATE `player` SET `email`=?, `firstname`=?, `lastname`=?, `alias`=? WHERE `id`=?'))!==false) {
        if($req->bindValue(1, $email) && $req->bindValue(2, $firstname) && $req->bindValue(3, $lastname) && $req->bindValue(4, $alias) && $req->bindValue(5, $id)) {
          if($req->execute()) {
            $res = $req->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not update in the database', 14, $e);
    }
  }

  /**
   * Deletes a player
   * 
   * @param integer $id
   * 
   * @throws Exception if an error occured
   * 
   * @return integer The number of rows affected
   */
  public function delete(int $id): int {
    try {
      if(($req = $this->_pdo->prepare('DELETE FROM `player` WHERE `id`=?'))!==false) {
        if($req->bindValue(1, $id)) {
          if($req->execute()) {
            $res = $req->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not delete in the database', 15, $e);
    }
  }

  public function signin(string $login) {
    try {
      if(($req = $this->_pdo->prepare('SELECT * FROM `player` WHERE `email`=:login OR `alias`=:login'))!==false) {
        if($req->bindValue('login', $login)) {
          if($req->execute()) {
            $res = $req->fetch(PDO::FETCH_ASSOC);
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not read the database', 16, $e);
    }
  }
}