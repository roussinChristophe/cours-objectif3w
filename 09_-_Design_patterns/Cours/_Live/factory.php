<?php
/**
 * Modèle de déclaration
 */

// class Voiture {
//   private $marque;

//   public function __construct($marque) {
//     $this->marque = $marque;
//   }

//   public function getMarque() {
//     return $this->marque;
//   }
// }


// class VoitureFactory {
//   public static function create($marque) {
//     return new Voiture($marque);
//   }
// }


// $twingo = VoitureFactory::create('Twingo');
// $punto = VoitureFactory::create('Punto');



/**
 * Application pour les controllers
 */

// class ArticleController {}
// class VoitureController {}
// class UtilisateurController {}

// class ControllerFactory {
//   public static function create($controller) {
//     $class = ucfirst(strtolower($controller)) . 'Controller';

//     if(class_exists($class)) {
//       return new $class;
//     }

//     return false;
//   }
// }

// if(!empty($_GET['c'])) {
//   var_dump(ControllerFactory::create($_GET['c']));
// } else {
//   var_dump(ControllerFactory::create('article'));
// }



/**
 * Application pour les connexions à la base de données
 */
interface iDB {
  public function connect(string $dsn);
  public function query(string $queryString, array $binds);
}

class DBMySQL implements iDB {
  public function __construct($dsn) {

  }
  public function connect(string $dsn) {}
  public function query(string $queryString, array $binds) {}
}

class DBOracle implements iDB {
  public function __construct($dsn) {

  }
  public function connect(string $dsn) {}
  public function query(string $queryString, array $binds) {}
}

// $db = new DBMySQL('mysql:user:password@localhost');
// $db2 = new DBOracle('oracle:user:password@localhost');

class DBFactory {
  public static function create($dsn) {
    $driverPos = strpos($dsn, ':');

    switch(substr($dsn, 0, $driverPos)) {
      case 'mysql':
        return new DBMySQL($dsn);
      case 'oracle':
        return new DBOracle($dsn);
    }
  }
}

$db = DBFactory::create('mysql:user:password@localhost');
$db2 = DBFactory::create('oracle:user:password@localhost');

var_dump($db);