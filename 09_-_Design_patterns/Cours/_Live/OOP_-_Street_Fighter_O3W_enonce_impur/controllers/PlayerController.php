<?php
class PlayerController {
  private $_model;

  public function show() {
    include('views/player/index.php');
  }

  private function connect() {
    try {
      $this->_model = new PlayerModel('mysql', 'localhost', 'streetfighter', 'root', '');
    } catch(Exception $e) {
      throw new Exception($e->getMessage(), 0, $e);
    }
  }

  public function logout() {
    if(isset($_SESSION['streetfighter'])) {
      unset($_SESSION['streetfighter']);
    }
    header('Location:.');
    exit;
  }

  public function signin() {
    $this->connect();
    if(($data = $this->_model->signin(htmlentities($_POST['login'])))!==false) {
      $player = new Player($data);

      $_SESSION['streetfighter']['player'] = serialize($player);
      header('Location:.?c=game');
      exit;
    } else {
      $message = 'Mauvais identifiant';
      include('views/player/index.php');
    }
  }

  public function signup(array $data) {
    $this->connect();

    if(($id = $this->_model->create(htmlentities($data['email']), htmlentities($data['firstname']), htmlentities($data['lastname']), htmlentities($data['alias'])))!==false) {
      $player = new Player($data);
      $player->setId($id);

      $_SESSION['streetfighter']['player'] = serialize($player);
      header('Location:.?c=game');
      exit;
    } else {
      $message = 'Problème de connexion avec la base de données. Nous vous présentons nos excuses. Veuillez réessayer ultérieurement';
      include('views/player/index.php');
    }
  }
}