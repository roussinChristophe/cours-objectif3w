<?php
class GameController {
  private $_model;

  public function __construct() {
    try {
      $this->_model = new CharacterModel('mysql', 'localhost', 'streetfighter', 'root', '');
    } catch(Exception $e) {
      $message = $e->getMessage();
      var_dump($e);
    }
  }

  private function fighterRecovery() {
    if(!empty($_SESSION['streetfighter']['fighter'])) {
      $fighter = unserialize($_SESSION['streetfighter']['fighter']);
      $data = $this->_model->readCopy($fighter->getName());
      if(isset($data[0]['damages'])) {
        $fighter->setDamages($data[0]['damages']);
      }

      return $fighter;
    }

    return false;
  }

  public function play() {
    $player = unserialize($_SESSION['streetfighter']['player']);

    $characters = $this->_model->read();
    $targets = $this->_model->readCopy();
    
    include('views/game/index.php');
  }

  public function create() {
    $player = unserialize($_SESSION['streetfighter']['player']);
    $fighter = $this->fighterRecovery();

    if(strtoupper($_POST['name'])=='CHUCK NORRIS') {
      $message = 'Personne ne peut créer Chuck Norris car c\'est lui qui vous crée';
    } else {
      if(($id = $this->_model->create(htmlentities($_POST['name']), Character::MIN_DAMAGES, $player->getId()))!==false) {
        $fighter = new Character($_POST);
        $fighter->setId($id);
        $fighter->setDamages(Character::MIN_DAMAGES);
        $fighter->setPlayer($player);

        if(($id = $this->_model->copy($fighter->getDamages(), $fighter->getId(), $fighter->getPlayer()->getId()))!==false) {
          $fighter->setId($id);
        }
      }
    }

    if(!empty($fighter)) {
      $_SESSION['streetfighter']['fighter'] = serialize($fighter);
    }

    $characters = $this->_model->read();
    $targets = $this->_model->readCopy();
    
    include('views/game/index.php');
  }

  public function choose() {
    $player = unserialize($_SESSION['streetfighter']['player']);
    $fighter = $this->fighterRecovery();

    $data = $this->_model->readCopy($_POST['name']);
    if(count($data)>0) {
      $fighter = new Character($data[0]);
    } else {
      $data = $this->_model->read($_POST['name']);
      if(count($data)>0) {
        $fighter = new Character($data[0]);
        if(($id = $this->_model->copy($fighter->getDamages(), $fighter->getId(), $fighter->getPlayer()->getId()))!==false) {
          $fighter->setId($id);
        }
      }
    }

    if(!empty($fighter)) {
      $_SESSION['streetfighter']['fighter'] = serialize($fighter);
    }

    $characters = $this->_model->read();
    $targets = $this->_model->readCopy();
    
    include('views/game/index.php');
  }

  public function fight() {
    $player = unserialize($_SESSION['streetfighter']['player']);
    $fighter = $this->fighterRecovery();
    
    $data = $this->_model->readCopy($_GET['target']);
    if(count($data)>0) {
      $target = new Character($data[0]);

      if(strtoupper($target->getName())=='CHUCK NORRIS') {
        $message = 'Chuck Norris ne perd jamais !!!';
      } else {
        $hit = $fighter->hit($target);
        switch($hit) {
          case 'self':
            $message = 'Ne te frappe pas toi-même idiot !';
            break;
          default:
            if($hit['receive']==='KO') {
              if($this->_model->delete($target->getId(), $target->getPlayer()->getId())>0) {
                $message = $fighter->getName() . ' a éliminé ' . $target->getName();
              } else {
                $message = 'Un problème est survenue ... Action annulée';
              }
            } else {
              if($this->_model->update($target->getId(), $target->getPlayer()->getId(), $target->getDamages())>0) {
                $message = $fighter->getName() . ' a infligé ' . $hit['hit'] . ' point(s) de dégât(s) à ' . $target->getName() . ' qui a encaissé ' . $hit['receive'];
              } else {
                $message = 'Un problème est survenue ... Action annulée';
              }
            }
        }
      }
    } else {
      if(strtoupper($_GET['target'])=='CHUCK NORRIS') {
        $message = 'Il n \'y a que dans Google qu\'on peut taper Chuck Norris';
      } else {
        $message = 'Tu frappes dans le vent minus !';
      }
    }

    $characters = $this->_model->read();
    $targets = $this->_model->readCopy();
    
    include('views/game/index.php');
  }

  public function init() {
    if(isset($_SESSION['streetfighter']['fighter'])) {
      unset($_SESSION['streetfighter']['fighter']);
    }
    header('Location:.?c=game');
    exit;
  }
}