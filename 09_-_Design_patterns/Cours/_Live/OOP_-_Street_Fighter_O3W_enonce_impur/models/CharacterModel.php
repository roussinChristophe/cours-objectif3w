<?php
/**
 * Manages the data for the character, according to the CRUD model
 * 
 * @license MIT
 * 
 * @since 1.0.0
 * 
 * @category PHP
 * @package StreetFighterO3W
 * @subpackage Character
 * @copyright 2018 Objectif 3W - all rights reserved
 * @author Damien TIVELET <d.tivelet@objectif3w.com>
 */
class CharacterModel extends Model {
  /**
   * The query string to create the table
   * 
   * @since 1.0.0
   *
   * @const string
   */
  const QUERY_CREATE_TABLE = 'CREATE TABLE IF NOT EXISTS `character` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(75) NOT NULL,
  `damages` INT(3) NOT NULL DEFAULT 0,
  `create_date` DATETIME NOT NULL DEFAULT NOW(),
  `player` INT(11) NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT `character_name` UNIQUE (`name`),
  CONSTRAINT `character_player` FOREIGN KEY(`player`) REFERENCES `player`(`id`) ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB charset=utf8mb4;';
  /**
   * The query string to create the copy of the table
   * 
   * @since 1.0.0
   *
   * @const string
   */
  const QUERY_CREATE_TABLE_COPY = 'CREATE TABLE IF NOT EXISTS `character_copy` (
  `damages` INT(3) NOT NULL DEFAULT 0,
  `play_date` DATETIME NOT NULL DEFAULT NOW(),
  `depend` INT(11) NOT NULL,
  `player` INT(11) NOT NULL,
  PRIMARY KEY(`depend`,`player`),
  CONSTRAINT `character_copy_player` FOREIGN KEY(`player`) REFERENCES `player`(`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT `character_copy_character` FOREIGN KEY(`depend`) REFERENCES `character`(`id`) ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB charset=utf8mb4;';



  /**
   * Creates a character in the database
   *
   * @param string $name The name of the character
   * @param integer $damages The damages of the character
   * @param integer $player The Id of the character's player
   * 
   * @throws Exception if an error occured
   * 
   * @return integer The last inserted ID
   */
  public function create(string $name, int $damages, int $player): int {
    try {
      if(($req = $this->_pdo->prepare('INSERT INTO `character` (`name`, `damages`, `player`) VALUES (?,?,?)'))!==false) {
        if($req->bindValue(1, $name) && $req->bindValue(2, $damages) && $req->bindValue(3, $player)) {
          if($req->execute()) {
            $res = $this->_pdo->lastInsertId();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not insert into the database', 12, $e);
    }
  }

  /**
   * Creates a character in the database
   *
   * @param integer $damages The damages of the character
   * @param integer $character The Id of the character's reference
   * @param integer $player The Id of the character's player
   * 
   * @throws Exception if an error occured
   * 
   * @return integer The last inserted ID
   */
  public function copy(int $damages, int $character, int $player): int {
    try {
      if(($req = $this->_pdo->prepare('INSERT INTO `character_copy` (`damages`, `depend`, `player`) VALUES (?,?,?)'))!==false) {
        if($req->bindValue(1, $damages) && $req->bindValue(2, $character) && $req->bindValue(3, $player)) {
          if($req->execute()) {
            $res = $this->_pdo->lastInsertId();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not insert into the database', 12, $e);
    }
  }

  /**
   * Selects one or more character
   *
   * @param string $name (optional) The name of a character
   * 
   * @throws Exception if an error occured
   * 
   * @return array The data of one or more character
   */
  public function read(string $name = null): array {
    try {
      if(is_null($name)) {
        if(($req = $this->_pdo->query('SELECT * FROM `character` ORDER BY `name` ASC'))!==false) {
          $res = array();
          while(($character = $req->fetch(PDO::FETCH_ASSOC))!==false) {
            if(!empty($character['player'])) {
              $playerModel = new PlayerModel($this->_engine, $this->_host, $this->_dbname, $this->_user, $this->_pwd, $this->_charset, $this->_collate);
              $player = $playerModel->read($character['player']);
              $character['player'] = $player[0];
            }
            $res[] = $character;
          }
          $req->closeCursor();
          return $res;
        }
      } else {
        // if(($req = $this->_pdo->prepare('SELECT `character`.*, `email`, `firstname`, `lastname`, `alias` FROM `character` JOIN `player` ON `character`.`player`=`player`.`id` WHERE `name`=?'))!==false) {
        //   if($req->bindValue(1, $name)) {
        //     if($req->execute()) {
        //       $res = $req->fetchAll(PDO::FETCH_ASSOC);
        //       $req->closeCursor();
        //       return $res;
        //     }
        //   }
        // }
        if(($req = $this->_pdo->prepare('SELECT * FROM `character` WHERE `name`=?'))!==false) {
          if($req->bindValue(1, $name)) {
            if($req->execute()) {
              $res = $req->fetchAll(PDO::FETCH_ASSOC);
              if(!empty($res[0]['player'])) {
                $playerModel = new PlayerModel($this->_engine, $this->_host, $this->_dbname, $this->_user, $this->_pwd, $this->_charset, $this->_collate);
                $player = $playerModel->read($res[0]['player']);
                $res[0]['player'] = $player[0];
              }
              $req->closeCursor();
              return $res;
            }
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not select in the database', 13, $e);
    }
  }

  /**
   * Selects one or more character
   *
   * @param string $name (optional) The name of a character
   * 
   * @throws Exception if an error occured
   * 
   * @return array The data of one or more character
   */
  public function readCopy(string $name = null): array {
    try {
      if(is_null($name)) {
        if(($req = $this->_pdo->query('SELECT `character`.`id`, `character`.`name`, `character_copy`.`damages`, `character`.`create_date`, `character_copy`.`play_date`, `character_copy`.`player` FROM `character_copy` JOIN `character` ON `character_copy`.`depend`=`character`.`id` ORDER BY `character`.`name` ASC'))!==false) {
          $res = array();
          while(($character = $req->fetch(PDO::FETCH_ASSOC))!==false) {
            if(!empty($character['player'])) {
              $playerModel = new PlayerModel($this->_engine, $this->_host, $this->_dbname, $this->_user, $this->_pwd, $this->_charset, $this->_collate);
              $player = $playerModel->read($character['player']);
              $character['player'] = $player[0];
            }
            $res[] = $character;
          }
          $req->closeCursor();
          return $res;
        }
      } else {
        // if(($req = $this->_pdo->prepare('SELECT `character`.*, `email`, `firstname`, `lastname`, `alias` FROM `character` JOIN `player` ON `character`.`player`=`player`.`id` WHERE `name`=?'))!==false) {
        //   if($req->bindValue(1, $name)) {
        //     if($req->execute()) {
        //       $res = $req->fetchAll(PDO::FETCH_ASSOC);
        //       $req->closeCursor();
        //       return $res;
        //     }
        //   }
        // }
        if(($req = $this->_pdo->prepare('SELECT `character`.`id`, `character`.`name`, `character_copy`.`damages`, `character`.`create_date`, `character_copy`.`play_date`, `character_copy`.`player` FROM `character_copy` JOIN `character` ON `character_copy`.`depend`=`character`.`id` WHERE `character`.`name`=?'))!==false) {
          if($req->bindValue(1, $name)) {
            if($req->execute()) {
              $res = $req->fetchAll(PDO::FETCH_ASSOC);
              if(!empty($res[0]['player'])) {
                $playerModel = new PlayerModel($this->_engine, $this->_host, $this->_dbname, $this->_user, $this->_pwd, $this->_charset, $this->_collate);
                $player = $playerModel->read($res[0]['player']);
                $res[0]['player'] = $player[0];
              }
              $req->closeCursor();
              return $res;
            }
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not select in the database', 13, $e);
    }
  }

  /**
   * Updates a character
   *
   * @param integer $character The of a character's model
   * @param integer $player The player of a character
   * @param integer $damages The damages of the character
   * 
   * @throws Exception if an error occured
   * 
   * @return integer The number of rows affected
   */
  public function update(int $character, int $player, int $damages): int {
    try {
      if(($req = $this->_pdo->prepare('UPDATE `character_copy` SET `damages`=? WHERE `depend`=? AND `player`=?'))!==false) {
        if($req->bindValue(1, $damages) && $req->bindValue(2, $character) && $req->bindValue(3, $player)) {
          if($req->execute()) {
            $res = $req->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not update in the database', 14, $e);
    }
  }

  /**
   * Deletes a character
   * 
   * @param integer $character The of a character's model
   * @param integer $player The player of a character
   * 
   * @throws Exception if an error occured
   * 
   * @return integer The number of rows affected
   */
  public function delete(int $character, int $player): int {
    try {
      if(($req = $this->_pdo->prepare('DELETE FROM `character_copy` WHERE `depend`=? AND `player`=?'))!==false) {
        if($req->bindValue(1, $character) && $req->bindValue(2, $player)) {
          if($req->execute()) {
            $res = $req->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not delete in the database', 15, $e);
    }
  }
}