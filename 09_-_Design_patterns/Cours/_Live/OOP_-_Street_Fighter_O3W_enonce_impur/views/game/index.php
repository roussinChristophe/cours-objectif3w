<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Street fighter O3W</title>

    <!-- <meta http-equiv="refresh" content="1; url=.?c=game"> -->
  </head>
  <body>
    <p>Bonjour <?php echo $player->getAlias(); ?></p>
    <a href=".?deconnect">Se déconnecter</a>
    <a href=".?c=game&del">Réinitialiser</a>

    <aside>
      <form action=".?c=game" method="post">
        <input type="text" name="name" placeholder="Nom du personnage" aria-label="Nom du personnage">
        <button name="create" type="submit">Créer le personnage</button>
      </form>
      <?php
      if(isset($fighter)) {
        echo '<p>Vous incarnez : ' . $fighter->getName() . ' (' . $fighter->getDamages() . ')</p>';
      }
      ?>
      <?php if(!empty($characters) && count($characters)>1) { ?>
      <form action=".?c=game" method="post">
        <select name="name">
          <option></option>
          <?php
          foreach($characters as $data) {
            $character = new Character($data);
            if($character!=$fighter && $character->getPlayer()==$player) {
          ?>
          <option value="<?php echo $character->getName(); ?>"><?php echo $character->getName(); ?> (<?php echo $character->getDamages(); ?>)</option>
          <?php
            }
          }
          ?>
        </select>
        <button name="choose" type="submit">Changer de personnage</button>
      </form>
      <?php } ?>
    </aside>

    <?php if(!empty($targets) && !empty($fighter)) { ?>
    <aside>
      <h2>Liste de vos adversaires possibles : </h2>
      <ul>
        <?php
        foreach($targets as $data) {
          $character = new Character($data);
          if($character->getPlayer()!=$player) {
        ?>
        <li><a href="?c=game&target=<?php echo urlencode($character->getName()); ?>"><?php echo $character->getName(); ?> (<?php echo $character->getDamages(); ?>)</a></li>
        <?php
          }
        }
        ?>
      </ul>
    </aside>
    <?php } ?>
    
    <?php
    if(!empty($message)) {
      echo '<p>' . $message . '</p>';
    }
    ?>
  </body>
</html>