<?php
require_once( 'vendor/Conversation.php' );

class ConversationController {
    private $model;

    public function __construct() {
        $this->model = new ConversationModel;
    }

    public function showAction() {
        $conversations = $this->model->selectAll();
        $title = 'Liste des conversations';
        include( 'views/inc/header.php' );
        include( 'views/conversation.php' );
        include( 'views/inc/footer.php' );
    }
}