<?php
class ContactController {
    const TITLE = 'Contact';

    public function showAction() {
        $title = self::TITLE;
        include( 'views/inc/header.php' );
        include( 'views/contact.php' );
        include( 'views/inc/footer.php' );
    }

    public function sendAction() {
        if( isset( $_POST['email'] ) ) {
            $result = 'Message envoyé';
        } else {
            $result = 'Remplir tous les champs !';
        }

        $title = self::TITLE;
        $styles = '<style>body { color:red; }</style>';
        include( 'views/inc/header.php' );
        include( 'views/send.php' );
        include( 'views/inc/footer.php' );
    }
}