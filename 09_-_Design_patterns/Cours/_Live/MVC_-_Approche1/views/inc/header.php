<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $title; ?></title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <?php if( isset( $styles ) ) echo $styles; ?>
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="." title="Accueil">Accueil</a></li>
                    <li><a href=".?c=contact" title="Contact">Contact</a></li>
                </ul>
            </nav>
        </header>