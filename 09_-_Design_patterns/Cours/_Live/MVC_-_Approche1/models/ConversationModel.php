<?php
class ConversationModel {
    private $pdo;

    public function __construct() {
        $this->pdo = SPDO::getInstance()->getDb();
    }

    public function selectAll() {
        try {
            if( ( $statement = $this->pdo->query( 'SELECT `c_id` AS `ID de la conversation`, DATE_FORMAT( `c_date`, "%d/%m/%Y" ) AS `Date de la conversation`, DATE_FORMAT( `c_date`, "%T" ) AS `Heure de la conversation`, COUNT( DISTINCT `m_id` ) AS `Nombre de messages`, `c_termine` FROM `conversation` LEFT JOIN `message` ON `conversation`.`c_id`=`message`.`m_conversation_fk` GROUP BY `c_id` ORDER BY `c_id` ASC' ) )!==false ) {
                $conversations = array();

                foreach( $statement->fetchAll( PDO::FETCH_ASSOC ) as $conversationDatas ) {
                    $conversations[] = new Conversation( $conversationDatas );
                }

                return $conversations;
            }

            return false;
        } catch( PDOException $e ) {
            die( $e->getMessage() );
        }
    }
}