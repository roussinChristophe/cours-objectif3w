<?php
define( 'DB_HOST', 'localhost' );
define( 'DB_NAME', 'cours_forum' );
define( 'DB_LOGIN', 'root' );
define( 'DB_PWD', '' );

require_once( 'vendor/SPDO.php' );


if( isset( $_GET['c'] ) ) {
    $controllerName = ucfirst( strtolower( $_GET['c'] ) ) . 'Controller';

    if( file_exists( 'controllers/' . $controllerName . '.php' ) ) {
        require_once( 'controllers/' . $controllerName . '.php' );

        if( class_exists( $controllerName ) ) {
            $controller = new $controllerName;

            if( isset( $_GET['a'] ) ) {
                $methodName = strtolower( $_GET['a'] ) . 'Action';
                if( method_exists( $controller, $methodName ) ) {
                    $controller->$methodName();
                } else {
                    header( 'Location: 404' );
                }
            } else {
                $controller->showAction();
            }
        } else {
            header( 'Location: 404' );
        }
    } else {
        header( 'Location: 404' );
    }

} else {
    require_once( 'controllers/ConversationController.php' );
    require_once( 'models/ConversationModel.php' );

    $controller = new ConversationController;
    $controller->showAction();
}