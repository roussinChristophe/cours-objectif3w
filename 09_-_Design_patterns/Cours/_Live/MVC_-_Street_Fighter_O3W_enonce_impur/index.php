<?php
session_start();

require_once('classes/Player.php');
require_once('classes/Character.php');
require_once('models/Model.php');
require_once('models/PlayerModel.php');
require_once('models/CharacterModel.php');
require_once('controllers/PlayerController.php');
require_once('controllers/GameController.php');


if(empty($_GET['c'])) {
  $controller = new PlayerController;

  if(isset($_GET['deconnect'])) {
    $controller->logout();
  }

  if(!empty($_SESSION['streetfighter']['player'])) {
    $player = unserialize($_SESSION['streetfighter']['player']);
  }

  if(isset($_POST['signin']) || isset($_POST['signup'])) {
      if(isset($_POST['signin']) && !empty($_POST['login'])) {
        $controller->signin();
      }

      if(isset($_POST['signup']) && !empty($_POST['email']) && !empty($_POST['alias'])) {
        $controller->signup();
      }
  } else {
    $controller->show();
  }
} else {
  if(empty($_SESSION['streetfighter']['player'])) {
    header('Location:.?_err=403');
    exit;
  }
  
  $controller = new GameController;

  if(isset($_GET['del'])) {
    $controller->init();
  } elseif(!empty($_POST['name'])) {
    if(isset($_POST['create'])) {
      $controller->create();
    } elseif(isset($_POST['choose'])) {
      $controller->choose();
    }
  } elseif(!empty($_GET['target'])) {
    $controller->fight();
  } else {
    $controller->play();
  }
}