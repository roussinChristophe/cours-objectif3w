<?php
session_start();

if(empty($_SESSION['streetfighter']['player'])) {
  header('Location:.?_err=403');
  exit;
}

require_once('classes/Player.php');
require_once('classes/Character.php');
require_once('models/Model.php');
require_once('models/PlayerModel.php');
require_once('models/CharacterModel.php');

$player = unserialize($_SESSION['streetfighter']['player']);

if(isset($_GET['del'])) {
  if(isset($_SESSION['streetfighter']['fighter'])) {
    unset($_SESSION['streetfighter']['fighter']);
  }
  header('Location:game.php');
  exit;
}

try {
  $characterModel = new CharacterModel('mysql', 'localhost', 'streetfighter', 'root', '');

  if(!empty($_SESSION['streetfighter']['fighter'])) {
    $fighter = unserialize($_SESSION['streetfighter']['fighter']);
    $data = $characterModel->readCopy($fighter->getName());
    if(isset($data[0]['damages'])) {
      $fighter->setDamages($data[0]['damages']);
    }
  }

  if(!empty($_POST['name'])) {
    if(isset($_POST['create'])) {
      if(strtoupper($_POST['name'])=='CHUCK NORRIS') {
        $message = 'Personne ne peut créer Chuck Norris car c\'est lui qui vous crée';
      } else {
        if(($id = $characterModel->create(htmlentities($_POST['name']), Character::MIN_DAMAGES, $player->getId()))!==false) {
          $fighter = new Character($_POST);
          $fighter->setId($id);
          $fighter->setDamages(Character::MIN_DAMAGES);
          $fighter->setPlayer($player);

          if(($id = $characterModel->copy($fighter->getDamages(), $fighter->getId(), $fighter->getPlayer()->getId()))!==false) {
            $fighter->setId($id);
          }
        }
      }
    } elseif(isset($_POST['choose'])) {
      $data = $characterModel->readCopy($_POST['name']);
      if(count($data)>0) {
        $fighter = new Character($data[0]);
      } else {
        $data = $characterModel->read($_POST['name']);
        if(count($data)>0) {
          $fighter = new Character($data[0]);
          if(($id = $characterModel->copy($fighter->getDamages(), $fighter->getId(), $fighter->getPlayer()->getId()))!==false) {
            $fighter->setId($id);
          }
        }
      }
    }

    if(!empty($fighter)) {
      $_SESSION['streetfighter']['fighter'] = serialize($fighter);
    }
  }

  if(!empty($fighter) && !empty($_GET['target'])) {
    $data = $characterModel->readCopy($_GET['target']);
    if(count($data)>0) {
      $target = new Character($data[0]);

      if(strtoupper($target->getName())=='CHUCK NORRIS') {
        $message = 'Chuck Norris ne perd jamais !!!';
      } else {
        $hit = $fighter->hit($target);
        switch($hit) {
          case 'self':
            $message = 'Ne te frappe pas toi-même idiot !';
            break;
          default:
            if($hit['receive']==='KO') {
              if($characterModel->delete($target->getId(), $target->getPlayer()->getId())>0) {
                $message = $fighter->getName() . ' a éliminé ' . $target->getName();
              } else {
                $message = 'Un problème est survenue ... Action annulée';
              }
            } else {
              if($characterModel->update($target->getId(), $target->getPlayer()->getId(), $target->getDamages())>0) {
                $message = $fighter->getName() . ' a infligé ' . $hit['hit'] . ' point(s) de dégât(s) à ' . $target->getName() . ' qui a encaissé ' . $hit['receive'];
              } else {
                $message = 'Un problème est survenue ... Action annulée';
              }
            }
        }
      }
    } else {
      if(strtoupper($_GET['target'])=='CHUCK NORRIS') {
        $message = 'Il n \'y a que dans Google qu\'on peut taper Chuck Norris';
      } else {
        $message = 'Tu frappes dans le vent minus !';
      }
    }
  }

  $characters = $characterModel->read();
  $targets = $characterModel->readCopy();
} catch(Exception $e) {
  $message = $e->getMessage();
  var_dump($e);
}
?><!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Street fighter O3W</title>

    <!-- <meta http-equiv="refresh" content="1; url=game.php"> -->
  </head>
  <body>
    <p>Bonjour <?php echo $player->getAlias(); ?></p>
    <a href=".?deconnect">Se déconnecter</a>
    <a href="?del">Réinitialiser</a>

    <aside>
      <form action="game.php" method="post">
        <input type="text" name="name" placeholder="Nom du personnage" aria-label="Nom du personnage">
        <button name="create" type="submit">Créer le personnage</button>
      </form>
      <?php
      if(isset($fighter)) {
        echo '<p>Vous incarnez : ' . $fighter->getName() . ' (' . $fighter->getDamages() . ')</p>';
      }
      ?>
      <?php if(!empty($characters) && count($characters)>1) { ?>
      <form action="game.php" method="post">
        <select name="name">
          <option></option>
          <?php
          foreach($characters as $data) {
            $character = new Character($data);
            if($character!=$fighter && $character->getPlayer()==$player) {
          ?>
          <option value="<?php echo $character->getName(); ?>"><?php echo $character->getName(); ?> (<?php echo $character->getDamages(); ?>)</option>
          <?php
            }
          }
          ?>
        </select>
        <button name="choose" type="submit">Changer de personnage</button>
      </form>
      <?php } ?>
    </aside>

    <?php if(!empty($targets) && !empty($fighter)) { ?>
    <aside>
      <h2>Liste de vos adversaires possibles : </h2>
      <ul>
        <?php
        foreach($targets as $data) {
          $character = new Character($data);
          if($character->getPlayer()!=$player) {
        ?>
        <li><a href="?target=<?php echo urlencode($character->getName()); ?>"><?php echo $character->getName(); ?> (<?php echo $character->getDamages(); ?>)</a></li>
        <?php
          }
        }
        ?>
      </ul>
    </aside>
    <?php } ?>
    
    <?php
    if(!empty($message)) {
      echo '<p>' . $message . '</p>';
    }
    ?>
  </body>
</html>