<?php
class Ville extends Core {
  private $_id;
  private $_nom;
  private $_superficie;



  public function setId(int $id): self {
    $this->_id = $id;
    return $this;
  }

  public function setNom(string $nom): self {
    $this->_nom = $nom;
    return $this;
  }

  public function setSuperficie(int $superficie): self {
    $this->_superficie = $superficie;
    return $this;
  }



  public function getId(): int {
    return $this->_id;
  }

  public function getNom(): string {
    return $this->_nom;
  }

  public function getSuperficie(): int {
    return $this->_superficie;
  }
}