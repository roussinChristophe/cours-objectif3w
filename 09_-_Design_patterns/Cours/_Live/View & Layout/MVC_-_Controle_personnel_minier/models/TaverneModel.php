<?php
class TaverneModel extends CoreModel {
  const TABLE = 'taverne';
  const PREFIX = 't_';

  public function create(string $nom, int $chambres, bool $blonde, bool $brune, bool $rousse, int $ville_fk) {
    try {
      if(($req = $this->_db->prepare('INSERT INTO `taverne`(`t_nom`, `t_chambres`, `t_blonde`, `t_brune`, `t_rousse`, `t_ville_fk`) VALUES (?, ?, ?, ?, ?, ?)'))!==false) {
        if($req->bindValue(1, $nom)
          && $req->bindValue(2, $chambres, PDO::PARAM_INT)
          && $req->bindValue(3, $blonde, PDO::PARAM_BOOL)
          && $req->bindValue(4, $brune, PDO::PARAM_BOOL)
          && $req->bindValue(5, $rousse, PDO::PARAM_BOOL)
          && $req->bindValue(6, $ville_fk, PDO::PARAM_INT)) {
            if($req->execute()) {
              $res = $this->_db->lastInsertId();
              $req->closeCursor();
              return $res;
            }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not create into the database', 10, $e);
    }
  }

  public function read(int $id = null) {
    try {
      if(empty($id)) {
        if(($req = $this->_db->query('SELECT `taverne`.*, (`t_chambres` - COUNT(`n_id`)) AS `t_chambresLibres` FROM `taverne` 
LEFT JOIN `groupe` ON `t_id`=`g_taverne_fk` 
LEFT JOIN `nain` ON `g_id`=`n_groupe_fk` GROUP BY `t_id` ORDER BY `t_nom` ASC'))!==false) {
          $res = $req->fetchAll(PDO::FETCH_ASSOC);
          $req->closeCursor();
          return $res;
        }
      } else {
        if(($req = $this->_db->prepare('SELECT `taverne`.*, (`t_chambres` - COUNT(`n_id`)) AS `t_chambresLibres` FROM `taverne` 
LEFT JOIN `groupe` ON `t_id`=`g_taverne_fk` 
LEFT JOIN `nain` ON `g_id`=`n_groupe_fk` WHERE `t_id`=?'))!==false) {
          if($req->bindValue(1, $id, PDO::PARAM_INT)) {
            if($req->execute()) {
              $res = $req->fetchAll(PDO::FETCH_ASSOC);
              $req->closeCursor();
              return $res;
            }
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not read from the database', 10, $e);
    }
  }

  public function readByVille(int $id) {
    try {
      if(($req = $this->_db->prepare('SELECT * FROM `taverne` WHERE `t_ville_fk`=?'))!==false) {
        if($req->bindValue(1, $id, PDO::PARAM_INT)) {
          if($req->execute()) {
            $res = $req->fetchAll(PDO::FETCH_ASSOC);
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not read from the database', 10, $e);
    }
  }

  public function update(int $id, string $nom, int $chambres, bool $blonde, bool $brune, bool $rousse, int $ville_fk) {
    try {
      if(($req = $this->_db->prepare('UPDATE `taverne` SET `t_nom`=?, `t_chambres`=?, `t_blonde`=?, `t_brune`=?, `t_rousse`=?, `t_ville_fk`=? WHERE `t_id`=?'))!==false) {
        if($req->bindValue(1, $nom)
          && $req->bindValue(2, $chambres, PDO::PARAM_INT)
          && $req->bindValue(3, $blonde, PDO::PARAM_BOOL)
          && $req->bindValue(4, $brune, PDO::PARAM_BOOL)
          && $req->bindValue(5, $rousse, PDO::PARAM_BOOL)
          && $req->bindValue(6, $ville_fk, PDO::PARAM_INT)
          && $req->bindValue(7, $id, PDO::PARAM_INT)) {
          if($req->execute()) {
            $res = $this->_db->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not update the database', 10, $e);
    }
  }
}