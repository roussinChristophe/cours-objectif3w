<?php
class VilleModel extends CoreModel {
  const TABLE = 'ville';
  const PREFIX = 'v_';

  public function create(string $nom, int $superficie) {
    try {
      if(($req = $this->_db->prepare('INSERT INTO `ville`(`v_nom`, `v_superficie`) VALUES (?, ?)'))!==false) {
        if($req->bindValue(1, $nom)
          && $req->bindValue(2, $superficie, PDO::PARAM_INT)) {
            if($req->execute()) {
              $res = $this->_db->lastInsertId();
              $req->closeCursor();
              return $res;
            }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not create into the database', 10, $e);
    }
  }

  public function update(int $id, string $nom, int $superficie) {
    try {
      if(($req = $this->_db->prepare('UPDATE `ville` SET `v_nom`=?, `v_superficie`=? WHERE `v_id`=?'))!==false) {
        if($req->bindValue(1, $nom)
          && $req->bindValue(2, $superficie, PDO::PARAM_INT)
          && $req->bindValue(3, $id, PDO::PARAM_INT)) {
          if($req->execute()) {
            $res = $this->_db->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not update the database', 10, $e);
    }
  }
}