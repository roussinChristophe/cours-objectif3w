<?php
class NainModel extends CoreModel {
  const TABLE = 'nain';
  const PREFIX = 'n_';

  public function create(string $nom, float $barbe, int $groupe, int $ville) {
    try {
      if(($req = $this->_db->prepare('INSERT INTO `nain`(`n_nom`, `n_barbe`, `n_groupe_fk`, `n_ville_fk`) VALUES (?, ?, ?, ?)'))!==false) {
        if($req->bindValue(1, $nom)
          && $req->bindValue(2, $barbe)
          && $req->bindValue(3, $groupe, PDO::PARAM_INT)
          && $req->bindValue(4, $ville, PDO::PARAM_INT)) {
            if($req->execute()) {
              $res = $this->_db->lastInsertId();
              $req->closeCursor();
              return $res;
            }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not create into the database', 10, $e);
    }
  }

  public function update(int $id, string $nom, float $barbe, int $groupe, int $ville) {
    try {
      if(($req = $this->_db->prepare('UPDATE `nain` SET `n_nom`=?, `n_barbe`=?, `n_groupe_fk`=?, `n_ville_fk`=? WHERE `n_id`=?'))!==false) {
        if($req->bindValue(1, $nom)
          && $req->bindValue(2, $barbe)
          && $req->bindValue(3, $groupe, PDO::PARAM_INT)
          && $req->bindValue(4, $ville, PDO::PARAM_INT)
          && $req->bindValue(5, $id, PDO::PARAM_INT)) {
          if($req->execute()) {
            $res = $this->_db->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not update the database', 10, $e);
    }
  }

  public function readByVille(int $id) {
    try {
      if(($req = $this->_db->prepare('SELECT * FROM `nain` WHERE `n_ville_fk`=?'))!==false) {
        if($req->bindValue(1, $id, PDO::PARAM_INT)) {
          if($req->execute()) {
            $res = $req->fetchAll(PDO::FETCH_ASSOC);
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not read from the database', 10, $e);
    }
  }
}