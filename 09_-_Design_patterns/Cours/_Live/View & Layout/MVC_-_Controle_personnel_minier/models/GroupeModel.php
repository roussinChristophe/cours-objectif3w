<?php
class GroupeModel extends CoreModel {
  const TABLE = 'groupe';
  const PREFIX = 'g_';

  public function create(string $debuttravail, string $fintravail, int $taverne, int $tunnel) {
    try {
      if(($req = $this->_db->prepare('INSERT INTO `groupe`(`g_debuttravail`, `g_fintravail`, `g_taverne_fk`, `g_tunnel_fk`) VALUES (?, ?, ?, ?)'))!==false) {
        if($req->bindValue(1, $debuttravail)
          && $req->bindValue(2, $fintravail)
          && $req->bindValue(3, $taverne, PDO::PARAM_INT)
          && $req->bindValue(4, $tunnel, PDO::PARAM_INT)) {
            if($req->execute()) {
              $res = $this->_db->lastInsertId();
              $req->closeCursor();
              return $res;
            }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not create into the database', 10, $e);
    }
  }

  public function read(int $id = null) {
    try {
      if(empty($id)) {
        if(($req = $this->_db->query('SELECT * FROM `' . static::TABLE . '` ORDER BY `' . static::PREFIX . 'id` ASC'))!==false) {
          $res = $req->fetchAll(PDO::FETCH_ASSOC);
          $req->closeCursor();
          return $res;
        }
      } else {
        if(($req = $this->_db->prepare('SELECT * FROM `' . static::TABLE . '` WHERE `' . static::PREFIX . 'id`=?'))!==false) {
          if($req->bindValue(1, $id, PDO::PARAM_INT)) {
            if($req->execute()) {
              $res = $req->fetchAll(PDO::FETCH_ASSOC);
              $req->closeCursor();
              return $res;
            }
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not read from the database', 10, $e);
    }
  }

  public function update(int $id, string $debuttravail, string $fintravail, int $taverne, int $tunnel) {
    try {
      if(($req = $this->_db->prepare('UPDATE `groupe` SET `g_debuttravail`=?, `g_fintravail`=?, `g_taverne_fk`=?, `g_tunnel_fk`=? WHERE `g_id`=?'))!==false) {
        if($req->bindValue(1, $debuttravail)
          && $req->bindValue(2, $fintravail)
          && $req->bindValue(3, $taverne, PDO::PARAM_INT)
          && $req->bindValue(4, $tunnel, PDO::PARAM_INT)
          && $req->bindValue(5, $id, PDO::PARAM_INT)) {
          if($req->execute()) {
            $res = $this->_db->rowCount();
            $req->closeCursor();
            return $res;
          }
        }
      }

      return false;
    } catch(PDOException $e) {
      throw new Exception('Can not update the database', 10, $e);
    }
  }
}