<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nains</title>
  </head>
  <body>
    <ul>
      <?php foreach($nains as $nain) { ?>
      <li><a href="?c=nain&a=card&id=<?php echo $nain->getId(); ?>"><?php echo $nain->getNom(); ?></a></li>
      <?php } ?>
    </ul>
  </body>
</html>