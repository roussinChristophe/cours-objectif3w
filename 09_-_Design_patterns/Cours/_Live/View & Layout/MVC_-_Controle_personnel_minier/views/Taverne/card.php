<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Taverne</title>
  </head>
  <body>
    <h1><?php echo $taverne->getNom(); ?></h1>
    <ul>
      <li>Se trouve à <a href="?c=ville&a=card&id=<?php echo $taverne->getVille()->getId(); ?>"><?php echo $taverne->getVille()->getNom(); ?></a></li>
      <li>Nombre de chambres : <?php echo $taverne->getChambresLibres(); ?> libres sur <?php echo $taverne->getChambres(); ?></li>
      <li>Bières disponibles :
        <ul>
          <?php if($taverne->getBlonde()) { ?><li>Blonde</li><?php } ?>
          <?php if($taverne->getBrune()) { ?><li>Brune</li><?php } ?>
          <?php if($taverne->getRousse()) { ?><li>Rousse</li><?php } ?>
        </ul>
      </li>
    </ul>
  </body>
</html>