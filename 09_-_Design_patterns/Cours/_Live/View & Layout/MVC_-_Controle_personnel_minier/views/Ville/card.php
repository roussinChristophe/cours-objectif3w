<h1><?php echo $ville->getNom(); ?></h1>
<ul>
  <li>Superficie : <?php echo $ville->getSuperficie(); ?></li>
  <li>Nains originaires :
    <ul>
      <?php foreach($nains as $nain) { ?>
      <li><a href="?c=nain&a=card&id=<?php echo $nain->getId(); ?>"><?php echo $nain->getNom(); ?></a></li>
      <?php } ?>
    </ul>
  </li>
  <li>Tavernes :
    <ul>
      <?php foreach($tavernes as $taverne) { ?>
      <li><a href="?c=taverne&a=card&id=<?php echo $taverne->getId(); ?>"><?php echo $taverne->getNom(); ?></a></li>
      <?php } ?>
    </ul>
  </li>
  <li>Tunnels :
    <ul>
      <?php
      foreach($tunnels as $tunnel) {
        if($tunnel->getVilleArrivee()->getId()==$ville->getId()) {
      ?>
      <li>vers <a href="?c=ville&a=card&id=<?php echo $tunnel->getVilleDepart()->getId(); ?>"><?php echo $tunnel->getVilleDepart()->getNom(); ?></a> / <?php echo $tunnel->getStatus(); ?></li>
      <?php } else { ?>
      <li>vers <a href="?c=ville&a=card&id=<?php echo $tunnel->getVilleArrivee()->getId(); ?>"><?php echo $tunnel->getVilleArrivee()->getNom(); ?></a> / <?php echo $tunnel->getStatus(); ?></li>
      <?php
        }
      }
      ?>
    </ul>
  </li>
</ul>