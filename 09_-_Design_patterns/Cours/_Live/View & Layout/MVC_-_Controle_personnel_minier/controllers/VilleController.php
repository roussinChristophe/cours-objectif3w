<?php
class VilleController extends CoreController {
  const MODEL = 'Ville';

  public function list() {
    $class = static::MODEL;

    $datas = $this->_model->read();
    
    foreach($datas as $data) {
      $ville = new $class($data);
      $villes[] = $ville;
    }

    $this->render('index', ['title'=>static::MODEL, 'villes'=>$villes]);
  }
}