<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>
  <body>
    <nav>
      <ul>
        <li><a href=".">Joueurs</a></li>
        <li><a href=".?ctrl=character">Personnages</a></li>
      </ul>
    </nav>
    <h1>Fiche de <?php echo $character->getName(); ?></h1>
    <ul>
      <li>Dégâts : <?php echo $character->getDamages(); ?></li>
    </ul>
    <a href=".?ctrl=character" title="Liste des personnages">Retour à la liste des personnages</a>

    <?php if(isset($msg)) {
      echo '<p>' . $msg . '</p>';
    }
    ?>
    <form action="?ctrl=character&action=update&id=<?php echo $_GET['id']; ?>" method="post">
      <input type="text" name="email" placeholder="Nom" value="<?php echo $character->getName(); ?>">
      <input type="number" name="damages" placeholder="Dégâts" value="<?php echo $character->getDamages(); ?>">
      <button name="update" type="submit">Mettre à jour</button>
    </form>
    <form action="?ctrl=character&action=delete&id=<?php echo $_GET['id']; ?>" method="post">
      <button name="delete" type="submit">Supprimer</button>
    </form>
  </body>
</html>