<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>
  <body>
    <nav>
      <ul>
        <li><a href=".">Joueurs</a></li>
        <li><a href=".?ctrl=character">Personnages</a></li>
      </ul>
    </nav>
    <?php if(isset($msg)) {
      echo '<p>' . $msg . '</p>';
    }
    ?>
    <ul>
      <?php
      foreach($users as $user) {
        $player = new Player($user);
      ?>
      <li><a href="?action=card&id=<?php echo $player->getId(); ?>" title="Afficher la fiche de <?php echo $player->getAlias(); ?>"><?php echo $player->getAlias(); ?></a></li>
      <?php
      } 
      ?>
    </ul>
    <a href="?action=create" title="Ajouter un utilisateur">Ajouter</a>
  </body>
</html>