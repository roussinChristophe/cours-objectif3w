<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>
  <body>
    <nav>
      <ul>
        <li><a href=".">Joueurs</a></li>
        <li><a href=".?ctrl=character">Personnages</a></li>
      </ul>
    </nav>
    <a href="." title="Liste des joueurs">Retour à la liste des joueurs</a>

    <?php if(isset($msg)) {
      echo '<p>' . $msg . '</p>';
    }
    ?>
    <form action="?action=send" method="post">
      <input type="email" name="email" placeholder="E-mail">
      <input type="text" name="firstname" placeholder="Prénom">
      <input type="text" name="lastname" placeholder="Nom">
      <input type="text" name="alias" placeholder="Pseudo">
      <button name="create" type="submit">Ajouter</button>
    </form>
  </body>
</html>