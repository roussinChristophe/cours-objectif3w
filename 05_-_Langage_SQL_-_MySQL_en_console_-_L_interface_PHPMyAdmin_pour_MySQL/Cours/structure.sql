--# ------------------------------
--# BASE DE DONNÉES
--# ------------------------------

--# Création
CREATE DATABASE IF NOT EXISTS `nom_de_ma_base` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

--# Suppression
DROP DATABASE IF EXISTS `nom_de_ma_base`;

--# Utilisation
USE `nom_de_ma_base`;

--# Visualisation de toutes les bases de données accessibles à l'utilisateur
SHOW DATABASES;



--# ------------------------------
--# TABLE (ENTITÉ)
--# ------------------------------

--# Création
CREATE TABLE IF NOT EXISTS `nom_de_ma_table` (
    `id_champs` INT(11) NOT NULL,
    `libelle_champs` VARCHAR(50) NOT NULL DEFAULT "Ma valeur par défaut"
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--# Altération post-création de la table pour ajouter une clé primaire sur un champs
ALTER TABLE `nom_de_ma_table` ADD PRIMARY KEY (`id_champs`);

--# Altération post-création de la table pour supprimer une clé primaire sur un champs
ALTER TABLE `nom_de_ma_table` DROP PRIMARY KEY;

--# Altération post-création de la table modifier la structure d'un champs (attribut)
ALTER TABLE `nom_de_ma_table` CHANGE `id_champs` `id_champs` INT(11) NOT NULL AUTO_INCREMENT;

--# Altérations combinées post-création de la table pour ajouter une clé primaire sur un champs et modifier la structure d'un champs pour le mettre en incrémentation automatique
ALTER TABLE `nom_de_ma_table` ADD PRIMARY KEY (`id_champs`), CHANGE `id_champs` `id_champs` INT(11) NOT NULL AUTO_INCREMENT; --# /!\ Attention à l'ordre des instructions /!\ L'incrémentation automatique ne peut se faire que sur une clé primaire

--# Création avec actions combinées
CREATE TABLE IF NOT EXISTS `nom_de_ma_table2` (
    `id_champs` INT(11) NOT NULL AUTO_INCREMENT,
    `date_champs` DATE NOT NULL DEFAULT "0000-00-00",
    PRIMARY KEY (`id_champs`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--# Altérations post-création de la table pour ajouter des champs
ALTER TABLE `nom_de_ma_table` ADD `fk1_id_champs` INT(11) NOT NULL AFTER `date_champs`, ADD `fk2_id_champs` INT(11) NOT NULL AFTER `fk1_id_champs`;

--# Altérations post-création de la table pour ajouter une indexation sur des champs afin de placer des contraintes de clés étrangères pour les relations
ALTER TABLE `nom_de_ma_table` ADD KEY( `fk1_id_champs`, `fk2_id_champs`), ADD CONSTRAINT `constraint_table1_table2` FOREIGN KEY (`fk1_id_champs`) REFERENCES `nom_de_ma_base`.`nom_de_ma_table2` (`id_champs`) ON UPDATE CASCADE ON DELETE RESTRICT, ADD CONSTRAINT `constraint_table1_table3` FOREIGN KEY (`fk2_id_champs`) REFERENCES `nom_de_ma_base`.`nom_de_ma_table3` (`id_champs`) ON DELETE RESTRICT ON UPDATE CASCADE;

--# Création avec actions combinées complète
CREATE TABLE IF NOT EXISTS `nom_de_ma_table2` (
    `id_champs` INT(11) NOT NULL AUTO_INCREMENT,
    `date_champs` DATE NOT NULL DEFAULT "0000-00-00",
    `fk1_id_champs` INT(11) NOT NULL,
    `fk2_id_champs` INT(11) NOT NULL,
    PRIMARY KEY (`id_champs`),
    KEY ( `fk1_id_champs`, `fk2_id_champs`),
    CONSTRAINT `constraint_table1_table2` FOREIGN KEY (`fk1_id_champs`) REFERENCES `nom_de_ma_table2` (`id_champs`) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT `constraint_table1_table3` FOREIGN KEY (`fk2_id_champs`) REFERENCES `nom_de_ma_table3` (`id_champs`) ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--# Altérations post-création de la table pour supprimer une contrainte de clés étrangères pour les relations
ALTER TABLE `nom_de_ma_table` DROP FOREIGN KEY `constraint_table1_table3`;

--# Altérations post-création de la table pour supprimer un champs (/!\ ATTENTION : un champ ne peut être supprimé que s'il n'est soumis à aucune contrainte)
ALTER TABLE `nom_de_ma_table` DROP `fk2_id_champs`;

--# Visualisation de toutes les tables de la base de données
SHOW TABLES;

--# Visualisation de tous les champs de la table
SHOW COLUMNS FROM `nom_de_ma_table`;
--# ou
DESCRIBE `nom_de_ma_table`;

--# Visualisation de la structure d'une table avec ses contraintes
SHOW CREATE TABLE `nom_de_ma_table`;

--# Pour importer un fichier depuis un terminal si vous n'êtes pas connecté à votre serveur MySQL (/!\ la base de données doit être créée auparavant) :
--# mysql -h 127.0.0.1 -u root -p NomDeLaBaseDeDonneesVisee < "Chemin\Vers\Le\Fichier.SQL"
--# Pour importer un fichier depuis un terminal si vous êtes pas connecté à votre serveur MySQL :
--# SOURCE Chemin\Vers\Le\Fichier.SQL