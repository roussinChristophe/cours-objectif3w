USE `popo`;

-- --------------------------------------------------------
-- C.R.U.D. : Create Read Update Delete
-- --------------------------------------------------------

--
-- Requêtes d'insertion (CREATE)
--

--# Si nous souhaitons envoyer une valeur dans tous les champs de la table et dans le même ordre que la déclaration, nous pouvons juste lister les valeurs voulues
INSERT INTO `rang` VALUES ( 1, "SuperAdmin" );
--# Puisque la clé primaire est en AUTO INCREMENT dans ce cas, nous pouvons laisser la valeur à NULL pour que le système la gère
INSERT INTO `rang` VALUES ( NULL, "SuperAdmin" );
--# Si nous souhaitons renseigner les valeurs dans un ordre différent de celui de la déclaration des champs, nous pouvons préciser l'ordre
INSERT INTO `rang`( `r_libelle`, `r_id` ) VALUES ( "Invité", 3 );
--# Puisque la clé primaire est en AUTO INCREMENT dans ce cas, nous pouvons ignorer le champ pour que le système le gère. Pour cela, nous devons préciser les champs pour lesquels nous envoyons une valeur
INSERT INTO `rang`( `r_libelle` ) VALUES ( "Abonné" );
/*
Nous pouvons saisir plusieurs lignes dans la même instruction
Puisque la table `user` contient des champs pouvant être null, l'envoi de données dans ces derniers n'est pas obligatoire
Nous pouvons utiliser des fonctions MySQL (par exemple, NOW() donne le moment actuel)
*/
INSERT INTO `user` ( `u_login`, `u_prenom`, `u_date_inscription`, `u_rang_fk` ) VALUES
    ( "the_boss_du_34", "Moi et moi seul", "2017-03-07 11:46:56", 1 ),
    ( "the_subboss_du_34", "L'autre", NOW(), 2 ),
    ( "the_subboss_du_34", "L'autre 2", NOW(), 2 );
--# Puisque la table `conversation` contient des champs ayant des valeurs par défaut, l'envoi de données dans ces derniers n'est pas obligatoire puisque non nulle par défaut
INSERT INTO `conversation`( `c_date` ) VALUES ( NOW() ); --# Nous pouvons aussi utiliser des fonctions SQL comme NOW()

-- --------------------------------------------------------

--
-- Requêtes de sélection (READ)
--
--# On sélectionne tous les champs dans toutes les lignes d'enregistrement de la table
SELECT * FROM `user`;
--# On limite le nombre de résultats d'une sélection (ex: les 20 premiers résultats)
SELECT * FROM `user` LIMIT 20;
--# On pagine le nombre de résultats d'une sélection (ex: les 20 résultats après le 40ème)
SELECT * FROM `user` LIMIT 40, 20;
--# On ordonne les résultats d'une sélection selon un ou plusieurs critères
SELECT * FROM `user` ORDER BY `u_date_inscription` DESC, `u_login` ASC;
--# On compte toutes les lignes d'enregistrement de la table grçace à une fonction d'aggrégation SQL
SELECT COUNT(`u_id`) FROM `user`;
--# On sélectionne la ligne d'enregistrement correspondant à un identifiant précis
SELECT * FROM `user` WHERE `u_id`=3;
--# On sélectionne la(les) ligne(s) d'enregistrement correspondant exactement à la condition
SELECT * FROM `user` WHERE `u_login`="admin";
--# On sélectionne la(les) ligne(s) d'enregistrement correspondant exactement parmis plusieurs conditions
SELECT * FROM `user` WHERE `u_login`="admin" OR `u_login`="Admin" OR `u_login`="superadmin" OR `u_login`="SuperAdmin";
--# On sélectionne la(les) ligne(s) d'enregistrement correspondant exactement parmis plusieurs conditions
SELECT * FROM `user` WHERE `u_login` IN( "admin", "Admin", "superadmin", "SuperAdmin" );
--# On sélectionne la(les) ligne(s) d'enregistrement correspondant à la condition sans tenir compte de la casse
SELECT * FROM `user` WHERE `u_login` LIKE "admin" OR `u_login` LIKE "superadmin";
--# On sélectionne la(les) ligne(s) d'enregistrement dont la valeur se termine par la condition sans tenir compte de la casse
SELECT * FROM `user` WHERE `u_login` LIKE "%admin";
--# On sélectionne la(les) ligne(s) d'enregistrement dont la valeur commence par la condition sans tenir compte de la casse
SELECT * FROM `user` WHERE `u_login` LIKE "admin%";
--# On sélectionne la(les) ligne(s) d'enregistrement dont la condition se trouve dans la valeur sans tenir compte de la casse
SELECT * FROM `user` WHERE `u_login` LIKE "%admin%";
--# On sélectionne la(les) ligne(s) d'enregistrement correspondant à plusieurs conditions en même temps
SELECT * FROM `user` WHERE `u_login` LIKE "%admin%" AND `u_nom`=NULL;
--# On sélectionne la(les) ligne(s) d'enregistrement correspondant à une borne minimale
SELECT * FROM `user` WHERE `u_date_naissance` > "1970-01-01";
--# On sélectionne la(les) ligne(s) d'enregistrement correspondant à une borne maximale
SELECT * FROM `user` WHERE `u_date_naissance` < "2017-12-31";
--# On sélectionne la(les) ligne(s) d'enregistrement correspondant à un intervalle
SELECT * FROM `user` WHERE `u_date_naissance` BETWEEN "1970-01-01" AND "2017-12-31";
--# On personnalise l'affichage des colonnes en listant les champs que l'on souhaite sortir à l'affichage
SELECT `u_id`, `u_date_inscription` FROM `user` WHERE `u_login` LIKE "the_subboss%";
--# On donne un alias à un champs
SELECT `u_id` AS `Identifiant`, `u_login` AS `Nom d'utilisateur` FROM `user`;
--# On donne un alias à une table
SELECT * FROM `user` AS `u`;
--# On sélectionne la(les) ligne(s) d'enregistrement correspondant à un élément d'une liste prédéfinie générée par une sous-requète (ex: les utilisateurs qui ont posté au moins un message)
--# On appelle cela une sous-requête.
--# /!\ Attention néanmoins aux performances de cette pratique
SELECT * FROM `user` WHERE `u_id` IN (
    SELECT DISTINCT(`m_auteur_fk`) FROM `message`
);
--# On compléte notre sélection avec des données liées à une autre table, grâce aux jointures (plus performant qu'une sous-requète).
--# /!\ Attention les jointures doivent toujours se faire sur les liens entre les clés étrangères et primaires pour conserver une cohérence des données
SELECT * FROM `user` JOIN `message` ON `user`.`u_id`=`message`.`m_auteur_fk`;
--# On personnalise l'affichage des colonnes en listant les champs d'une table précise que l'on souhaite sortir à l'affichage
SELECT `user`.*, `rang`.`r_libelle` FROM `user` JOIN `rang` ON `user`.`u_rang_fk`=`rang`.`r_id`;
--# On compléte notre sélection avec des données liées à plusieurs autres tables, grâce aux jointures
SELECT * FROM `user` JOIN `rang` ON `user`.`u_rang_fk`=`rang`.`r_id` JOIN `message` ON `user`.`u_id`=`message`.`m_auteur_fk` JOIN `conversation` ON `message`.`m_conversation_fk`=`conversation`.`c_id`;
--# On regroupe des données après une jointure
SELECT * FROM `user` JOIN `rang` ON `user`.`u_rang_fk`=`rang`.`r_id` JOIN `message` ON `user`.`u_id`=`message`.`m_auteur_fk` JOIN `conversation` ON `message`.`m_conversation_fk`=`conversation`.`c_id` GROUP BY `u_id`;
--# On conditionne un résultat en fonction d'un alias de sélection
SELECT `user`.*, `m_contenu` AS `Contenu` FROM `message` JOIN `user` ON `message`.`m_auteur_fk`=`user`.`u_id` GROUP BY `u_login` HAVING `Contenu` LIKE "Bla%";
/*
Il existe différents types de jointures ayant des effets différents sur le jeu de résultats
cf : infographie
*/
--# Ordre des instructions cumulées ([Optionnel])
--# SELECT {champ}, {champ}, ... 
--# FROM {table} 
--# [JOIN {table} ON {cle}={cle}] 
--# [WHERE {condition(s)}] 
--# [GROUP BY {champ}] 
--# [HAVING {condition(s)}] 
--# [ORDER BY {champ} ASC/DESC] 
--# [LIMIT ({limite} || {debut},{limite})]

-- --------------------------------------------------------

--
-- Requêtes de modification (UPDATE)
-- Ici s'appliqueront les contraintes de clés étrangères ON UPDATE : CASCADE propagera la modification, RESTRICT empêchera la modification
--

--# On modifie la valeur d'un champ dans toutes les lignes d'enregistrement de la table
UPDATE `user` SET `u_date_naissance`="1989-05-26";
--# On modifie la valeurs de plusieurs champs dans toutes les lignes d'enregistrement de la table
UPDATE `user` SET `u_date_naissance`="1989-05-26", `u_prenom`="Test";
--# On modifie la ligne d'enregistrement correspondant à un identifiant précis pour ne pas toucher toutes les lignes d'enregistrement de la table
UPDATE `user` SET `u_prenom`="Mon prénom" WHERE `u_id`=3;
/*
...
Toutes les conditions de WHERE s'appliquent aussi sur les requêtes de modification
*/

-- --------------------------------------------------------

--
-- Requêtes de suppression (DELETE)
-- Ici s'appliqueront les contraintes de clés étrangères ON DELETE : CASCADE propagera la modification, RESTRICT empêchera la modification
--

--# On supprime toutes les lignes d'enregistrement de la table
DELETE FROM `user`;
--# On supprime la ligne d'enregistrement correspondant à un identifiant précis
DELETE FROM `user` WHERE `u_id`=3;
/*
...
Toutes les conditions de WHERE s'appliquent aussi sur les requêtes de suppression
*/