-- phpMyAdmin SQL Dump

--
-- Base de données :  `Gestion_De_Personnel_Minier_En_Zone_De_Creusement_Intensif`
-- CREATE DATABASE IF NOT EXISTS `gurdil` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci; 
--

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE `ville` (
  `v_id` INT(11) NOT NULL AUTO_INCREMENT,
  `v_nom` VARCHAR(255) NOT NULL,
  `v_superficie` INT(11) NOT NULL,
  PRIMARY KEY (`v_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `taverne`
--

CREATE TABLE `taverne` (
  `t_id` INT(11) NOT NULL AUTO_INCREMENT,
  `t_nom` VARCHAR(255) NOT NULL,
  `t_chambres` INT(11) NOT NULL,
  `t_blonde` TINYINT(1) NOT NULL,
  `t_brune` TINYINT(1) NOT NULL,
  `t_rousse` TINYINT(1) NOT NULL,
  `t_ville_fk` INT(11) NOT NULL,
  PRIMARY KEY (`t_id`),
  KEY (`t_ville_fk`),
  CONSTRAINT `taverne_ville_ibfk` FOREIGN KEY (`t_ville_fk`) REFERENCES `ville` (`v_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tunnel`
--

CREATE TABLE `tunnel` (
  `t_id` INT(11) NOT NULL AUTO_INCREMENT,
  `t_progres` FLOAT NOT NULL,
  `t_villedepart_fk` INT(11) NOT NULL,
  `t_villearrivee_fk` INT(11) NOT NULL,
  PRIMARY KEY (`t_id`),
  KEY (`t_villedepart_fk`),
  KEY (`t_villearrivee_fk`),
  CONSTRAINT `tunnel_villedepart_ibfk` FOREIGN KEY (`t_villedepart_fk`) REFERENCES `ville` (`v_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tunnel_villearrivee_ibfk` FOREIGN KEY (`t_villearrivee_fk`) REFERENCES `ville` (`v_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `g_id` INT(11) NOT NULL AUTO_INCREMENT,
  `g_debuttravail` TIME NOT NULL,
  `g_fintravail` TIME NOT NULL,
  `g_taverne_fk` INT(11) DEFAULT NULL,
  `g_tunnel_fk` INT(11) DEFAULT NULL,
  PRIMARY KEY (`g_id`),
  KEY (`g_taverne_fk`),
  KEY (`g_tunnel_fk`),
  CONSTRAINT `groupe_taverne_ibfk` FOREIGN KEY (`g_taverne_fk`) REFERENCES `taverne` (`t_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `groupe_tunnel_ibfk` FOREIGN KEY (`g_tunnel_fk`) REFERENCES `tunnel` (`t_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `nain`
--

CREATE TABLE `nain` (
  `n_id` INT(11) NOT NULL AUTO_INCREMENT,
  `n_nom` VARCHAR(255) NOT NULL,
  `n_barbe` FLOAT NOT NULL,
  `n_groupe_fk` INT(11) DEFAULT NULL,
  `n_ville_fk` INT(11) NOT NULL,
  PRIMARY KEY (`n_id`),
  KEY (`n_groupe_fk`),
  KEY (`n_ville_fk`),
  CONSTRAINT `nain_groupe_ibfk` FOREIGN KEY (`n_groupe_fk`) REFERENCES `groupe` (`g_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nain_ville_ibfk` FOREIGN KEY (`n_ville_fk`) REFERENCES `ville` (`v_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;