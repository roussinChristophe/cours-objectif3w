<?php
/*
ALGORITHME combinatoire( E p : ENTIER, E n : ENTIER ) : ENTIER
ENTRER p, n
    SI p = 0 || p = n ALORS
        tmp = 1
    SINON
        tmp = combinatoire( p, n-1 ) + combinatoire ( p-1, n-1 )
    FINSI
    ÉCRIRE "n=", n, "p=", p, "résultat=", tmp
RETOURNER tmp

DÉBUT
    ÉCRIRE combinatoire( 5, 10 )
FIN
*/

/**
 * combinatoire Calcule et renvoie le résultat de la combinatoire de deux nombres, en affichant au passage les étapes récursives
 * @param int $p
 * @param int $n
 * @return int
 */
function combinatoire( int $p, int $n ) : int {
    if( $p == 0 || $p == $n ) :
        $tmp = 1;
    else :
        $tmp = combinatoire( $p, $n-1 ) + combinatoire ( $p-1, $n-1 );
    endif;
    echo 'n=' . $n . ' p=' . $p . ' résultat=' . $tmp . "\r\n";
    
    return $tmp;
}

echo combinatoire( 5, 10 );