<?php
/*
ALGORITHME factorielle( E n : ENTIER ) : ENTIER
ENTRER n
    SI n = 0 ALORS
        tmp = 1
    SINON
        tmp = factorielle( n-1 ) * n
    FINSI
RETOURNER tmp

DÉBUT
    ÉCRIRE factorielle( 5 )
FIN
*/

/**
 * factorielle Calcule et renvoie la factorielle d'un nombre
 * @param int $n
 * @return int
 */
function factorielle( int $n ) : int {
    if( $n == 0 ) :
        $tmp = 1;
    else :
        $tmp = factorielle( $n-1 ) * $n;
    endif;

    return $tmp;
}

echo factorielle( 5 );