<?php
// Définition du title de la page.
$metaTitle = 'Le partitionnement et l\'inclusion de pages';
// Définition du titre principal de la page.
$pageTitle = $metaTitle;

// Inclusion du fichier contenant l'en-tête de la page
include('07_-_Le_partitionnement_et_l_inclusion_de_pages/header.php');
?>
        <p><em>En PHP, nous pouvons facilement insérer d'autres pages (on peut aussi insérer seulement des morceaux de pages) à l'intérieur d'une page. Cela va nous permettre de centraliser des morceaux de codes que l'on peut trouver sur plusieurs pages et ainsi simplifier notre maintenance.</em></p>
        <pre><code class="php">
&lt;?php include('07_-_Le_partitionnement_et_l_inclusion_de_pages/header.php'); ?&gt;
&lt;?php include('07_-_Le_partitionnement_et_l_inclusion_de_pages/footer.html'); ?&gt;
</code></pre>
        <p><em>L'instruction "include" possède une déclinaison permettant de ne pas répéter une seconde fois l'inclusion : include_once</p></em>
        <p><em>Dans le cas où le fichier ne serait pas présent à l'URI indiquée en paramètre, l'inclusion avec "include" générera un avertissement (warning), ce qui ne bloquera pas la tentative pour continuer l'exécution du code.<br>Cette instruction est donc à utiliser si le fichier n'a pas une grande importance pour la suite du script.<br>Dans le cas contraire, vous pouvez employer l'instruction "require" (ou sa déclinaison "require_once") qui bloquera le script avec une erreur (fatal error).</p></em>

<?php
include('07_-_Le_partitionnement_et_l_inclusion_de_pages/footer.html');