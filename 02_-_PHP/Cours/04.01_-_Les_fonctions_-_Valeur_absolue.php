<?php
/*
ALGORITHME abs( E unEntier : ENTIER ) : ENTIER
ENTRER unEntier
    SI unEntier>=0 ALORS
        tmp = unEntier
    SINON
        tmp = ( unEntier * -1 )
    FINSI
RETOURNER tmp

DÉBUT
    REQUÊTE "Saisir un entier ", a
    b = abs( a )
    ÉCRIRE "La valeur absolue de ", a, "est", b
FIN
*/

/**
 * valAbs Calcule et renvoie la valeur absolue d'un nombre
 * @param int $unEntier
 * @return int
 */
function valAbs( int $unEntier ) : int {
    if( $unEntier>=0 ) :
        $tmp = $unEntier;
    else :
        $tmp = ( $unEntier * -1 );
    endif;
    
    return $tmp;
}

$a = readline( "Saisir un entier : " );
$b = valAbs( $a );
echo 'La valeur absolue de ' . $a . ' est ' . $b;
// echo "La valeur absolue de $a est $b";