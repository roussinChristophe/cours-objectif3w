<?php
/*
DÉBUT
    \\ Affichage de la moyenne des notes d’une promo saisies
    sum = 0
    RÉPÉTER
        REQUÊTE "Nombre d'élèves (max. 100) ? ", nbStudents
    JUSQU'À nbStudents>0 ET nbStudents <= 100
    POUR i = 1 JUSQU'À nbStudents INCRÉMENT 1 FAIRE
        REQUÊTE "Saisissez une note : ", score[i]
        sum = sum + score[i]
    FINPOUR
    ÉCRIRE "Moyenne des notes :", ( sum / nbStudents )
    POUR i = 1 JUSQU'À nbStudents INCRÉMENT 1 FAIRE
        ÉCRIRE "Note n°", i, ":", score[i]
    FINPOUR
FIN
*/

$sum = 0;
$score = [];
do {
    $nbStudents = readline( "Nombre d'élèves (max. 100) ? " );
} while( $nbStudents<0 || $nbStudents > 100 );

for( $i = 1; $i <= $nbStudents; $i++ ) {
    $score[$i] = readline( "Saisissez une note : " );
    $sum = $sum + $score[$i];
}

echo "Moyenne des notes :" . ( $sum / $nbStudents ) . "\r\n";
for( $i = 1; $i <= $nbStudents; $i++ ) {
    echo "Note n°" . $i . " : " . $score[$i] . "\r\n";
}

echo "Le nombre d'éléments dans le tableau est de : " . count( $score ) . "\r\n";