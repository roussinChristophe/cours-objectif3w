<?php
/*
DÉBUT
    \\ Saisir des données et s'arrêter dès que leur somme dépasse
    \\ 500.
    somme = 0
    RÉPÉTER
        REQUÊTE "Saisir une valeur : ", val
        somme = somme + val
    JUSQU'À somme > 500
FIN
*/
$somme = 0;
do {
    $val = readline( "Saisir une valeur : " );
    $somme += $val; // $somme = $somme + $val;
} while( $somme <= 500 );