<?php
/**
 * calc Effectue un calcul
 * @param string $operande1
 * @param string $operateur
 * @param string $operande2
 * @return mixed
 */
function calc( string $operande1, string $operateur, string $operande2 ) {
    if( $operande1!='' && $operande2!='' ) : // Si les données attendues sont présentes et non vides,
        if( is_numeric( str_replace( ',', '.', $operande1 ) ) && is_numeric( str_replace( ',', '.', $operande2 ) ) ) : // Si les données sont numériques (une fois le caractère virgule remplacé par le caractère point pour respecter le format numérique),
            if( strpos( $operande1, ',' )!==false ) :
                $operande1 = str_replace( ',', '.', $operande1 ); // On remplace le caractère virgule par le caractère point pour respecter le format numérique
                $sepOperande1 = ',';
            endif;
            if( strpos( $operande2, ',' )!==false ) :
                $operande2 = str_replace( ',', '.', $operande2 ); // On remplace le caractère virgule par le caractère point pour respecter le format numérique
                $sepOperande2 = ',';
            endif;

            switch( $operateur ) : // Selon l'opérateur,
                case '+': // Dans le cas d'une addition :
                    return ( isset( $sepOperande1 ) && $sepOperande1==',' && isset( $sepOperande2 ) && $sepOperande2==',' ? str_replace( '.', ',', $operande1 + $operande2 ) : $operande1 + $operande2 ); // On effectue le calcul
                    break;
                case '-':
                    return ( isset( $sepOperande1 ) && $sepOperande1==',' && isset( $sepOperande2 ) && $sepOperande2==',' ? str_replace( '.', ',', $operande1 - $operande2 ) : $operande1 - $operande2 ); // On effectue le calcul
                    break;
                case '*':
                return ( isset( $sepOperande1 ) && $sepOperande1==',' && isset( $sepOperande2 ) && $sepOperande2==',' ? str_replace( '.', ',', $operande1 * $operande2 ) : $operande1 * $operande2 ); // On effectue le calcul
                    break;
                case '/':
                    if( $operande2=='0' )
                        return 'La division par 0 n\'est pas possible !'; // Si le deuxième champs envoyé contient un 0, on indique une erreur.
                    else
                        return ( isset( $sepOperande1 ) && $sepOperande1==',' && isset( $sepOperande2 ) && $sepOperande2==',' ? str_replace( '.', ',', $operande1 / $operande2 ) : $operande1 / $operande2 ); // Sinon, on effectue le calcul.
                    break;
            endswitch;
        else :
            if( isset( $sepOperande1 ) && $sepOperande1==',' )
                $operande1 = str_replace( '.', ',', $operande1 ); // On remplace le caractère point par le caractère virgule pour respecter le format d'origine
            if( isset( $sepOperande2 ) && $sepOperande2==',' )
                $operande2 = str_replace( '.', ',', $operande2 ); // On remplace le caractère point par le caractère virgule pour respecter le format d'origine
            return ( !is_numeric( $operande1 ) ? $operande1 . ' n\'est pas un nombre valide pour une opération !' . ( !is_numeric( $operande2 ) ? '<br>' . $operande2 . ' n\'est pas un nombre valide pour une opération !' : '' ) : ( !is_numeric( $operande2 ) ? $operande2 . ' n\'est pas un nombre valide pour une opération !' : '' ) );
        endif;
    else :
        return ( !isset( $operande1 ) || $operande1=='' ? 'Veuillez saisir une valeur pour la première opérande !' . ( !isset( $operande2 ) || $operande2=='' ? '<br>Veuillez saisir une valeur pour la deuxième opérande !' : '' ) : ( !isset( $operande2 ) || $operande2=='' ? 'Veuillez saisir une valeur pour la deuxième opérande !' : '' ) );
    endif;
} ?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title>Calculatrice | Le passage de données - Mise en pratique</title>

        <link rel="stylesheet" type="text/css" href="../../_assets/css/style.css">

        <!-- // highlight.js : Coloration syntaxique du code -->
        <link rel="stylesheet" type="text/css" href="../../_assets/plugins/highlight/styles/monokai_sublime.css">
        <script type="text/javascript" src="../../_assets/plugins/highlight/highlight.pack.js"></script>
        <script type="text/javascript">
            hljs.initHighlightingOnLoad();
        </script>
        <!-- // -->

        <style type="text/css">
            input[type="text"] {
                color:blue;
                padding-left:10px;
                padding-right:10px;
                text-align:right;
                width:150px;
            }
        </style>
    </head>
    <body>
        <h1>Calculatrice | Le passage de données - Mise en pratique</h1>
        <hr>
        <form action="" method="POST" name="frm-calcul">
            <input name="operande1" type="number" step="0.01" placeholder="Saisir un nombre"<?php if( isset( $_POST['operande1'] ) ) echo ' value="' . $_POST['operande1'] . '"'; ?>>
            <select name="operateur">
                <option<?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='+' ) echo ' selected="selected"'; ?> value="+">+</option>
                <option<?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='-' ) echo ' selected="selected"'; ?> value="-">-</option>
                <option<?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='*' ) echo ' selected="selected"'; ?> value="*">x</option>
                <option<?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='/' ) echo ' selected="selected"'; ?> value="/">/</option>
            </select>
            <input name="operande2" type="number" step="0.01" placeholder="Saisir un nombre"<?php if( isset( $_POST['operande2'] ) ) echo ' value="' . $_POST['operande2'] . '"'; ?>>
            <input type="submit" value="=">

            <?php
            if( isset( $_POST['operande1'] ) && isset( $_POST['operateur'] ) && isset( $_POST['operande2'] ) ) :
                $result = calc( $_POST['operande1'], $_POST['operateur'], $_POST['operande2'] );
                echo is_numeric( str_replace( ',', '.', $result ) ) ? '<input disabled="disabled" placeholder="Résultat" type="text" value="' . $result . '">' : '<span style="background-color:red;color:white;display:block;margin:10px 0;padding:4px 7px;">' . $result . '</span>';
            endif;
            ?>

            <hr>
            <label>Historique :</label>
            <br>
            <textarea name="historique" cols="50" rows="50"><?php echo ( isset( $result ) && is_numeric( $result ) ? $_POST['operande1'] . $_POST['operateur'] . $_POST['operande2'] . '=' . $result : '' ) . ( isset( $_POST['historique'] ) ? PHP_EOL . $_POST['historique'] : '' ); ?></textarea>
        </form>
        <form action="" method="POST" name="frm-reset">
            <input type="submit" value="Reset">
        </form>
        <pre><code class="php">
&lt;?php
/**
 * calc Effectue un calcul
 * @param string $operande1
 * @param string $operateur
 * @param string $operande2
 * @return mixed
 */
function calc( string $operande1, string $operateur, string $operande2 ) {
    if( $operande1!='' && $operande2!='' ) : // Si les données attendues sont présentes et non vides,
        if( is_numeric( str_replace( ',', '.', $operande1 ) ) && is_numeric( str_replace( ',', '.', $operande2 ) ) ) : // Si les données sont numériques (une fois le caractère virgule remplacé par le caractère point pour respecter le format numérique),
            if( strpos( $operande1, ',' )!==false ) :
                $operande1 = str_replace( ',', '.', $operande1 ); // On remplace le caractère virgule par le caractère point pour respecter le format numérique
                $sepOperande1 = ',';
            endif;
            if( strpos( $operande2, ',' )!==false ) :
                $operande2 = str_replace( ',', '.', $operande2 ); // On remplace le caractère virgule par le caractère point pour respecter le format numérique
                $sepOperande2 = ',';
            endif;

            switch( $operateur ) : // Selon l'opérateur,
                case '+': // Dans le cas d'une addition :
                    return ( isset( $sepOperande1 ) && $sepOperande1==',' && isset( $sepOperande2 ) && $sepOperande2==',' ? str_replace( '.', ',', $operande1 + $operande2 ) : $operande1 + $operande2 ); // On effectue le calcul
                    break;
                case '-':
                    return ( isset( $sepOperande1 ) && $sepOperande1==',' && isset( $sepOperande2 ) && $sepOperande2==',' ? str_replace( '.', ',', $operande1 - $operande2 ) : $operande1 - $operande2 ); // On effectue le calcul
                    break;
                case '*':
                return ( isset( $sepOperande1 ) && $sepOperande1==',' && isset( $sepOperande2 ) && $sepOperande2==',' ? str_replace( '.', ',', $operande1 * $operande2 ) : $operande1 * $operande2 ); // On effectue le calcul
                    break;
                case '/':
                    if( $operande2=='0' )
                        return 'La division par 0 n\'est pas possible !'; // Si le deuxième champs envoyé contient un 0, on indique une erreur.
                    else
                        return ( isset( $sepOperande1 ) && $sepOperande1==',' && isset( $sepOperande2 ) && $sepOperande2==',' ? str_replace( '.', ',', $operande1 / $operande2 ) : $operande1 / $operande2 ); // Sinon, on effectue le calcul.
                    break;
            endswitch;
        else :
            if( isset( $sepOperande1 ) && $sepOperande1==',' )
                $operande1 = str_replace( '.', ',', $operande1 ); // On remplace le caractère point par le caractère virgule pour respecter le format d'origine
            if( isset( $sepOperande2 ) && $sepOperande2==',' )
                $operande2 = str_replace( '.', ',', $operande2 ); // On remplace le caractère point par le caractère virgule pour respecter le format d'origine
            return ( !is_numeric( $operande1 ) ? $operande1 . ' n\'est pas un nombre valide pour une opération !' . ( !is_numeric( $operande2 ) ? '&lt;br&gt;' . $operande2 . ' n\'est pas un nombre valide pour une opération !' : '' ) : ( !is_numeric( $operande2 ) ? $operande2 . ' n\'est pas un nombre valide pour une opération !' : '' ) );
        endif;
    else :
        return ( !isset( $operande1 ) || $operande1=='' ? 'Veuillez saisir une valeur pour la première opérande !' . ( !isset( $operande2 ) || $operande2=='' ? '&lt;br&gt;Veuillez saisir une valeur pour la deuxième opérande !' : '' ) : ( !isset( $operande2 ) || $operande2=='' ? 'Veuillez saisir une valeur pour la deuxième opérande !' : '' ) );
    endif;
}</code>
<code class="php">
&lt;form action="" method="POST" name="frm-calcul"&gt;
    &lt;input name="operande1" type="number" step="0.01" placeholder="Saisir un nombre"&lt;?php if( isset( $_POST['operande1'] ) ) echo ' value="' . $_POST['operande1'] . '"'; ?&gt;&gt;
    &lt;select name="operateur"&gt;
        &lt;option&lt;?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='+' ) echo ' selected="selected"'; ?&gt; value="+"&gt;+&lt;/option&gt;
        &lt;option&lt;?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='-' ) echo ' selected="selected"'; ?&gt; value="-"&gt;-&lt;/option&gt;
        &lt;option&lt;?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='*' ) echo ' selected="selected"'; ?&gt; value="*"&gt;x&lt;/option&gt;
        &lt;option&lt;?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='/' ) echo ' selected="selected"'; ?&gt; value="/"&gt;/&lt;/option&gt;
    &lt;/select&gt;
    &lt;input name="operande2" type="number" step="0.01" placeholder="Saisir un nombre"&lt;?php if( isset( $_POST['operande2'] ) ) echo ' value="' . $_POST['operande2'] . '"'; ?&gt;&gt;
    &lt;input type="submit" value="="&gt;

    &lt;?php
    if( isset( $_POST['operande1'] ) && isset( $_POST['operateur'] ) && isset( $_POST['operande2'] ) ) :
        $result = calc( $_POST['operande1'], $_POST['operateur'], $_POST['operande2'] );
        echo is_numeric( str_replace( ',', '.', $result ) ) ? '&lt;input disabled="disabled" placeholder="Résultat" type="text" value="' . $result . '"&gt;' : '&lt;span style="background-color:red;color:white;display:block;margin:10px 0;padding:4px 7px;"&gt;' . $result . '&lt;/span&gt;';
    endif;
    ?&gt;

    &lt;hr&gt;
    &lt;label&gt;Historique :&lt;/label&gt;
    &lt;br&gt;
    &lt;textarea name="historique" cols="50" rows="50"&gt;&lt;?php echo ( isset( $result ) && is_numeric( $result ) ? $_POST['operande1'] . $_POST['operateur'] . $_POST['operande2'] . '=' . $result : '' ) . ( isset( $_POST['historique'] ) ? PHP_EOL . $_POST['historique'] : '' ); ?&gt;&lt;/textarea&gt;
&lt;/form&gt;
&lt;form action="" method="POST" name="frm-reset"&gt;
    &lt;input type="submit" value="Reset"&gt;
&lt;/form&gt;&lt;/code&gt;&lt;/pre&gt;
    </body>
</html>