<?php
/*
ENTRER 
    REQUÊTE "Confirmez-vous l'écriture d'une cellule (o/n) ? ", confirm
    tmp = ""
    SI confirm="o" ALORS
        tmp = "\n            <td></td>" . genTD()
    FINSI
RETOURNER tmp

ENTRER 
    REQUÊTE "Confirmez-vous l'écriture d'une ligne (o/n) ? ", confirm
    tmp = ""
    SI confirm="o" ALORS
        tmp = "\n        <tr>" . genTD() . "\n        </tr>" . genTR()
    FINSI
RETOURNER tmp

ENTRER 
RETOURNER "<table>\n    <tbody>" . genTR() . "\n    </tbody>\n</table>"

DÉBUT
    \\ En utilisant une fonction récursive, écrire un algorithme
    \\ qui écrit la structure d'un tableau HTML
    \\ (<table><tr><td></td></tr></table>) en permettant
    \\ l'écriture de plusieurs lignes et plusieurs cellules si
    \\ l'utilisateur indique qu'il souhaite poursuivre ch
    ÉCRIRE genTABLE()
FIN
*/

/**
 * genTD Génère et renvoie une cellule de tableau
 * @return string
 */
function genTD() : string {
    $confirm = readline( "Confirmez-vous l'écriture d'une cellule (o/n) ? " );
    $tmp = "";
    if( $confirm=="o" ) :
        $tmp = "\r\n            <td></td>" . genTD();
    endif;
    
    return $tmp;
}

/**
 * genTR Génère et renvoie une ligne de tableau
 * @return string
 */
function genTR() : string {
    $confirm = readline( "Confirmez-vous l'écriture d'une ligne (o/n) ? " );
    $tmp = "";
    if( $confirm=="o" ) :
        $tmp = "\r\n        <tr>" . genTD() . "\r\n        </tr>" . genTR();
    endif;
    
    return $tmp;
}

/**
 * genTABLE Génère et renvoie un tableau
 * @return string
 */
function genTABLE() : string {
    return "<table>\r\n    <tbody>" . genTR() . "\r\n    </tbody>\r\n</table>";
}

echo genTABLE();