<?php
/*
ENTRER n
    resultat = 0
    SI n > 0 ALORS
        SI n = 1 ALORS
            resultat = 1
        SINON
            resultat = fibonacci( n - 1 ) + fibonacci( n - 2 )
        FINSI
    FINSI
RETOURNER resultat

DÉBUT
    \\ En utilisant une fonction récursive, écrire un algorithme
    \\ qui détermine le terme U(n) de la suite de Fibonacci
    \\ définie comme suit :
    \\ U0 = 0
    \\ U1 = 1
    \\ Un = U(n-1) + U(n-2), n >= 2
    REQUÊTE "Saisir un nombre entier positif : ", n
    ÉCRIRE "Le résultat de Fibonacci pour", n, "est égale à :", fibonacci( n )
FIN
*/

/**
 * fibonnaci Calcule le nombre correspondant à la suite de Fibonnaci
 * @param int $n
 * @return int
 */
function fibonacci( int $n ) : int {
    $resultat = $n;
    if( $n > 0 ) :
        if( $n == 1 ) :
            $resultat = 1;
        else :
            $resultat = fibonacci( $n - 1 ) + fibonacci( $n - 2 );
        endif;
    endif;

    return $resultat;
}
//// Optimisation
// function fibonacci( int $n ) : int {
//     $resultat = $n;
//     if( $n > 1 ) :
//         $resultat = fibonacci( $n - 1 ) + fibonacci( $n - 2 );
//     endif;
//
//     return $resultat;
// }

$n = readline( "Saisir un nombre entier positif : " );
echo 'Le résultat de Fibonacci pour ' . $n . ' est égale à : ' . fibonacci( $n );