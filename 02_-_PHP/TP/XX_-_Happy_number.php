<?php
/**
 * Checks a number is a Happy number or not
 * A happy number is defined by the following process: Starting with any positive integer, replace the number by the sum of the squares of its digits in base-ten, and repeat the process until the number either equals 1 (where it will stay), or it loops endlessly in a cycle that does not include 1. Those numbers for which this process ends in 1 are happy numbers, while those that do not end in 1 are unhappy numbers (or sad numbers).
 * 
 * Example :
 * n = 19
 * 1^2 + 9^2 = 82
 * 8^2 + 2^2 = 68
 * 6^2 + 8^2 = 100
 * 1^2 + 0^2 + 0^2 = 1
 * As we reached to 1, 19 is a Happy number
 * 
 * https://fr.wikipedia.org/wiki/Nombre_heureux
 */

/**
 * isHappyNumber Returns true if n is Happy number
 * (Loop way)
 * @param integer $n
 * @return boolean
 */
function isHappyNumber( int $n ) : bool {
    $storage = array();

    do {
        $sum = 0;

        while( $n > 0 ) {
            $sum += ( $n % 10 ) * ( $n % 10 ); // $sum = $sum + ( $n % 10 ) * ( $n % 10 ); // $sum += pow( ( $n % 10 ), 2 ); // $sum = $sum + pow( ( $n % 10 ), 2 ); // $sum += ( $n % 10 ) ** 2; // $sum = $sum + ( $n % 10 ) ** 2;
            $n /= 10; // $n = $n / 10;
        }
    
        if( in_array( $sum, $storage ) ) {
            return false;
        } else {
            $storage[] = $sum;
        }
        $n = $sum;
    } while( $sum !== 1 );

    return true;
}

/**
 * squareSum Utility method to return sum of square of digit of n
 * @param integer $n
 * @return integer
 */
function squareSum( int $n ) : int {
    $sum = 0;
    while( $n > 0 ) {
        $sum += ( $n % 10 ) * ( $n % 10 ); // $sum = $sum + ( $n % 10 ) * ( $n % 10 ); // $sum += pow( ( $n % 10 ), 2 ); // $sum = $sum + pow( ( $n % 10 ), 2 ); // $sum += ( $n % 10 ) ** 2; // $sum = $sum + ( $n % 10 ) ** 2;
        $n /= 10; // $n = $n / 10;
    }

    return $sum;
}

/**
 * isHappyNumberRec Returns true if n is Happy number
 * (Recursive way)
 * @param integer $n
 * @return boolean
 */
function isHappyNumberRec( int $n ) : bool {
    $slow = $n;
    $fast = $n;

    do {
        $slow = squareSum( $slow );
        $fast = squareSum( squareSum( $fast ) );
    } while( $slow != $fast );

    // if( $slow == 1 ) {
    //     return true
    // } else {
    //     return false;
    // }
    return ( $slow == 1 );
}

/**
 * happySequence Returns the sequence of the first n happy numbers
 *
 * @param integer $limit
 * @return array
 */
function happySequence( int $limit ) : array {
    $seq = [];
    $n = 0;
    do {
        $n++; // $n += 1; // $n = $n + 1;
        if( isHappyNumberRec( $n ) ) {
            $seq[] = $n;
            $limit--; // $limit -= 1; // $limit = $limit - 1;
        }
    } while( $limit > 0 );

    return $seq;
}


// Test
// $n = readline( 'Veuillez saisir un nombre pour savoir s\'il est heureux : ' );
// if( isHappyNumber( $n ) )
//     echo "$n est un nombre heureux :)\r\n";
// else
//     echo "$n n'est pas un nombre heureux :(\r\n";

// Séquence
$n = readline( 'Veuillez saisir un volume positif de nombres heureux à trouver : ' );
echo "Les $n premiers nombres :) sont : \r\n" . implode( ', ', happySequence( $n ) ) ;