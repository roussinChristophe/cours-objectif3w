<?php
/*
ENTRER a, b
RETOURNER ( a + b ) / 2

DÉBUT
    \\ Écrire le sous-algorithme de la fonction "moyenne" qui
    \\ renvoie la moyenne de deux entiers.
    \\ Écrire l'algorithme qui contient la déclaration de la
    \\ fonction moyenne et des instructions qui appellent cette
    \\ fonction.
    REQUÊTE "Saisir un nombre : ", nombre1
    REQUÊTE "Saisir un autre nombre : ", nombre2
    ÉCRIRE "La moyenne de", nombre1, "et", nombre2, "est :", moyenne( nombre1, nombre2 )
FIN
*/

/**
 * moyenne Calcule et renvoie la moyenne de deux nombres
 * @param float $a
 * @param float $b
 * @return float
 */
function moyenne( float $a, float $b ) : float {
    return ( $a + $b ) / 2;
}

$nombre1 = readline( "Saisir un nombre : " );
$nombre2 = readline( "Saisir un autre nombre : " );
echo 'La moyenne de ' . $nombre1 . ' et ' . $nombre2 . ' est : ' . moyenne( $nombre1, $nombre2 );