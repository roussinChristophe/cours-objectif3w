<?php
/*
Articles aléatoires | Les tableaux - Mise en pratique
Dans cet exercice, nous souhaitons mettre en place un affichage de 3 articles aléatoires parmi 5. Nous allons donc devoir :
    - créer une structure en PHP capable de stocker 5 articles, chaque article devant être composé :
        - d'un titre
        - d'un texte
        - d'une adresse vers une image
    - déterminer aléatoirement 3 articles à afficher parmi les 5 stockés
    - afficher chacun de ces articles selon une structure HTML différente :
        - titre, suivi de texte, suivi de image
        - titre, suivi de image, suivi de texte
        - image, suivi de titre, suivi de texte
*/

/**
 * On définit un tableau d'articles
 */
$_arr_articles = array(
    array( 'titre'=>'Titre 1', 'texte'=>'Texte 1', 'image'=>'assets/images/img.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 0) */
    array( 'titre'=>'Titre 2', 'texte'=>'Texte 2', 'image'=>'assets/images/objectif_3w_logo.png' ), /* Ligne représentant un article (automatiquement stocké à la clé 1) */
    array( 'titre'=>'Titre 3', 'texte'=>'Texte 3', 'image'=>'assets/images/picto_O3W.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 2) */
    array( 'titre'=>'Titre 4', 'texte'=>'Texte 4', 'image'=>'assets/images/region.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 3) */
    array( 'titre'=>'Titre 5', 'texte'=>'Texte 5', 'image'=>'assets/images/volutes.png' ) /* Ligne représentant un article (automatiquement stocké à la clé 4) */
);

shuffle( $_arr_articles ); // On tri de manière aléatoire les articles dans le tableau (http://php.net/manual/fr/function.shuffle.php)

/**
 * Comme les articles sont maintenant mélangés dans le tableau, les 3 premiers articles seront toujours différents
 * Nous pouvons donc afficher tout le temps les 3 premiers les uns après les autres
 */
echo "\r\nArticle à écrire à la place 1 : \r\n";
echo 'Titre : ' . $_arr_articles[0]['titre'] . "\r\n"; // On affiche le titre du premier article
echo 'Texte : ' . $_arr_articles[0]['texte'] . "\r\n"; // On affiche le texte du premier article
echo 'Image : ' . $_arr_articles[0]['image'] . "\r\n"; // On affiche l'image' du premier article

echo "\r\nArticle à écrire à la place 2 : \r\n";
echo 'Titre : ' . $_arr_articles[2]['image'] . "\r\n"; // On affiche l'image' du troisième article
echo 'Texte : ' . $_arr_articles[2]['titre'] . "\r\n"; // On affiche le titre du troisième article
echo 'Image : ' . $_arr_articles[2]['texte'] . "\r\n"; // On affiche le texte du troisième article

echo "\r\nArticle à écrire à la place 3 : \r\n";
echo 'Titre : ' . $_arr_articles[1]['titre'] . "\r\n"; // On affiche le titre du deuxième article
echo 'Texte : ' . $_arr_articles[1]['image'] . "\r\n"; // On affiche l'image' du deuxième article
echo 'Image : ' . $_arr_articles[1]['texte'] . "\r\n"; // On affiche le texte du deuxième article



echo "\r\n\r\n\r\nUn peu plus loin dans l'aléatoire : \r\n";
/**
 * Nous allons faire le même exercice mais cette fois-ci, l'ordre d'affichage des éléments HTML sera lui aussi aléatoire.
 */

/**
 * ashuffle - Mélange les clés d'un tableau et conserve l'association des index
 * @param   array $arr
 * @return  array
 */
function ashuffle( $arr ) {
    $_arr_random = array(); // On définit un tableau vide.

    $_arr_keys = array_keys( $arr ); // On affecte à la variable _arr_keys toutes les clés du tableau passé en paramètre. La variable _arr_keys devient donc un tableau. (http://php.net/manual/fr/function.array-keys.php).
    shuffle( $_arr_keys ); // On tri de manière aléatoire les valeurs dans le tableau (http://php.net/manual/fr/function.shuffle.php).

    foreach( $_arr_keys as $value ) : // Pour chaque ligne d'enregistrement dans le tableau _arr_keys,
        $_arr_random[$value] = $arr[$value]; // On recompose un tableau en utilisant les clés mélangées et en leur affectant les valeurs correspondantes dans le tableau d'origine.
    endforeach;

    return $_arr_random; // On retourne le tableau mélangé.
}

/**
 * On définit un tableau d'articles
 */
$_arr_articles = array(
    array( 'titre'=>'Titre 1', 'texte'=>'Texte 1', 'image'=>'assets/images/img.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 0) */
    array( 'titre'=>'Titre 2', 'texte'=>'Texte 2', 'image'=>'assets/images/objectif_3w_logo.png' ), /* Ligne représentant un article (automatiquement stocké à la clé 1) */
    array( 'titre'=>'Titre 3', 'texte'=>'Texte 3', 'image'=>'assets/images/picto_O3W.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 2) */
    array( 'titre'=>'Titre 4', 'texte'=>'Texte 4', 'image'=>'assets/images/region.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 3) */
    array( 'titre'=>'Titre 5', 'texte'=>'Texte 5', 'image'=>'assets/images/volutes.png' ) /* Ligne représentant un article (automatiquement stocké à la clé 4) */
);

$_int_limite = 3; // On définit une limite pour le nombre d'articles à afficher.

for( $i=0; $i<$_int_limite; $i++ ) : // On répète les opérations suivantes autant de fois que nous l'indique la variable limite
    echo "\r\nArticle à écrire à la place " . ( $i + 1 ) . " : \r\n";
    /**
     * On définit un nombre aléatoire parmi toutes les clés du tableau _arr_articles; soit un nombre compris entre 0 et le nombre d'articles stockés dans le tableau _arr_articles moins 1 car on commence à la clé 0.
     *      rand(0,count($_arr_articles)-1) retourne donc un nombre aléatoire entre 0 et 4
     * Cette valeur aléatoire nous permet donc d'accéder aléatoirement à un article dans le tableau _arr_articles car chacun d'eux possède une clé numérique.
     *      $_arr_articles[rand(0,count($_arr_articles)-1)] correspond donc à un article aléatoirement pris dans le tableau
     * On passe l'article aléatoire en paramètre de la fonction ashuffle.
     *      Si on imagine que la valeur aléatoire est 3 :
     *          La fonction ashuffle reçoit donc un tableau structuré de cette manière : array( 'titre'=>'Titre 3', 'texte'=>'Texte 3', 'image'=>'assets/images/picto_O3W.jpg' ).
     *          La fonction array_keys retourne donc le tableau suivant : array( 0=>'titre', 1=>'texte', 2=>'image' ).
     *          La fonction shuffle peut donc retourner un résultat tel que : array( 0=>'image', 1=>'titre', 2=>'texte' ). Nous pouvons utliser shuffle car là nous n'avons pas besoin de conserver l'association clé/valeur dans ce tableau.
     *          Pour chaque ligne de ce tableau trié aléatoirement,
     *              Nous utilisons le tableau tampon _arr_random qui était vide jusque là.
     *              Nous créons une clé dans ce tableau qui correspond à la valeur du tableau _arr_keys actuellement pointée par le foreach.
     *              Dans notre exemple, cette première valeur est donc 'image'.
     *              Nous venons donc de créer une clé 'image' dans le tableau _arr_random.
     *              Nous associons à cette clé la valeur contenue à la même clé dans le tableau d'origine passé en paramètre de la fonction ashuffle; soit le tableau contenant les données de l'article.
     *              Dans notre exemple, cette valeur est donc 'assets/images/picto_O3W.jpg'.
     *              Nous venons donc de créer une première ligne ayant l'association clé/valeur 'image'=>'assets/images/picto_O3W.jpg'.
     *              Puis nous faisons ensuite cela avec les autres lignes.
     *          La fonction ashuffle nous retourne donc dans ce cas le tableau array('image'=>'assets/images/picto_O3W.jpg', 'titre'=>'Titre 3', 'texte'=>'Texte 3').
     * On affecte à la variable _arr_article le retour de la fonction ashuffle
     */
    $_arr_article = ashuffle( $_arr_articles[rand(0,count($_arr_articles)-1)] );

    /**
     * Comme le tableau _arr_article a été mélangé par la fonction ashuffle, l'ordre des éléments composant l'article est aléatoire.
     * Nous ne pouvons donc pas savoir dans quel ordre afficher les balises html.
     * En revanche, nous pouvons parcourir le tableau représentant l'article et afficher la balise en fonction de la clé que nous rencontrons.
     */
    foreach( $_arr_article as $key => $value ) : // Pour chaque ligne du tableau _arr_article, on affecte à la variable key la clé en cours de lecture et à la variable value la valeur en cours de lecture,
        switch( $key ) : // En fonction de la clé en cours,
            case 'titre': // Si cette clé est 'titre',
                echo "Titre : $value\r\n"; // On affiche la balise h2 avec la valeur en cours (soit la valeur contenue à la clé 'titre' dans le tableau _arr_article : $_arr_article['titre']).
                break;
            case 'texte': // Si cette clé est 'texte',
                echo "Texte : $value\r\n"; // On affiche la balise p avec la valeur en cours (soit la valeur contenue à la clé 'texte' dans le tableau _arr_article : $_arr_article['texte']).
                break;
            case 'image': // Si cette clé est 'image',
                echo "Image : $value\r\n"; // On affiche la balise img avec la valeur en cours (soit la valeur contenue à la clé 'image' dans le tableau _arr_article : $_arr_article['image']).
                break;
        endswitch;
    endforeach;
endfor;



echo "\r\n\r\n\r\nDe l'aléatoire pas si aléatoire que ça : \r\n";
/**
 * Nous allons faire le même exercice mais cette fois-ci, nous ne voulons pas que les articles puissent apparaître autrement que selon les formats prévus.
 */

/**
 * On définit un tableau d'articles
 */
$_arr_articles = array(
    array( 'titre'=>'Titre 1', 'texte'=>'Texte 1', 'image'=>'assets/images/img.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 0) */
    array( 'titre'=>'Titre 2', 'texte'=>'Texte 2', 'image'=>'assets/images/objectif_3w_logo.png' ), /* Ligne représentant un article (automatiquement stocké à la clé 1) */
    array( 'titre'=>'Titre 3', 'texte'=>'Texte 3', 'image'=>'assets/images/picto_O3W.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 2) */
    array( 'titre'=>'Titre 4', 'texte'=>'Texte 4', 'image'=>'assets/images/region.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 3) */
    array( 'titre'=>'Titre 5', 'texte'=>'Texte 5', 'image'=>'assets/images/volutes.png' ) /* Ligne représentant un article (automatiquement stocké à la clé 4) */
);
/**
 * On définit un tableau d'organisation
 */
$_arr_orgas = array(
    array( 'titre', 'texte', 'image' ),
    array( 'image', 'titre', 'texte' ),
    array( 'titre', 'image', 'texte' ),
);
shuffle( $_arr_orgas );

$_int_limite = 3; // On définit une limite pour le nombre d'articles à afficher.

for( $i=0; $i<$_int_limite; $i++ ) : // On répète les opérations suivantes autant de fois que nous l'indique la variable limite
    echo "\r\nArticle à écrire à la place " . ( $i + 1 ) . " : \r\n";
    $_int_rand = rand(0,count($_arr_articles)-1);

    for( $j=0; $j<$_int_limite; $j++ ) : // On répète les opérations suivantes autant de fois que nous l'indique la variable limite
        echo $_arr_orgas[$i][$j] . ' : ' . $_arr_articles[$_int_rand][$_arr_orgas[$i][$j]] . "\r\n";
    endfor;
endfor;



echo "\r\n\r\n\r\nDe l'aléatoire ayant de la mémoire : \r\n";
/**
 * Nous allons faire le même exercice mais cette fois-ci, nous ne voulons pas que le même article apparaisse plusieurs fois.
 */

/**
 * On définit un tableau d'articles
 */
$_arr_articles = array(
    array( 'titre'=>'Titre 1', 'texte'=>'Texte 1', 'image'=>'assets/images/img.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 0) */
    array( 'titre'=>'Titre 2', 'texte'=>'Texte 2', 'image'=>'assets/images/objectif_3w_logo.png' ), /* Ligne représentant un article (automatiquement stocké à la clé 1) */
    array( 'titre'=>'Titre 3', 'texte'=>'Texte 3', 'image'=>'assets/images/picto_O3W.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 2) */
    array( 'titre'=>'Titre 4', 'texte'=>'Texte 4', 'image'=>'assets/images/region.jpg' ), /* Ligne représentant un article (automatiquement stocké à la clé 3) */
    array( 'titre'=>'Titre 5', 'texte'=>'Texte 5', 'image'=>'assets/images/volutes.png' ) /* Ligne représentant un article (automatiquement stocké à la clé 4) */
);
/**
 * On définit un tableau d'organisation
 */
$_arr_orgas = array(
    array( 'titre', 'texte', 'image' ),
    array( 'image', 'titre', 'texte' ),
    array( 'titre', 'image', 'texte' ),
);
shuffle( $_arr_orgas );

$_int_limite = 3; // On définit une limite pour le nombre d'articles à afficher.
$_arr_log = array(); // On crée un tableau vide qui nous servira à stocker les articles déjà affichés.

for( $i=0; $i<$_int_limite; $i++ ) : // On répète les opérations suivantes autant de fois que nous l'indique la variable limite
    echo "\r\nArticle à écrire à la place " . ( $i + 1 ) . " : \r\n";
    do {
        /**
         * On définit un nombre aléatoire parmi toutes les clés du tableau _arr_articles; soit un nombre compris entre 0 et le nombre d'articles stockés dans le tableau _arr_articles moins 1 car on commence à la clé 0.
         *      rand(0,count($_arr_articles)-1) retourne donc un nombre aléatoire entre 0 et 4
         */
        $_int_rand = rand(0,count($_arr_articles)-1);
    } while( in_array( $_int_rand, $_arr_log ) ); // On répète le bloc d'instructions tant que la clé choisie est présente dans le tableau des articles déjà affichés (http://php.net/manual/fr/function.in-array.php).

    for( $j=0; $j<$_int_limite; $j++ ) : // On répète les opérations suivantes autant de fois que nous l'indique la variable limite
        echo $_arr_orgas[$i][$j] . ' : ' . $_arr_articles[$_int_rand][$_arr_orgas[$i][$j]] . "\r\n";
    endfor;

    $_arr_log[] = $_int_rand; // On stocke la clé dans le tableau des articles déjà affichés. Cela nous permettra à la prochaine boucle de ne pas ressortir une clé déjà utilisée.
endfor;