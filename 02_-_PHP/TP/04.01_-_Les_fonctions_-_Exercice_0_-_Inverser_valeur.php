<?php
/*
ENTRER RÉFÉRENCE a, RÉFÉRENCE b
    tmp = a
    a = b
    b = tmp
RETOURNER 

DÉBUT
    \\ Ecrire un algorithme qui échange la valeur de deux
    \\ variables.
    \\ Exemple, si a vaut 2 et b vaut 5, le programme donnera a vaut 5 et b vaut 2.
    REQUÊTE "Saisir un premier entier : ", a
    REQUÊTE "Saisir un deuxième entier : ", b
    ÉCRIRE "A vaut actuellement", a, "et B vaut actuellement", b
    inverserValeurs( a, b )
    ÉCRIRE "A vaut actuellement", a, "et B vaut actuellement", b
FIN
*/

/**
 * inverserValeurs Inverse deux valeurs
 * @param I/O mixed $a
 * @param I/O mixed $b
 * @return void
 */
function inverserValeurs( &$a, &$b ) {
    echo 'invers : A vaut actuellement ' . $a . ' et B vaut actuellement ' . $b . "\r\n";
    $tmp = $a;
    $a = $b;
    $b = $tmp;
    echo 'invers : A vaut actuellement ' . $a . ' et B vaut actuellement ' . $b . "\r\n";
}

$a = readline( "Saisir un premier entier : " );
$b = readline( "Saisir un deuxième entier : " );
echo 'A vaut actuellement ' . $a . ' et B vaut actuellement ' . $b . "\r\n";
inverserValeurs( $a, $b );
echo 'A vaut actuellement ' . $a . ' et B vaut actuellement ' . $b . "\r\n";