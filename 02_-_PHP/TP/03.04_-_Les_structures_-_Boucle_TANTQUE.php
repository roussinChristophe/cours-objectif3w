<?php
/*
DÉBUT
    \\ Reproduire l'algorithme de la boucle POUR avec
    \\ l'instruction TANT QUE … FAIRE.
    \\ L'expression logique d'arrêt sera la saisie de la valeur
    \\ "-1".
    cumul = 0
    iteration = 0
    REQUÊTE "Saisissez une valeur (-1 termine la saisie) : ", val
    TANTQUE val != -1 FAIRE
        iteration = iteration + 1
        cumul = cumul + val
        REQUÊTE "Saisissez une valeur (-1 termine la saisie) : ", val
    FINTANTQUE
    ÉCRIRE "Le total des", iteration, "valeurs saisies est :", cumul
FIN
*/
$cumul = 0;
$iteration = 0;
$val = readline( "Saisissez une valeur (-1 termine la saisie) : " );
while( $val != -1 ) {
    $iteration++; // $iteration = $iteration + 1
    $cumul += $val; // $cumul = $cumul + $val;
    $val = readline( "Saisissez une valeur (-1 termine la saisie) : " );
}
echo "Le total des $iteration valeurs saisies est : $cumul";