<?php
/*
ALGORITHME saisirNote( E/S t : TABLEAU[1...n] DE RÉELS ) : ENTIER
    DÉBUT
        \\ saisirNote - Vérifie une saisie utilisateur pour compléter un tableau avec des notes et retourne le nombre de notes ajoutées
        \\ 
        \\ ALGORITHME saisirNote( E/S t : TABLEAU[1...n] DE RÉELS ) : ENTIER

        \\ On définit la valeur d'arrêt pour la saisie
        STOP = -1

        \\ On initialise le nombre d'entrées pour le tableau
        indice = 0
        RÉPÉTER
            \\ On demande une note
            REQUÊTE "Veuillez saisir une note (-1 pour arrêter) : ", note

            \\ Tant que la note n'est pas comprise entre 0 et 20,
            TANTQUE note != STOP ET ( note < 0 OU note > 20 ) FAIRE

                \\ On demande une autre saisie
                REQUÊTE "La note doit être comprise entre 0 et 20 !\nNouvelle saisie : ", note
            FINTANTQUE

            \\ Si la saisie est différente de la valeur d'arrêt,
            SI note != STOP ALORS

                \\ On incrémente le nombre d'entrées pour le tableau
                indice = indice + 1

                \\ On ajoute la note au tableau
                t[indice] = note
            FINSI

            \\ On répète les opérations tant que la saisie n'est pas -1 (ou jusqu'à ce que la saisie soit égale à -1)
        JUSQU'À note = STOP

        \\ On retourne le nombre d'entrées du tableau
        RETOURNER indice
    FIN

ALGORITHME afficherNote( E t : TABLEAU[1...n] DE RÉELS, E nb : ENTIER ) : VOID
    DÉBUT
        \\ afficherNote - Parcours un tableau pour en afficher le contenu
        \\ 
        \\ ALGORITHME afficherNote( E t : TABLEAU[1...n] DE RÉELS, E nb : ENTIER )

        \\ Pour chaque élément du tableau,
        POUR i = 1 JUSQU'À nb INCRÉMENT 1 FAIRE

            \\ On l'affiche
            ÉCRIRE t[i]
        FINPOUR
    FIN 

ALGORITHME indiceMinimum( E t : TABLEAU[1...n] DE RÉELS, E nb : ENTIER, E rang : ENTIER ) : ENTIER
    DÉBUT
        \\ indiceMinimum - Parcours un tableau de nombres pour en trouver le plus petit et retourner son emplacement dans le tableau
        \\ 
        \\ ALGORITHME indiceMinimum( E t : TABLEAU[1...n] DE RÉELS, E nb : ENTIER, E rang : ENTIER ) : ENTIER

        \\ On part du principe que par défaut, le plus petit élément se trouve au premier emplacement observé
        indice = rang

        \\ Pour chaque élément du tableau entre celui qui succède à un élément de référence passé en paramètre et le dernier du tableau,
        POUR i = rang + 1 JUSQU'À nb INCRÉMENT 1 FAIRE

            \\ Si l'élément sur lequel on se trouve est plus petit que celui se trouvant à l'indice de référence,
            SI t[i] < t[indice] ALORS

                \\ On considère que la nouvelle référence du plus petit est l'élément observé
                indice = i
            FINSI
        FINPOUR

        \\ On retourne l'indice du plus petit élément du tableau
        RETOURNER indice
    FIN

ALGORITHME inverserValeur( E a : RÉEL, E b : RÉEL ) : VOID
    DÉBUT
        \\ inverserValeur - Inverse deux valeurs
        \\ 
        \\ ALGORITHME inverserValeur( E a : RÉEL, E b : RÉEL )
        tmp = a
        a = b
        b = tmp
    FIN

ALGORITHME triMinimum( E/S t : TABLEAU[1...n] DE RÉELS, E nb : ENTIER ) : VOID
    DÉBUT
        \\ triMinimum - Ordonne par ordre croissant un tableau de nombres
        \\ 
        \\ ALGORITHME triMinimum( E/S t : TABLEAU[1...n] DE RÉELS, E nb : ENTIER ) : VOID

        \\ Pour chaque élément du tableau jusqu'à l'avant dernier,
        POUR i = 1 JUSQU'À nb - 1 INCRÉMENT 1 FAIRE

            \\ Si un élément plus petit est trouvé dans le tableau à un
            \\ indice différent de celui sur lequel on se trouve,
            SI indiceMinimum( t, nb, i ) != i ALORS

                \\ On inverse l'élément sur lequel on se trouve avec le plus
                \\ petit trouvé
                inverserValeur( t[i], t[indiceMinimum( t, nb, i )] )
            FINSI
        FINPOUR
    FIN

ALGORITHME moyenne( E t : TABLEAU[1...n] DE RÉELS, E nb : ENTIER ) : RÉEL
    DÉBUT
        \\ moyenne - Calcule la moyenne d'un tableau de nombres
        \\ 
        \\ ALGORITHME moyenne( E t : TABLEAU[1...n] DE RÉELS, E nb : ENTIER ) : RÉEL

        \\ On initialise la somme des notes pour le calcul de la moyenne
        somme = 0.0

        \\ Pour chaque élément du tableau,
        POUR i = 1 JUSQU'À nb INCRÉMENT 1 FAIRE

            \\ On cumule les valeurs
            somme = somme + t[i]
        FINPOUR

        \\ On retourne la moyenne des valeurs
        RETOURNER somme / nb
    FIN

DÉBUT
    \\ Écrire un programme qui affiche en ordre croissant les notes d'une promotion de 10 élèves, suivies de la note la plus faible, de la note la plus élevée et de la moyenne.

    \\ On initialise un tableau vide
    notes = []

    \\ On saisit les notes
    nbNote = saisirNote( notes )

    \\ On affiche les notes non triées
    ÉCRIRE "Voici le tableau non trié :"
    afficherNote( notes, nbNote )

    \\ On trie le tableau
    triBulles( notes, nbNote )

    \\ On affiche les notes après le tri
    ÉCRIRE "Voici le tableau trié :"
    afficherNote( notes, nbNote )

    \\ On affiche la note la plus faible ... donc puisque le tableau est trié, celle qui se trouve au début du tableau
    ÉCRIRE "Note la plus faible :", notes[1]

    \\ On affiche la note la plus élevée ... donc puisque le tableau est trié, celle qui se trouve à la fin du tableau
    ÉCRIRE "Note la plus élevée :", notes[nbNote]
    ÉCRIRE "Moyenne :", moyenne( notes, nbNote )
FIN
*/

/**
 * saisirNote - Vérifie une saisie utilisateur pour compléter un tableau avec des notes et retourne le nombre de notes ajoutées
 * @param I/O array $t Tableau de nombres
 * @return int Nombre de nombres saisis
 */
function saisirNote( array &$t ) : int {
    // On définit la valeur d'arrêt pour la saisie
    define( 'STOP', -1 );

    // On initialise le nombre d'entrées pour le tableau
    $indice = 0;
    do {
        // On demande une note
        $note = readline( 'Veuillez saisir une note (-1 pour arrêter) : ' );

        // Tant que la note n'est pas comprise entre 0 et 20,
        while( $note != STOP && ( $note < 0 || $note > 20 ) ) {
            // On demande une autre saisie
            $note = readline( 'La note doit être comprise entre 0 et 20 !\r\nNouvelle saisie : ' );
        }

        // Si la saisie est différente de la valeur d'arrêt,
        if( $note != STOP ) {
            // On incrémente le nombre d'entrées pour le tableau
            $indice++; // $indice += 1; // $indice = $indice + 1;

            // On ajoute la note au tableau
            $t[$indice] = $note;
        }

        // On répète les opérations tant que la saisie n'est pas -1 (ou jusqu'à ce que la saisie soit égale à -1)
    } while( $note != STOP );

    // On retourne le nombre d'entrées du tableau
    return $indice;
}

/**
 * afficherNote - Parcours un tableau pour en afficher le contenu
 * @param array $t Tableau de nombres
 * @param int $nb Compteur de nombres dans le tableau
 */
function afficherNote( array $t, int $nb ) {
    // Pour chaque élément du tableau,
    for( $i = 1; $i <= $nb; $i++ ) {
        // On l'affiche
        echo $t[$i] . "\r\n";
    }
}

/**
 * inverserValeur - Inverse deux valeurs
 * @param I/O mixed $a
 * @param I/O mixed $b
 * @return void
 */
function inverserValeur( &$a, &$b ) {
    $tmp = $a;
    $a = $b;
    $b = $tmp;
}

/**
 * indiceMinimum - Parcours un tableau de nombres pour en trouver le plus petit et retourner son emplacement dans le tableau
 * @param array $t
 * @param int $nb
 * @param int $rang
 * @return int
 */
function indiceMinimum( array $t, int $nb, int $rang ) : int {
    // On part du principe que par défaut, le plus petit élément se trouve au premier emplacement observé
    $indice = $rang;

    // Pour chaque élément du tableau entre celui qui succède à un élément de référence passé en paramètre et le dernier du tableau,
    for( $i = $rang + 1; $i <= $nb; $i++ ) {
        // Si l'élément sur lequel on se trouve est plus petit que celui se trouvant à l'indice de référence,
        if( $t[$i] < $t[$indice] ) {
            // On considère que la nouvelle référence du plus petit est l'élément observé
            $indice = $i;
        }
    }

    // On retourne l'indice du plus petit élément du tableau
    return $indice;
}

/**
 * triMinimum - Ordonne par ordre croissant un tableau de nombres
 * @param I/O array $t
 * @param int $nb
 * @return void
 */
function triMinimum( array &$t, int $nb ) {
    // Pour chaque élément du tableau jusqu'à l'avant dernier,
    for( $i = 1; $i < $nb; $i++ ) {
        // Si un élément plus petit est trouvé dans le tableau à un indice différent de celui sur lequel on se trouve,
        /*
        $indice = indiceMinimum( $t, $nb, $i );
        if( $indice != $i ) {
        */
        if( ( $indice = indiceMinimum( $t, $nb, $i ) ) != $i ) {
            // On inverse l'élément sur lequel on se trouve avec le plus petit trouvé
            inverserValeur( $t[$i], $t[$indice] );
        }
    }
}

/**
 * moyenne - Calcule et renvoie la moyenne d'un tableau de nombres
 * @param array $t
 * @param int $nb
 * @return float
 */
function moyenne( array $t, int $nb ) : float {
    // On initialise la somme des notes pour le calcul de la moyenne
    $somme = 0.0;

    // Pour chaque élément du tableau,
    for( $i = 1; $i <= $nb; $i++ ) {
        // On cumule les valeurs
        $somme += $t[$i]; // $somme = $somme + $t[$i];
    }

    // On retourne la moyenne des valeurs
    return $somme / $nb;
}

/*
Écrire un programme qui affiche en ordre croissant les notes d'une promotion de 10 élèves,
suivies de la note la plus faible, de la note la plus élevée
et de la moyenne.
*/

// On initialise un tableau vide
$notes = [];

// On saisit les notes
$nbNote = saisirNote( $notes );

// On affiche les notes non triées
echo 'Voici le tableau non trié :' . "\r\n";
afficherNote( $notes, $nbNote );

// On trie le tableau
triMinimum( $notes, $nbNote );

// On affiche les notes après le tri
echo 'Voici le tableau trié :' . "\r\n";
afficherNote( $notes, $nbNote );

// On affiche la note la plus faible ... donc puisque le tableau est trié, celle qui se trouve au début du tableau
echo 'Note la plus faible : ' . $notes[1] . "\r\n";
// On affiche la note la plus élevée ... donc puisque le tableau est trié, celle qui se trouve à la fin du tableau
echo 'Note la plus élevée : ' . $notes[$nbNote] . "\r\n";

echo 'Moyenne : ' . moyenne( $notes, $nbNote );