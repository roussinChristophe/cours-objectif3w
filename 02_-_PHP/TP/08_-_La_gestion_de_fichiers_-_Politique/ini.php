<?php
if( !defined( 'PHP_EOL') ) : // Si la constante PHP_EOL n'existe pas (http://php.net/manual/fr/reserved.constants.php),
    if( strtoupper( substr( PHP_OS, 0, 3 ) ) == 'WIN' ) : // Si la version du système d'exploitation (fournie par la constantes pré-définie PHP_OS) correspond à un noyau Windows,
        define( 'PHP_EOL', "\r\n" ); // On définit la constante avec les caractères Windows.
    else : // Sinon,
        define( 'PHP_EOL', "\n" ); // On définit la constante avec les caractères UNIX.
    endif;
endif;
if( !defined( 'DIRECTORY_SEPARATOR') ) : // Si la constante DIRECTORY_SEPARATOR n'existe pas (http://php.net/manual/fr/reserved.constants.php),
    if( strtoupper( substr( PHP_OS, 0, 3 ) ) == 'WIN' ) : // Si la version du système d'exploitation (fournie par la constantes pré-définie PHP_OS) correspond à un noyau Windows,
        define( 'DS', '\\' ); // On définit la constante avec les caractères Windows.
    else : // Sinon,
        define( 'DS', '/' ); // On définit la constante avec les caractères Windows.
    endif;
else :
    define( 'DS', DIRECTORY_SEPARATOR );
endif;

define( 'DIRNAME', dirname( realpath( basename( $_SERVER['PHP_SELF'] ) ) ) . DS );  // On définit le chemin physique du fichier pour pouvoir le réexploiter plus tard sans avoir à la saisir à nouveau. Ce chemin est basé sur celui du script que nous sommes en train d'éxecuter (http://php.net/manual/fr/function.realpath.php).
define( 'FILENAME', 'data.txt' ); // On définit le nom du fichier pour pouvoir le réexploiter plus tard sans avoir à la saisir à nouveau.
define( 'NBSENTENCES', 8 ); // On définit le nombre de phrases.