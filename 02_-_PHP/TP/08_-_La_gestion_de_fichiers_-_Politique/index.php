<?php
/*
Nous allons écrire un discours automatique de politicien.
Notre programme doit sélectionner plusieurs morceaux de phrase aléatoirement et les compose pour former 5 phrases.
Les différents morceaux de phrase utilisables sont regroupés dans le fichier "data.txt". Chaque morceau est sur une ligne. Chaque groupe est séparé par une ligne vide.
Les phrases seront créées à partir d'un morceau du premier groupe, puis un morceau du second groupe, puis un du troisième, et enfin un du quatrième ... le tout aléatoirement (cas spécial : la première phrase commencera toujours par le premier morceau du premier groupe).
Un bon politicien ne se répète pas. Aussi, le programme ne pourra pas utiliser le même morceau de phrase deux fois.
Votre programme ne doit pas non plus modifier le fichier texte et doit prendre en considération le fait que l'on puisse ajouter d'autres morceaux de phrases.
*/
require( 'ini.php' );
require( 'functions.php' );

echo "Mon discours :\r\n--------------------\r\n";
if( ( $sentences = getSentences( FILENAME, DIRNAME ) )!==false && isset( $sentences[0] ) )
    echo makeSpeech( $sentences, ( NBSENTENCES<=count( $sentences[0] ) ? NBSENTENCES : count( $sentences[0] ) ) );