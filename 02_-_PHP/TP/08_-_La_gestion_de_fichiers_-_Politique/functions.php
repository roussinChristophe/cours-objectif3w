<?php
/**
 * getSentences Récupère le contenu d'un fichier pour le stocker dans un tableau
 * @param   string  $file
 * @param   string  $dir
 * @return  mixed (array/bool)
**/
function getSentences( string $file, string $dir ) {
    if( file_exists( $dir . $file ) ) : // Si le fichier existe (http://php.net/manual/en/function.file-exists.php),
        if( is_readable( $dir . $file ) ) : // Si le fichier est accessible avec des droits de lecture (http://php.net/manual/en/function.is-readable.php),
            if( ( $_resrc_file = fopen( $dir . $file, 'r' ) )!==false ) : // On ouvre le fichier en mode lecture seule et récupère la ressource.
                $_arr_strings = array( array() );
                while( !feof( $_resrc_file ) ) : // Tant que l'on n'a pas atteind la fin du fichier,
                    if( ( $_str_line = fgets( $_resrc_file ) )==PHP_EOL ) : // S'il n'y a pas eu d'erreur et si cette ligne estune ligne de séparation,
                        $_arr_strings[] = array(); // On crée une nouvelle entrée dans le tableau.
                    endif;

                    $_arr_strings[count( $_arr_strings )-1][] = str_replace( PHP_EOL, ' ', $_str_line ); // On stocke la ligne en supprimant au passage le caractère de fin de ligne(http://php.net/manual/en/function.str-replace.php).
                endwhile;

                return $_arr_strings;
            else : // Sinon,
                echo "/!\\ ERREUR : Le fichier \"$file\" ne peut pas être ouvert." . PHP_EOL; // On affiche un message d'erreur indiquant une erreur d'ouverture.
            endif;
        else : // Sinon,
            echo "/!\\ ERREUR : Vous ne possédez pas les droits de lecture sur le fichier \"$file\"." . PHP_EOL; // On affiche un message d'erreur indiquant une erreur d'autorisation.
        endif;
    else : // Sinon,
        echo "/!\\ ERREUR : Le fichier \"$file\" n'existe pas dans le dossier \"$dir\"." . PHP_EOL; // On affiche un message d'erreur indiquant que le fichier n'existe pas.
    endif;

    return false;
}

/**
 * makeSpeech Compose un discours aléatoirement
 * @param   array   $strings
 * @param   int     $iteration
 * @return  string
**/
function makeSpeech( array $strings, int $iteration ) : string {
    $speech = '';

    if( isset( $strings[0][0] ) ) : // Si la première clé existe,
        $speech .= $strings[0][0]; // On l'affiche.
        unset( $strings[0][0] ); // On supprime l'entrée dans le tableau pour ne plus pouvoir l'utiliser.
    endif;

    for( $i = 0; $i<$iteration; $i++ ) : // Pour le nombre de phrases défini,
        foreach( $strings as $keyGroup => $group ) : // Pour chaque groupe de morceaux de phrase,
            if( !( $i===0 && $keyGroup===0 ) ) : // Si on ne débute pas le discours (nous avons déjà affiché "Mesdames, messieurs" donc nous sautons le premier groupe),
                $_keyRand = array_rand( $group ); // On sélectionne un morceau aléatoirement.
                $speech .= $group[$_keyRand]; // On affiche ce morceau de phrase.
                unset( $strings[$keyGroup][$_keyRand] ); // On supprime l'entrée dans le tableau pour ne plus pouvoir l'utiliser.
            endif;
        endforeach;
        $speech .= PHP_EOL . PHP_EOL;
    endfor;

    return $speech;
}