<?php
/**
 * manage_ds Manage file separators at the end of the line
 * @param string $entry
 * @return string
 */
function manage_ds( string $entry ) : string {
    return $entry . ( substr( $entry, -1 )==DS ? '' : DS ); // http://php.net/manual/en/function.substr.php
}

/**
 * is_indexed Checks if a directory is indexed and returns the name of the entry if it is, false if not
 * @param string $dir
 * @return mixed (string | boolean)
 */
function is_indexed( string $dir ) {
    $keyword = 'index';
    foreach( scandir( $dir ) as $entry ) : // http://php.net/manual/en/function.scandir.php
        // if( is_file( manage_ds( $dir ) . $entry ) && strtolower( substr( $entry, 0, strlen( $keyword ) ) )==$keyword ) : // http://php.net/manual/en/function.is-file.php // http://php.net/manual/en/function.substr.php // http://php.net/manual/en/function.strlen.php
        if( is_file( manage_ds( $dir ) . $entry ) && strtolower( pathinfo( manage_ds( $dir ) . $entry )['filename'] )==$keyword ) : // http://php.net/manual/en/function.pathinfo.php
            return $entry;
        endif;
    endforeach;

    return false;
}

/**
 * tabulation Manage indentation
 * @param integer $iteration
 * @return string
 */
function tabulation( int $iteration ) : string {
    $blank = '';
    for( $i = 0; $i<$iteration; $i++ ) :
        $blank .= '    ';
    endfor;

    return $blank;
}

/**
 * filetree Browse a directory and returns a file tree
 * @param string $dir
 * @param [integer $iteration]
 * @return string
 */
function filetree( string $dir, int $iteration = 0 ) : string {
    $res = '';
    $indent = tabulation( $iteration );
    
    if( is_dir( $dir ) ) : // http://php.net/manual/en/function.is-dir.php
        $res .= PHP_EOL . $indent . '- ' . basename( $dir ); // http://php.net/manual/en/function.basename.php

        if( ( $index = is_indexed( $dir ) )!==false ) :
            $res .= PHP_EOL . $indent . '    (Indexation protégée)';
        else :
            foreach( scandir( $dir ) as $entry ) : // http://php.net/manual/en/function.scandir.php
                if( is_dir( manage_ds( $dir ) . manage_ds( $entry ) ) && $entry!='.' && $entry!='..' ) :
                    $res .= filetree( manage_ds( $dir ) . manage_ds( $entry ), $iteration+1 );
                elseif( is_file( manage_ds( $dir ) . $entry ) ) : // http://php.net/manual/en/function.is-file.php
                    $res .= PHP_EOL . $indent . '    - ' . basename( $entry );
                endif;
            endforeach;
        endif;
    else : die( "$dir n'est pas un répertoire" );
    endif;

    return $res;
}