<?php
/*
DÉBUT
    \\ Saisir une valeur entiere et afficher :
    \\  - "Recu avec mention Assez bien" si une note est
    \\ superieure ou egale a 12,
    \\  - "Recu avec mention Passable" si une note est superieure
    \\ a 10 et inferieure a 12,
    \\  - "Insuffisant" dans tous les autres cas.
    REQUÊTE "Veuillez saisir une note : ", note
    SI note >= 12 ALORS
        ÉCRIRE "Recu avec mention Assez bien"
    SINON
        SI note >= 10 ET note < 12 ALORS
            ÉCRIRE "Recu avec mention Passable"
        SINON
            ÉCRIRE "Insuffisant"
        FINSI
    FINSI
FIN
*/
$note = readline( "Veuillez saisir une note : " );
if( $note >= 12 ) {
    echo "Recu avec mention Assez bien";
} elseif( $note >= 10 && $note < 12 ) {
    echo "Recu avec mention Passable";
} else {
    echo "Insuffisant";
}