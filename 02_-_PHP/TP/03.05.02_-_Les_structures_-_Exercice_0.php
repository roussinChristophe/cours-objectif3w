<?php
/*
DÉBUT
    \\ Écrire un algorithme qui échange la valeur de deux
    \\ variables.
    \\ Exemple, si a vaut 2 et b vaut 5, le programme donnera a
    \\ vaut 5 et b vaut 2.
    REQUÊTE "Saisir une première valeur : ", a
    REQUÊTE "Saisir une deuxième valeur : ", b
    ÉCRIRE "A vaut actuellement", a, "et B vaut actuellement", b
    a = a + b
    b = a - b
    a = a - b
    ÉCRIRE "A vaut actuellement", a, "et B vaut actuellement", b
FIN
*/
$a = readline( "Saisir une première valeur : " );
$b = readline( "Saisir une deuxième valeur : " );
echo "A vaut actuellement $a et B vaut actuellement $b\r\n";
$a = $a + $b;
$b = $a - $b;
$a = $a - $b;
echo "A vaut actuellement $a et B vaut actuellement $b\r\n";

/*
DÉBUT
    \\ Écrire un algorithme qui échange la valeur de deux
    \\ variables.
    \\ Exemple, si a vaut 2 et b vaut 5, le programme donnera a
    \\ vaut 5 et b vaut 2.
    REQUÊTE "Saisir une première valeur : ", a
    REQUÊTE "Saisir une deuxième valeur : ", b
    ÉCRIRE "A vaut actuellement", a, "et B vaut actuellement", b
    c = a
    a = b
    b = c
    ÉCRIRE "A vaut actuellement", a, "et B vaut actuellement", b
FIN
*/
$a = readline( "Saisir une première valeur : " );
$b = readline( "Saisir une deuxième valeur : " );
echo "A vaut actuellement $a et B vaut actuellement $b\r\n";
$c = $a;
$a = $b;
$b = $c;
echo "A vaut actuellement $a et B vaut actuellement $b\r\n";