<?php
/*
ALGORITHME inverserValeur( E a : RÉEL, E b : RÉEL ) : VOID
    DÉBUT
        \\ inverserValeur - Inverse deux valeurs
        \\ 
        \\ ALGORITHME inverserValeur( E a : RÉEL, E b : RÉEL )
        tmp = a
        a = b
        b = tmp
    FIN

ALGORITHME triRapide( E/S t : TABLEAU[1...n] DE RÉELS, E premier : ENTIER, E dernier : ENTIER, E pivot : ENTIER ) : VOID
    DÉBUT
        \\ triRapide - Ordonne par ordre croissant un tableau de nombres
        \\ 
        \\ ALGORITHME triRapide( E/S t : TABLEAU[1...n] DE RÉELS, E premier : ENTIER, E dernier : ENTIER, E pivot : ENTIER ) : VOID

        \\ On définit un marqueur sur la gauche du tableau
        gauche = premier - 1

        \\ On définit un marqueur sur la droite du tableau
        droite = dernier + 1

        \\ Si le pivot se trouve entre les bornes du tableau,
        SI pivot >= premier && pivot <= dernier ALORS
            \\ On inverse le pivot avec l'élément contenu dans le dernier emplacement du tableau
            inverserValeur( t[pivot], t[dernier] )

            \\ Tant que le marqueur de gauche n'a pas dépassé celui de droite,
            TANTQUE gauche < droite FAIRE
                RÉPÉTER
                    \\ On fait avancer le marqueur de gauche
                    gauche = gauche + 1
                \\ On répète tant que la valeur contenue dans le tableau à l'emplacement du marqueur gauche est strictement inférieure à la valeur contenue dans le dernier élément du tableau (notre pivot)
                JUSQU'À t[gauche] >= t[dernier]

                \\ Si le marqueur de gauche n'a pas dépassé celui de droite,
                SI gauche < droite ALORS
                    RÉPÉTER
                        \\ On fait avancer le marqueur de droite
                        droite = droite - 1
                    \\ On répète tant que la valeur contenue dans le tableau à l'emplacement du marqueur droite est strictement supérieure à la valeur contenue dans le dernier élément du tableau (notre pivot)
                    JUSQU'À t[droite] <= t[dernier]

                    \\ Si l'élément contenu dans le tableau à l'endroit où le marqueur de gauche se situe est supérieur à l'élément contenu dans le tableau à l'endroit où le marqueur de droite se situe,
                    SI t[gauche] > t[droite] ALORS
                        \\ On inverse l'élément contenu dans le tableau à l'endroit où le marqueur de gauche se situe avec l'élément contenu dans le tableau à l'endroit où le marqueur de droite se situe
                        inverserValeur( t[gauche], t[droite] )
                    FINSI
                SINON
                    \\ On inverse l'élément contenu dans le dernier emplacement du tableau (le pivot) avec l'élément contenu dans la tableau à l'endroit où le marqueur de gauche et le marqueur de droite se sont croisés
                    inverserValeur( t[gauche], t[dernier] )
                FINSI
            FINTANTQUE

            \\ On rappelle l'algorithme de "tri rapide" sur le sous tableau se trouvant à gauche du marqueur gauche
            triRapide( t, premier, gauche-1, gauche-1 )

            \\ On rappelle l'algorithme de "tri rapide" sur le sous tableau se trouvant à droite du marqueur droite
            triRapide( t, droite+1, dernier, dernier )
        FINSI
    FIN

DÉBUT
    \\ Reproduire l'algorithme de "tri rapide" expliqué dans le diaporama

    \\ On définit un tableau d'entiers avec des valeurs non ordonnées
    monTableau = [12,39,46,10,6,32]

    \\ On affiche le tableau
    ÉCRIRE "Ordre initial du tableau :", monTableau

    \\ On tri le tableau avec l'algorithme de "tri rapide" en lui indiquant le tableau à trier, le début et la fin du tableau à trier, ainsi qu'un emplacement pour le pivot
    triRapide( monTableau, 1, 6, 6 )

    \\ On affiche le tableau
    ÉCRIRE "Ordre final du tableau :", monTableau
FIN
*/

/**
 * inverserValeur - Inverse deux valeurs
 * @param I/O mixed $a
 * @param I/O mixed $b
 * @return void
 */
function inverserValeur( &$a, &$b ) {
    $tmp = $a;
    $a = $b;
    $b = $tmp;
}

/**
 * triRapide - Ordonne par ordre croissant un tableau de nombres
 * @param I/O array $t
 * @param int $premier
 * @param int $dernier
 * @param int $pivot
 * @return void
 */
function triRapide( array &$t, int $premier, int $dernier, int $pivot ) {
    $gauche = $premier - 1;
    $droite = $dernier + 1;
    if( $pivot >= $premier && $pivot <= $dernier ) {
        inverserValeur( $t[$pivot], $t[$dernier] );
        while( $gauche < $droite ) {
            do {
                $gauche = $gauche + 1;
            } while( $t[$gauche] < $t[$dernier] );

            if( $gauche < $droite ) {
                do {
                    $droite = $droite - 1;
                } while( $t[$droite] > $t[$dernier] );

                if( $t[$gauche] >= $t[$droite] ) {
                    inverserValeur( $t[$gauche], $t[$droite] );
                }
            } else {
                inverserValeur( $t[$gauche], $t[$dernier] );
            }
        }

        triRapide( $t, $premier, $gauche-1, $gauche-1 );
        triRapide( $t, $droite+1, $dernier, $dernier );
    }
}

/*
Reproduire l'algorithme de "tri rapide" expliqué dans le diaporama
*/
$monTableau = [12,39,46,10,6,32];
echo "Ordre initial du tableau :\r\n";
print_r( $monTableau );
triRapide( $monTableau, 0, 5, 5 );
echo "Ordre final du tableau :\r\n";
print_r( $monTableau );