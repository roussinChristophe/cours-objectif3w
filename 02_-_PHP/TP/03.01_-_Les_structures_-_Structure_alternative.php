<?php
/*
DÉBUT
    \\ Saisir une valeur entière et afficher son double si cette
    \\ donnée est inférieure à un seuil donné.
    seuil = 20
    REQUÊTE "Veuillez saisir une valeur entière inférieure à 20 : ", val
    SI ( val * 2 ) < ( seuil * 2 ) ALORS
        ÉCRIRE "Le double de", val, "est", ( val * 2 )
    SINON
        ÉCRIRE "La valeur dépasse le seuil établi à", seuil
    FINSI
FIN
*/
$seuil = 20;
$val = readline( "Veuillez saisir une valeur entière inférieure à 20 : " );
if( ( $val * 2 ) < ( $seuil * 2 ) ) {
    echo "Le double de $val est " . ( $val * 2 );
} else {
    echo "La valeur dépasse le seuil établi à $seuil";
}