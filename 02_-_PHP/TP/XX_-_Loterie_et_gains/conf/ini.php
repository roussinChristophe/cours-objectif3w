<?php
define( 'INITIALWALLET', 100.00 );
define( 'COST', 2.00 );
define( 'MAXTICKET', 49 );
define( 'MAXWINNING', 6 );
define( 'PROFITS', array( 6=>20.00, 5=>15.00, 4=>8.00, 3=>4.00, 2=>2.00, 1=>0.00, 0=>0.00 ) );