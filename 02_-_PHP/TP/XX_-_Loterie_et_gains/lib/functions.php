<?php
/**
 * generateTickets Generates an interval containing a range of n values
 * @param integer $max [Optional] End of the sequence
 * @return array The interval containing a range of n tickets
 */
function generateTickets( int $max = 49 ) : array {
    return range( 1, $max ); // http://php.net/manual/en/function.range.php
}

/**
 * buyTicket Fills a cart with an element
 * @param integer $ticket
 * @param array $cart I/O
 * @return void
 */
function buyTicket( int $ticket, array &$cart ) : void {
    $cart[] = $ticket;
}

/**
 * availableTickets Returns the difference between generated and purchased tickets
 * @param array $generated
 * @param array $purchased
 * @return array The list of available tickets
 */
function availableTickets( array $generated, array $purchased ) : array {
    return array_diff( $generated, $purchased ); // http://php.net/manual/en/function.array-diff.php
}

/**
 * lottery Performs the lottery and returns the n winning tickets
 * @param integer $n
 * @param array $tickets
 * @return array The winning tickets
 */
function lottery( int $n, array $tickets ) : array {
    $winners = array();
    do {
        // /!\ Attention aux numéros déjà générés
        $key = mt_rand( 0, count($tickets)-1 );
        if( !in_array( $tickets[$key], $winners ) ) { // http://php.net/manual/en/function.mt-rand.php
            $winners[] = $tickets[$key];
        }
    } while( count( $winners ) < $n );

    return $winners;
}

/**
 * cashOut Award profits
 * @param integer $n
 * @param array $profits
 * @return float
 */
function cashOut( int $n, array $profits ) : float {
    return $profits[$n];
}