<?php
/**
 * 0 - Le joueur possède 100€ au début de l'application
 *     Chaque grille à remplir coûte 2€
 * 1 - Générer des tickets numérotés de 1 à 49
 * 2 - Demander 6 numéros parmis les tickets disponibles
 *     /!\ Attention aux numéros hors limite
 *     /!\ Attention aux numéros doublons
 * 3 - Générer 6 valeurs aléatoirement parmi les tickets
 *     /!\ Attention aux numéros déjà générés
 * 4 - Comparer chaque ticket demandé aux tickets générés pour en afficher les gagnants
 * 5 - Attribuer les gains suivant :
 *      6 numéros gagnants : 20€
 *      5 numéros gagnants : 15€
 *      4 numéros gagnants :  8€
 *      3 numéros gagnants :  4€
 *      2 numéros gagnants :  2€
 * 6 - Permettre de recommencer un nouveau tirage avec le portefeuille existant (pertes et gains) ou d'arrêter
**/



require( 'conf/ini.php' );
require( 'lib/functions.php' );

// 0 - Le joueur possède 100€ au début de l'application
$wallet = INITIALWALLET;
echo "Argent disponible initialement : $wallet\r\n\r\n";
if( readline( 'Voulez-vous jouer au loto pour la modique somme de ' . COST . ' € par partie (o/n) ? ' )==='o' ) {
    if( $wallet >= COST ) {
        do {
            //     Chaque grille à remplir coûte 2€
            $wallet -= COST;
            
            // 1 - Générer des tickets numérotés de 1 à 49
            $grid = generateTickets( MAXTICKET );
            echo 'Grille du loto : ' . implode( ', ', $grid ) . "\r\n\r\n"; // http://php.net/manual/en/function.implode.php

            //2 - Demander 6 numéros parmis les tickets disponibles
            $cart = array();
            do {
                $ticket = readline( 'Numéro à cocher sur la grille : ' );
                // /!\ Attention aux numéros hors limite
                // /!\ Attention aux numéros doublons
                if( in_array( $ticket, availableTickets( $grid, $cart ) ) ) { // http://php.net/manual/en/function.in-array.php
                    buyTicket( $ticket, $cart );
                } else {
                    echo "/!\\ Numéro non disponible !\r\nVeuillez choisir parmi : " . implode( ', ', availableTickets( $grid, $cart ) ) . "\r\n\r\n";
                }
            } while( count( $cart ) < MAXWINNING );

            sort( $cart ); // http://php.net/manual/en/function.sort.php
            echo "\r\nVotre sélection : " . implode( ', ', $cart ) . "\r\n";

            // 3 - Générer 6 valeurs aléatoirement parmi les tickets
            /* ----------
            Début jeu de tests
            ---------- */
            // $results = [1,2,3,4,5,6];
            // $results = [1,2,3,4,5,46];
            // $results = [1,2,3,4,45,46];
            // $results = [1,2,3,44,45,46];
            // $results = [1,2,43,44,45,46];
            // $results = [1,42,43,44,45,46];
            // $results = [41,42,43,44,45,46];
            /* ----------
            Fin jeu de tests
            ---------- */
            // $results = lottery( MAXWINNING, $grid );
            sort( $results );
            echo "\r\nLes numéros gagnants sont : " . implode( ', ', $results ) . "\r\n";

            // 4 - Comparer chaque ticket demandé aux tickets générés pour en afficher les gagnants
            switch( count( array_intersect( $results, $cart ) ) ) {
                case 0:
                    echo "Vous n'avez aucun numéro gagnant\r\n";
                    break;
                case 1:
                    echo 'Vous avez 1 numéro gagnant : ' . implode( ', ', array_intersect( $results, $cart ) ) . "\r\n"; // http://php.net/manual/en/function.array-intersect.php
                    break;
                default:
                    echo 'Vous avez ' . count( array_intersect( $results, $cart ) ) . ' numéros gagnants : ' . implode( ', ', array_intersect( $results, $cart ) ) . "\r\n"; // http://php.net/manual/en/function.array-intersect.php
            }

            // 5 - Attribuer les gains suivant :
            //      6 numéros gagnants : 20€
            //      5 numéros gagnants : 15€
            //      4 numéros gagnants :  8€
            //      3 numéros gagnants :  4€
            //      2 numéros gagnants :  2€
            $gains = cashOut( count( array_intersect( $results, $cart ) ), PROFITS );
            $wallet += $gains;
            if( $gains > 0 ) {
                echo "Avec un gain de $gains €, vous possédez maintenant dans votre porte-monnaie : $wallet €\r\n\r\n";
            } else {
                echo "Malheureusement, vous n'avez rien gagné. Il vous reste $wallet € dans votre porte-monnaie\r\n";
            }
        
            // 6 - Permettre de recommencer un nouveau tirage avec le portefeuille existant (pertes et gains) ou d'arrêter
            $response = readline( 'Voulez-vous rejouer pour ' . COST . ' € de plus (o/n) ? ' );
        } while( $wallet>=COST && $response==='o' );

        if( $response==='o' && $wallet<COST ) {
            echo "\r\nVous n'avez plus assez d'argent pour jouer ... on vous a bien eu !!!\r\n";
        }
    }
}
echo "\r\nMerci d'avoir joué avec nous";