<?php
/**
 * generateNumber Génère un nombre aléatoire compris entre les marges passées en paramètre si elles sont précisées, ou entre 0 et la plus grande valeur aléatoire possible.
 * @param int $min [optional]
 * @param int $max [optional]
 * @return int
**/
function generateNumber( int $min=NULL, int $max=NULL ) : int {
    if( is_int( $min ) && is_int( $max ) ) // (http://php.net/manual/fr/function.is-int.php)
        return mt_rand( $min, $max ); // Si des limites sont passées en paramètres et que ces limites sont numériques, alors on génère un nombre aléatoire compris dans ces marges.
    else
        return mt_rand(); // Sinon, on génère un nombre aléatoire dans les limites du système.
}

/**
 * compareNumber Compare deux nombres passés en paramètres et nous retourne le rapport entre eux : égalité, plus petit, plus grand.
 * @param int $val
 * @param int $ref
 * @return string
**/
function compareNumber( int $val, int $ref ) : string {
    if( $val < $ref )
        return 'trop petit !';
    elseif( $val > $ref )
        return 'trop grand !';
    else
        return 'trouvé';
}



session_start(); // On autorise la page à accéder à la superglobale de session (http://php.net/manual/fr/function.session-start.php).

if( isset( $_GET['destroy'] ) ) : // Si la clé "destroy" est passée en paramètre dans l'URL,
    unset( $_SESSION['minijeu'] ); // On détruit la clé de session pour vider l'historique (http://php.net/manual/fr/function.unset.php).
    $page = $_SERVER['PHP_SELF']; // On utilise la superglobale "$_SERVER" pour récupérer le nom de la page en cours d'utilisation (http://php.net/manual/fr/reserved.variables.server.php).
    header( 'Location:' . $page ); // On utilise la fonction "header" pour rediriger vers une autre page (http://php.net/manual/fr/function.header.php).
    exit;
endif;

if( !isset( $_SESSION['minijeu']['rand'] ) ) // Si la session n'existe pas OU que la clé "minijeu" n'a pas été déclarée OU que la clé "rand" n'a pas été générée,
    $_SESSION['minijeu']['rand'] = generateNumber(1, 32767); // On stocke un nombre aléatoire en session.

if( isset( $_POST['txt-search'] ) ) : // Si on a soumis des données via notre formulaire,
    if( ctype_digit( $_POST['txt-search'] ) ) : // Si la saisie est un entier, (http://php.net/manual/fr/function.ctype-digit.php)
        $result = compareNumber( intval( $_POST['txt-search'] ), $_SESSION['minijeu']['rand'] ); // On compare la saisie avec le nombre généré aléatoirement et stocké en session. // http://php.net/manual/fr/function.intval.php

        $_SESSION['minijeu']['historique'][] = array( // On stocke en session la saisie et le résultat de la comparaison.
            'saisie'    => $_POST['txt-search'],
            'comp'      => $result
        );
    else :
        $error = '<span class="block error">La saisie doit être un entier !</span>';
    endif;
endif;
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title>Mini jeu | Les sessions - Mise en pratique</title>

        <link rel="stylesheet" type="text/css" href="../../_assets/css/style.css">

        <!-- // highlight.js : Coloration syntaxique du code -->
        <link rel="stylesheet" type="text/css" href="../../_assets/plugins/highlight/styles/monokai_sublime.css">
        <script type="text/javascript" src="../../_assets/plugins/highlight/highlight.pack.js"></script>
        <script type="text/javascript">
            hljs.initHighlightingOnLoad();
        </script>
        <!-- // -->

        <style type="text/css">
            input[type="text"] {
                text-align:right;
                width:50px;
            }

            .error {
                background-color:red;
                color:white;
            }
            .warn { color:orange; }
            .ok { color:green; }

            .button {
                background-color:lightblue;
                color:white;
                display:inline-block;
                padding:10px 15px;
                text-align:center;
                text-decoration:none;
            }
        </style>
    </head>
    <body>
        <h1>Mini jeu | Les sessions - Mise en pratique</h1>
        <hr>
        <p>Un nombre <strong>entier naturel</strong> a été généré aléatoirement par la fonction PHP 'mt_rand' (mais limité à 32767 car cet algorithme, plus performant que 'rand', aurait donné une valeur trop élevée).<br>Saurez-vous le retrouver ?</p>
        <form action="" method="POST" name="frm-minijeu">
            <label for="txt-search">Saisissez une valeur :</label>
            <input<?php if( isset( $_SESSION['minijeu']['historique'] ) && $_SESSION['minijeu']['historique'][count($_SESSION['minijeu']['historique'])-1]['comp']=='trouvé' ) echo ' disabled="disabled"'; ?> id="txt-search" min="0" name="txt-search" type="text" value="<?php echo isset( $_POST['txt-search'] ) ? $_POST['txt-search'] : 0; ?>">

            <?php if( !( isset( $_SESSION['minijeu']['historique'] ) && $_SESSION['minijeu']['historique'][count($_SESSION['minijeu']['historique'])-1]['comp']=='trouvé' ) ) : ?>
            <input type="submit" value="Comparer">
            <?php endif; ?>
        </form>
        <blockquote><small><em>P.S. : le nombre maximum est limité à 32767 (même si techniquement, l'algorithme 'mt_rand' pourrait monter jusqu'à <?php echo mt_getrandmax(); ?>)</em></small></blockquote>

        <?php echo isset( $error ) ? $error : ''; ?>

        <?php if( isset( $_SESSION['minijeu']['historique'] ) ) : // Si la session existe, ?>
        <hr>
        <h2>Historique</h2>
        <?php
        foreach( $_SESSION['minijeu']['historique'] as $cle=>$tentative ) : // Pour chaque ligne de l'historique,
            echo '<span class="' . ( $tentative['comp']=='trouvé' ? 'ok' : 'warn' ) . '">Tentative n°' . ( $cle + 1 ) . ' : ' . $tentative['saisie'] . ' &rarr; ' . $tentative['comp'] . ( $tentative['comp']=='trouvé' ? ' en ' . count( $_SESSION['minijeu']['historique'] ) . ' coups !' : '' ) . '</span><br>'; // On affiche la ligne.
        endforeach;
        ?>
        <br>
        <a class="button" href="?destroy" title="">Nouvelle partie</a>
        <?php endif; ?>
        <pre><code class="php">
&lt;?php
/**
 * generateNumber Génère un nombre aléatoire compris entre les marges passées en paramètre si elles sont précisées, ou entre 0 et la plus grande valeur aléatoire possible.
 * @param int $min [optional]
 * @param int $max [optional]
 * @return int
**/
function generateNumber( int $min=NULL, int $max=NULL ) : int {
    if( is_int( $min ) && is_int( $max ) ) // (http://php.net/manual/fr/function.is-int.php)
        return mt_rand( $min, $max ); // Si des limites sont passées en paramètres et que ces limites sont numériques, alors on génère un nombre aléatoire compris dans ces marges.
    else
        return mt_rand(); // Sinon, on génère un nombre aléatoire dans les limites du système.
}

/**
 * compareNumber Compare deux nombres passés en paramètres et nous retourne le rapport entre eux : égalité, plus petit, plus grand.
 * @param int $val
 * @param int $ref
 * @return string
**/
function compareNumber( int $val, int $ref ) : string {
    if( $val &lt; $ref )
        return 'trop petit !';
    elseif( $val &gt; $ref )
        return 'trop grand !';
    else
        return 'trouvé';
}



session_start(); // On autorise la page à accéder à la superglobale de session (http://php.net/manual/fr/function.session-start.php).

if( isset( $_GET['destroy'] ) ) : // Si la clé "destroy" est passée en paramètre dans l'URL,
    unset( $_SESSION['minijeu'] ); // On détruit la clé de session pour vider l'historique (http://php.net/manual/fr/function.unset.php).
    $page = $_SERVER['PHP_SELF']; // On utilise la superglobale "$_SERVER" pour récupérer le nom de la page en cours d'utilisation (http://php.net/manual/fr/reserved.variables.server.php).
    header( 'Location:' . $page ); // On utilise la fonction "header" pour rediriger vers une autre page (http://php.net/manual/fr/function.header.php).
    exit;
endif;

if( !isset( $_SESSION['minijeu']['rand'] ) ) // Si la session n'existe pas OU que la clé "minijeu" n'a pas été déclarée OU que la clé "rand" n'a pas été générée,
    $_SESSION['minijeu']['rand'] = generateNumber(1, 32767); // On stocke un nombre aléatoire en session.

if( isset( $_POST['txt-search'] ) ) : // Si on a soumis des données via notre formulaire,
    if( ctype_digit( $_POST['txt-search'] ) ) : // Si la saisie est un entier, (http://php.net/manual/fr/function.ctype-digit.php)
        $result = compareNumber( intval( $_POST['txt-search'] ), $_SESSION['minijeu']['rand'] ); // On compare la saisie avec le nombre généré aléatoirement et stocké en session. // http://php.net/manual/fr/function.intval.php

        $_SESSION['minijeu']['historique'][] = array( // On stocke en session la saisie et le résultat de la comparaison.
            'saisie'    =&gt; $_POST['txt-search'],
            'comp'      =&gt; $result
        );
    else :
        $error = '&lt;span class="block error"&gt;La saisie doit être un entier !&lt;/span&gt;';
    endif;
endif;&lt;/code&gt;
<code class="html">
&lt;form action="" method="POST" name="frm-minijeu"&gt;
    &lt;label for="txt-search"&gt;Saisissez une valeur :&lt;/label&gt;
    &lt;input&lt;?php if( isset( $_SESSION['minijeu']['historique'] ) && $_SESSION['minijeu']['historique'][count($_SESSION['minijeu']['historique'])-1]['comp']=='trouvé' ) echo ' disabled="disabled"'; ?&gt; id="txt-search" min="0" name="txt-search" type="text" value="&lt;?php echo isset( $_POST['txt-search'] ) ? $_POST['txt-search'] : 0; ?&gt;"&gt;

    &lt;?php if( !( isset( $_SESSION['minijeu']['historique'] ) && $_SESSION['minijeu']['historique'][count($_SESSION['minijeu']['historique'])-1]['comp']=='trouvé' ) ) : ?&gt;
    &lt;input type="submit" value="Comparer"&gt;
    &lt;?php endif; ?&gt;
&lt;/form&gt;
&lt;blockquote&gt;&lt;small&gt;&lt;em&gt;P.S. : le nombre maximum est limité à 32767 (même si techniquement, l'algorithme 'mt_rand' pourrait monter jusqu'à &lt;?php echo mt_getrandmax(); ?&gt;)&lt;/em&gt;&lt;/small&gt;&lt;/blockquote&gt;

&lt;?php echo isset( $error ) ? $error : ''; ?&gt;

&lt;?php if( isset( $_SESSION['minijeu']['historique'] ) ) : // Si la session existe, ?&gt;
&lt;hr&gt;
&lt;h2&gt;Historique&lt;/h2&gt;
&lt;?php
foreach( $_SESSION['minijeu']['historique'] as $cle=&gt;$tentative ) : // Pour chaque ligne de l'historique,
    echo '&lt;span class="' . ( $tentative['comp']=='trouvé' ? 'ok' : 'warn' ) . '"&gt;Tentative n°' . ( $cle + 1 ) . ' : ' . $tentative['saisie'] . ' &rarr; ' . $tentative['comp'] . ( $tentative['comp']=='trouvé' ? ' en ' . count( $_SESSION['minijeu']['historique'] ) . ' coups !' : '' ) . '&lt;/span&gt;&lt;br&gt;'; // On affiche la ligne.
endforeach;
?&gt;
&lt;br&gt;
&lt;a class="button" href="?destroy" title=""&gt;Nouvelle partie&lt;/a&gt;
&lt;?php endif; ?&gt;&lt;/code&gt;&lt;/pre&gt;
    </body>
</html>