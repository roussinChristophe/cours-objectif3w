<?php
/*
DÉBUT
    \\ Écrire un algorithme qui demande deux nombres à
    \\ l'utilisateur et l'informe ensuite si leur produit est
    \\ négatif ou positif (on laisse de côté le cas où le produit
    \\ est nul).
    \\ Attention toutefois : on ne doit pas calculer le produit
    \\ des deux nombres.
    REQUÊTE "Veuillez saisir un nombre : ", nb1
    REQUÊTE "Veuillez saisir un nombre : ", nb2
    SI ( nb1 >= 0 ET nb2 >=0 ) OU ( nb1 < 0 ET nb2 < 0 ) ALORS
        ÉCRIRE "Le produit de", nb1, "et", nb2, "est positif"
    SINON
        ÉCRIRE "Le produit de", nb1, "et", nb2, "est négatif"
    FINSI
FIN
*/
$nb1 = readline( "Veuillez saisir un nombre : " );
$nb2 = readline( "Veuillez saisir un nombre : " );
if( ( $nb1 >= 0 && $nb2 >=0 ) || ( $nb1 < 0 && $nb2 < 0 ) ) :
    echo "Le produit de $nb1 et $nb2 est positif\r\n";
else :
    echo "Le produit de $nb1 et $nb2 est négatif\r\n";
endif;