<?php
/*
DÉBUT
    \\ Écrire un algorithme qui demande un nombre compris entre 10
    \\ et 20, jusqu'à ce que la réponse convienne.
    \\ En cas de réponse supérieure à 20, on fera apparaître un
    \\ message : "Plus petit !" , et inversement, "Plus grand !"
    \\ si le nombre est inférieur à 10.
    RÉPÉTER
        REQUÊTE "Veuillez saisir un nombre entre 10 et 20 : ", nombre
        SI nombre < 10 ALORS
            ÉCRIRE "Plus grand !"
        SINON
            SI nombre > 20 ALORS
                ÉCRIRE "Plus petit !"
            FINSI
        FINSI
    JUSQU'À nombre >= 10 ET nombre <=20
FIN
*/
do {
    $nombre = readline( "Veuillez saisir un nombre entre 10 et 20 : " );
    if( $nombre < 10 ) :
        echo "Plus grand !\r\n";
    elseif( $nombre > 20 ) :
        echo "Plus petit !\r\n";
    endif;
} while( $nombre < 10 || $nombre > 20 );