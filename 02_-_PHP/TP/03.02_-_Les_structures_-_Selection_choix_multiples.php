<?php
/*
DÉBUT
    \\ Reproduire avec l'instruction SELON l'équivalence de cette
    \\ structure alternative :
    \\     VARIABLES
    \\         abreviation: CHAINE DE CARACTERES
    \\     DÉBUT
    \\         {Préparation du traitement}
    \\         REQUÊTE "Civilité (Mme / Mlle / M / Autre) : ",
    \\ abreviati
    REQUÊTE "Civilité (Mme / Mlle / M / Autre) : ", abreviation
    SÉLECTIONNER abreviation
        "Mlle" :
            ÉCRIRE "Bonjour Mademoiselle"
        "Mme" :
            ÉCRIRE "Bonjour Madame"
        "M" :
            ÉCRIRE "Bonjour Monsieur"
        SINON
            ÉCRIRE "Bonjour inconnu"
    FINSÉLECTIONNER
FIN
*/
$abreviation = readline( "Civilité (Mme / Mlle / M / Autre) : " );
switch( $abreviation ) {
    case "Mlle" :
        echo "Bonjour Mademoiselle";
        break;
    case "Mme" :
        echo "Bonjour Madame";
        break;
    case "M" :
        echo "Bonjour Monsieur";
        break;
    default :
        echo "Bonjour inconnu";
}