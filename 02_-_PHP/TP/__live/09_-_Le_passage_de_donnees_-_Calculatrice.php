<?php
// if( isset( $_POST['operande1'] ) && isset( $_POST['operateur'] ) && isset( $_POST['operande2'] ) ) {
if( isset( $_POST['operande1'], $_POST['operateur'], $_POST['operande2'] ) ) {
    $_POST['operande1'] = str_replace( ',', '.', $_POST['operande1'] );
    $_POST['operande2'] = str_replace( ',', '.', $_POST['operande2'] );
    if( is_numeric( $_POST['operande1'] ) && is_numeric( $_POST['operande2'] ) ) {
        $calcul = $_POST['operande1'] . $_POST['operateur'] . $_POST['operande2'] . '=';
        switch( $_POST['operateur'] ) {
            case '+':
                $resultat = $_POST['operande1'] + $_POST['operande2'];
                $calcul = $calcul . $resultat;
                break;
            case '-':
                $resultat = $_POST['operande1'] - $_POST['operande2'];
                $calcul = $calcul . $resultat;
                break;
            case 'x':
                $resultat = $_POST['operande1'] * $_POST['operande2'];
                $calcul = $calcul . $resultat;
                break;
            case '/':
                if( $_POST['operande2']!=0 ) {
                    $resultat = $_POST['operande1'] / $_POST['operande2'];
                    $calcul = $calcul . $resultat;
                } else {
                    $calcul = '';
                    $erreur = 'La division par 0 étant impossible, merci de ne pas provoquer de trou noir';
                }
                break;
        }
    } else {
        $erreur = 'Veuillez indiquer des nombres';
    }
}
?>

<?php if( isset( $erreur ) ) echo '<div style="background-color:red;color:white;padding:7px;">' . $erreur . '</div>'; ?>
<form action="" method="POST">
    <input type="text" name="operande1" value="<?php if( isset( $_POST['operande1'] ) ) echo $_POST['operande1']; ?>">
    <select name="operateur">
        <option<?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='+' ) echo ' selected'; ?>>+</option>
        <option<?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='-' ) echo ' selected'; ?>>-</option>
        <option<?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='x' ) echo ' selected'; ?>>x</option>
        <option<?php if( isset( $_POST['operateur'] ) && $_POST['operateur']=='/' ) echo ' selected'; ?>>/</option>
    </select>
    <input type="text" name="operande2" value="<?php if( isset( $_POST['operande2'] ) ) echo $_POST['operande2']; ?>">
    <input type="submit" value="="><?php if( isset( $resultat ) ) echo str_replace( '.', ',', $resultat ); ?>
    <br>
    <textarea name="historique"><?php if( isset( $calcul ) ) echo $calcul . PHP_EOL; ?><?php if( isset( $_POST['historique'] ) ) echo $_POST['historique']; ?></textarea>
</form>
