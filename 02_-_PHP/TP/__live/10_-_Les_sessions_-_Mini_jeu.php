<?php
session_start();

if( isset( $_GET['destroy'] ) ) {
    unset( $_SESSION['minijeu'] );
    header( 'Location: 09_-_Les_sessions_-_Mini_jeu.php' );
    exit;
}

if( !isset( $_SESSION['minijeu']['rand'] ) ) {
    $_SESSION['minijeu']['rand'] = mt_rand(1, 40000);
    $_SESSION['minijeu']['hist'] = array();
}

var_dump( $_SESSION['minijeu']['rand'] );

if( isset( $_POST['choice'] ) ) {
    if( $_POST['choice']==="0" || ctype_digit( $_POST['choice'] ) ) {
        if( $_POST['choice'] > $_SESSION['minijeu']['rand'] ) {
            $result = "Trop grand";
        } elseif( $_POST['choice'] < $_SESSION['minijeu']['rand'] ) {
            $result = "Trop petit";
        } else {
            $result = "Trouvé !!!";
        }

        $_SESSION['minijeu']['hist'][] = $_POST['choice'] . ' : ' . $result;
    } else {
        $error = "Entier naturel svp";
    }
}
?>
<a href="?destroy" title="">Nouvelle partie</a>
<form method="POST">
    <?php
    if( !isset( $result ) || $result != "Trouvé !!!" ) {
    ?>
    <input type="text" name="choice">
    <input type="submit" value="Va chercher">
    <?php
    }
    ?>
</form>
<?php
if( isset( $_SESSION['minijeu']['hist'] ) ) {
    // foreach( $_SESSION['minijeu']['hist'] as $hist ) {
    for( $i = count( $_SESSION['minijeu']['hist'] )-1; $i >=0; $i-- ) {
        echo $_SESSION['minijeu']['hist'][$i] . '<br>';
    }
}
if( isset( $error ) ) {
    echo $error;
}