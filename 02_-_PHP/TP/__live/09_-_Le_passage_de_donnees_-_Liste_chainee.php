<?php
// function genWord( int $indice, array $datas ) : string {
//     $word = $datas[$indice]['lettre'];
//     $next = $datas[$indice]['suivant'];

//     while( $next!=-1 ) {
//         $word = $word . $datas[$next]['lettre'];
//         $next = $datas[$next]['suivant'];
//     }

//     return $word;
// }

function genWord( $indice, array $datas ) : string {
    $word = '';

    do {
        $word = $word . $datas[$indice]['lettre'];
        $indice = $datas[$indice]['suivant'];
    } while( $indice!=-1 );

    return $word;
}

$donnees = array(
    array('lettre' => 'a', 'suivant' => 10),
    array('lettre' => 'e', 'suivant' => -1),
    array('lettre' => 'e', 'suivant' => 6),
    array('lettre' => 'l', 'suivant' => 1),
    array('lettre' => 'p', 'suivant' => 8),
    array('lettre' => 'o', 'suivant' => 11),
    array('lettre' => 'x', 'suivant' => 12),
    array('lettre' => 'p', 'suivant' => 3),
    array('lettre' => 'r', 'suivant' => 5),
    array('lettre' => 'm', 'suivant' => 7),
    array('lettre' => 'b', 'suivant' => 3),
    array('lettre' => 'b', 'suivant' => 0),
    array('lettre' => 'a', 'suivant' => 9)
);
?>
<form action="" method="POST">
    <input type="text" name="choice" placeholder="Saisir un entier entre 0 et <?php echo count( $donnees )-1; ?>" style="width:250px;">
    <input type="submit" value="Choisir">
</form>
<?php
if( isset( $_POST['choice'] ) ) {
    if( ctype_digit( $_POST['choice'] ) ) {
        if( $_POST['choice'] >=0 && $_POST['choice'] < count( $donnees ) ) {
            echo "Le mot est : " . genWord( $_POST['choice'], $donnees );
        } else {
            echo 'Les bornes bordel !!!';
        }
    } else {
        echo "Numérique bordel !!!";
    }
}