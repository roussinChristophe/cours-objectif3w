<?php
session_start();

/**
 * aarray_search
 * @param string $needle
 * @param array $haystack
 * @return mixed (int | false)
 */
function aarray_search( $needle, array $haystack ) {
    foreach( $haystack as $key => $value ) {
        if( $value['name']==$needle ) {
            return $key;
        }
    }

    return false;
}

// if( isset( $_POST ) && count( $_POST )>0 ) {
//     var_dump( $_POST );
// }

// if( isset( $_POST['article'] ) && isset( $_POST['quantity'] ) ) {
if( isset( $_POST['article'], $_POST['quantity'], $_POST['add'] ) ) {
    if( is_numeric( $_POST['quantity'] ) ) {
        if( isset( $_SESSION['cart'] ) && ( $key = aarray_search( $_POST['article'], $_SESSION['cart'] ) )!==false ) {
            if( $_SESSION['cart'][$key]['qty'] + $_POST['quantity'] <= 0 ) {
                unset( $_SESSION['cart'][$key] );
            } else {
                $_SESSION['cart'][$key]['qty'] += $_POST['quantity']; // $_SESSION['cart'][$key]['qty'] = $_SESSION['cart'][$key]['qty'] + $_POST['quantity'];
            }
        } else {
            $_SESSION['cart'][] = array(
                'name' => $_POST['article'],
                'qty' => $_POST['quantity']
            );
        }
    } else {
        echo 'Mauvaise saisie ! Veuillez indiquer une quantité numérique';
    }
}

if( isset( $_POST['del'], $_SESSION['cart'] ) && count( $_POST )>0 ) {
    foreach( $_POST as $keyCart => $checked ) {
        unset( $_SESSION['cart'][$keyCart] );
    }
}
?>
<form method="post">
    <input name="article" placeholder="Saisissez un produit" type="text">
    <input name="quantity" placeholder="Saisissez une quantité" type="text">
    <input name="add" type="submit">
</form>
<?php
if( !empty( $_SESSION['cart'] ) ) {
?>
<form method="post">
    <table border="1">
        <thead>
            <tr>
                <th>Article</th>
                <th>Quantité</th>
                <td></td>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="3"><input name="del" type="submit" value="Supprimer"></td>
            </tr>
        </tfoot>
        <tbody>
            <?php
            // var_dump( $_SESSION['cart'] );
            foreach( $_SESSION['cart'] as $key => $article ) {
                // var_dump( $key );
                // var_dump( $article );
                echo '<tr><td>' . $article['name'] . '</td><td>' . $article['qty'] . '</td><td><input name="' . $key . '" type="checkbox"></td></tr>';
            }
        ?>
        </tbody>
    </table>
</form>
<?php
    // var_dump($_SESSION['cart']);
}