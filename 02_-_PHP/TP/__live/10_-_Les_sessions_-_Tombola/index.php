<?php
session_start();

define( 'INITIALWALLET', 500 );
define( 'TICKETPRICE', 2 );
define( 'MAXTICKETS', 100 );
define( 'JACKPOT', array( 100, 50, 20 ) );

if( isset( $_GET['ini'] ) ) {
    unset( $_SESSION['tombola'] );
    header( 'Location:.' );
    exit;
}

if( !isset( $_SESSION['tombola'] ) ) {
    $_SESSION['tombola']['wallet'] = INITIALWALLET;
}
if( !isset( $_SESSION['tombola']['ticketsToBuy'] ) ) {
    $_SESSION['tombola']['ticketsToBuy'] = range( 1, MAXTICKETS );
    $_SESSION['tombola']['pocket'] = array();
}

if( isset( $_GET['tirage'] ) ) {
    if( isset( $_SESSION['tombola']['pocket'] ) && count( $_SESSION['tombola']['pocket'] )>0 ) {
        $ticketsTirage = range( 1, MAXTICKETS );
        // // $winnersKeys = array_rand( $ticketsTirage, count(JACKPOT) );
        // // if( !is_array($winnersKeys) ) {
        // //     $winnersKeys = array( $winnersKeys );
        // // }
        $winnersKeys = (array)array_rand( $ticketsTirage, count(JACKPOT) );

        // foreach( $winnersKeys as $winnerKey ) {
        //     $winners[] = $ticketsTirage[$winnerKey];
        // }
        // shuffle( $winners );
        // foreach( $winners as $key=>$winner ) {
        //     $gains[$winner] = JACKPOT[$key];
        // }
        
        // $_SESSION['tombola']['gains'] = array();
        // foreach( $gains as $ticketNumber=>$gain ) {
        //     if( in_array( $ticketNumber, $_SESSION['tombola']['pocket'] ) ) {
        //         $_SESSION['tombola']['gains'][] = $gain;
        //         $_SESSION['tombola']['wallet'] += $gain; // $_SESSION['tombola']['wallet'] = $_SESSION['tombola']['wallet'] + $gain;
        //     }
        // }
        foreach( $winnersKeys as $winnerKey ) {
            $winners[] = $ticketsTirage[$winnerKey];
        }
        shuffle( $winners );
        $_SESSION['tombola']['gains'] = array();
        foreach( $winners as $key=>$winner ) {
            if( in_array( $winner, $_SESSION['tombola']['pocket'] ) ) {
                $_SESSION['tombola']['gains'][] = JACKPOT[$key];
                $_SESSION['tombola']['wallet'] += JACKPOT[$key]; // $_SESSION['tombola']['wallet'] = $_SESSION['tombola']['wallet'] + JACKPOT[$key];
            }
        }

        // unset( $_SESSION['tombola']['ticketsToBuy'] );
        // unset( $_SESSION['tombola']['pocket'] );
        unset( $_SESSION['tombola']['ticketsToBuy'], $_SESSION['tombola']['pocket'] );
    } else {
        $_SESSION['tombola']['_err'] = "Trucheur !!!";
    }
    header( 'Location:.' );
    exit;
}

if( isset( $_SESSION['tombola']['gains'] ) ) {
    if( count( $_SESSION['tombola']['gains'] )>0 ) {
        foreach( $_SESSION['tombola']['gains'] as $gain ) {
            echo "Vous avez gagné $gain<br>";
        }
    } else {
        echo "Vous n'avez rien gagné :(";
    }
    unset( $_SESSION['tombola']['gains'] );
}

if( isset( $_POST['number'] ) ) {
    if( ctype_digit( $_POST['number'] ) ) {

        if( $_POST['number']<=count($_SESSION['tombola']['ticketsToBuy']) ) {
            $maxTicketToBuy = $_POST['number'];
        } else {
            $maxTicketToBuy = count($_SESSION['tombola']['ticketsToBuy']);
            echo 'Pas assez de tickets disponibles à la vente. Vous n\'avez pu en acheter que ' . $maxTicketToBuy . '<br>';
        }

        if( ( $maxTicketToBuy * TICKETPRICE ) > $_SESSION['tombola']['wallet'] ) {
            $maxTicketToBuy = floor( $_SESSION['tombola']['wallet'] / TICKETPRICE );
            echo 'Pas assez d\'argent. Vous n\'avez pu en acheter que ' . $maxTicketToBuy . '<br>';
        }

        if( $maxTicketToBuy>0 ) {
            // $bought = array_rand( $_SESSION['tombola']['ticketsToBuy'], $maxTicketToBuy );
            // if( is_array( $bought ) ) {
            //     foreach( $bought as $ticketKey ) {
            //         $_SESSION['tombola']['pocket'][] = $_SESSION['tombola']['ticketsToBuy'][$ticketKey];
            //         unset($_SESSION['tombola']['ticketsToBuy'][$ticketKey]);
            //     }
            // } else {
            //     $_SESSION['tombola']['pocket'][] = $_SESSION['tombola']['ticketsToBuy'][$bought];
            //     unset($_SESSION['tombola']['ticketsToBuy'][$bought]);
            // }
            
            $bought = (array)array_rand( $_SESSION['tombola']['ticketsToBuy'], $maxTicketToBuy );
            foreach( $bought as $ticketKey ) {
                $_SESSION['tombola']['pocket'][] = $_SESSION['tombola']['ticketsToBuy'][$ticketKey];
                unset($_SESSION['tombola']['ticketsToBuy'][$ticketKey]);
            }
            $_SESSION['tombola']['wallet'] -= ( $maxTicketToBuy * TICKETPRICE ); // $_SESSION['tombola']['wallet'] = $_SESSION['tombola']['wallet'] - ( $maxTicketToBuy * TICKETPRICE );
        }
    } else {
        echo "Votre saisie " . htmlentities( $_POST['number'] ) . " doit être numérique uniquement !<br>";
    }
}

if( isset( $_SESSION['tombola']['_err'] ) ) {
    echo $_SESSION['tombola']['_err'] . "<br>";
    unset( $_SESSION['tombola']['_err'] );
}

if( isset( $_SESSION['tombola']['wallet'] ) ) {
    echo "Vous possédez : " . $_SESSION['tombola']['wallet'] . "€";
}
?>
<form action="." method="post">
    <input type="text" name="number" placeholder="Nombre de ticket(s)">
    <input type="submit" value="Acheter">
</form>
<?php
if( isset( $_SESSION['tombola']['pocket'] ) && count( $_SESSION['tombola']['pocket'] )>0 ) {
?>
<a href=".?tirage" title="Tirage au sort">Tirage au sort</a>
<br>
<?php
}
?>
<a href=".?ini" title="Réinitialiser tout">Réinitialiser tout</a>
<?php
if( isset( $_SESSION['tombola']['pocket'] ) ) {
    sort($_SESSION['tombola']['pocket']);
    foreach( $_SESSION['tombola']['pocket'] as $ticket ) {
        echo '| ' . $ticket . ' ';
    }
}