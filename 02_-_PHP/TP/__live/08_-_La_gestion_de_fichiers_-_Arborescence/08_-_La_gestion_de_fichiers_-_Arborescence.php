<?php
function is_indexed( $dir ) {
    foreach( scandir( $dir ) as $entry ) {
        // $fileinfo = pathinfo( $dir . $entry );
        // if( is_file( $dir . $entry ) && strtolower( $fileinfo['filename'] )=='index' ) {
        if( is_file( $dir . $entry ) && strtolower( pathinfo( $dir . $entry )['filename'] )=='index' ) {
            return true;
        }
    }

    return false;
}


function filetree( string $dir, int $iteration = 1 ) {
    $indent = '';
    for( $i=1; $i<$iteration; $i++ ) {
        $indent = $indent . '    ';
    }

    if( is_dir( $dir ) ) {

        if( !is_indexed( $dir ) ) {
            foreach( scandir( $dir ) as $entry ) {
                if( $entry!='.' && $entry!='..' ) {
                    // echo $dir . $entry . "\r\n";
                    echo $indent . $entry . "\r\n";
                    $newdir = $dir . $entry . DIRECTORY_SEPARATOR;
                    if( is_dir( $newdir ) ) {
                        filetree( $newdir, $iteration + 1 );
                    }
                }
            }
        } else {
            echo "$indent Indexation protégée\r\n";
        }

        // $ressource = opendir( $dir );
        // if( $ressource !== false ) {
        //     $dir = readdir( $ressource );
        //     while( $dir!==false ) {
        //         if( $dir!='.' && $dir!='..' ) {
        //             echo $dir . "\r\n";
        //         }
        //         $dir = readdir( $ressource );
        //     }

        //     closedir( $ressource );
        // }
        // if( ( $ressource = opendir( $dir ) )!==false ) {
        //     while( ( $dir = readdir( $ressource ) )!==false ) {
        //         if( $dir!='.' && $dir!='..' ) {
        //             echo $dir . "\r\n";
        //         }
        //     }

        //     closedir( $ressource );
        // }
    }
}

$source = __DIR__ . DIRECTORY_SEPARATOR . 'arborescence_test' . DIRECTORY_SEPARATOR;
filetree( $source );