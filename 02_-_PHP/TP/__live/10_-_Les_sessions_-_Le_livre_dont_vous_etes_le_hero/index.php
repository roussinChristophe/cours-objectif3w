<?php
session_start();
if( isset( $_GET['reset'] ) ) {
    unset( $_SESSION['story'] );
    header( 'Location: .');
    exit;
}
require( '_db/story.php' );
/**
 * genChapter Affiche le chapitre n de l'histoire
 *
 * @param integer $n
 * @param array $story
 * @return mixed (string|false)
 */
function genChapter( int $n, array $story ) {
    if( isset( $story[$n]['text'] ) ) {
        return $story[$n]['text'];
    }

    return false;
}

/**
 * genChoices Affiche les choix du chapitre n de l'histoire
 *
 * @param integer $n
 * @param array $story
 * @return mixed (string|false)
 */
function genChoices( int $n, array $story ) {
    $result = '';
    if( isset( $story[$n]['choice'] ) ) {
        if( count( $story[$n]['choice'] ) > 0 ) {
            $result .= '<form method="post">'; // $result = $result . '<form method="post">';

            // $result .= '<form method="post"><select name="choice">'; // $result = $result . '<form method="post"><select name="choice">';
            // foreach( $story[$n]['choice'] as $choice ) {
            //     $result .= '<option value="' . $choice['goto'] . '">' . $choice['text'] . '</option>';
            // }
            // $result .= '</select><input type="submit" value="Aller au chapitre suivant">';

            foreach( $story[$n]['choice'] as $choice ) {
                $result .= '<button name="choice" type="submit" value="' . $choice['goto'] . '">' . $choice['text'] . '</button>';
            }

            $result .= '</form>';
        }
    }
    
    return $result;
}

function play( int $n, array $story ) {
    return genChapter( $n, $story ) . genChoices( $n, $story );
}

/**
 * Undocumented function
 *
 * @param [type] $choice
 * @param [type] $story
 * @param [type] $hist
 * @return boolean
 */
function verifChoice( $yourchoice, $story, $hist ) {
    $last = count( $hist ) - 1;
    $lastKey = $hist[$last];
    $lastChapter = $story[$lastKey];
    foreach( $lastChapter['choice'] as $choice ) {
        if( $choice['goto'] == $yourchoice ) {
            return true;
        }
    }

    return false;
}
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title>Le livre dont vous êtes le héro | Les sessions - Mise en pratique</title>
    </head>
    <body>
        <h1>Le livre dont vous êtes le héro | Les sessions - Mise en pratique</h1>
        <p><em>Le livre dont vous êtes le héro est un concept bien connu dans lequel il existe plusieurs points d'arrêt où un choix vous est proposé. Ce choix influence la suite de votre parcours dans l'histoire.</em></p>
        <p><em>Dans cet exercice, le fichier <a href="_db/story.php" title="Morceaux de l'hitoire">story.php</a> contenant les différents morceaux de l'histoire vous est mis à disposition.<br />Il vous est demandé :</em></p>
        <ol style="font-style:italic;">
            <li>de créer une fonction pour afficher le chapitre n</li>
            <li>mettre en place un formulaire proposant les choix possibles à chaque décision à prendre</li>
            <li>faire en sorte d'ajouter une persistance des données pour ne pas perdre le cours de l'histoire</li>
        </ol>
        <hr>
        <?php
        if( !isset( $_SESSION['story'] ) ) {
            $_SESSION['story']['chapters'][] = 0;
        }
        $mavar = $_SESSION['story']['chapters'];

        if( isset( $_POST['choice'] ) ) {
            if( $_POST['choice']==0 ) {
                unset( $_SESSION['story'] );
            }

            if( !isset( $_SESSION['story']['chapters'] ) || verifChoice( $_POST['choice'], $story, $_SESSION['story']['chapters'] ) ) {
                $_SESSION['story']['chapters'][] = $_POST['choice'];
            }
        }

        $last = count( $_SESSION['story']['chapters'] ) - 1;
        foreach( $_SESSION['story']['chapters'] as $key=>$chapter ) {
            if( $key !== $last ) {
                echo genChapter( $chapter, $story ) . "<br><br>";
            }
        }
        
        echo play( $_SESSION['story']['chapters'][$last], $story );
        ?>
    </body>
</html>