<?php
/*
Nous allons écrire un discours automatique de politicien.
Notre programme doit sélectionner plusieurs morceaux de phrase aléatoirement et les compose pour former 5 phrases.
Les différents morceaux de phrase utilisables sont regroupés dans le fichier "data.txt". Chaque morceau est sur une ligne. Chaque groupe est séparé par une ligne vide.
Les phrases seront créées à partir d'un morceau du premier groupe, puis un morceau du second groupe, puis un du troisième, et enfin un du quatrième ... le tout aléatoirement (cas spécial : la première phrase commencera toujours par le premier morceau du premier groupe).
Un bon politicien ne se répète pas. Aussi, le programme ne pourra pas utiliser le même morceau de phrase deux fois.
Votre programme ne doit pas non plus modifier le fichier texte et doit prendre en considération le fait que l'on puisse ajouter d'autres morceaux de phrases.
*/

$file = 'data.txt';
$ressource = fopen( $file, 'r' );
$lines = array();
$i = 0;
while( !feof( $ressource ) ) {
    $line = fgets( $ressource );
    
    if( $line == "\r\n" ) {
        $i++;
    } else {
        $lines[$i][] = str_replace( "\r\n", '', $line );
    }
}
fclose( $ressource );

echo $lines[0][0];
unset( $lines[0][0] );


$rand = array_rand( $lines[1], 1 );
echo ' ' . $lines[1][$rand];
unset( $lines[1][$rand] );

$rand = array_rand( $lines[2], 1 );
echo ' ' . $lines[2][$rand];
unset( $lines[2][$rand] );

$rand = array_rand( $lines[3], 1 );
echo ' ' . $lines[3][$rand] . "\r\n";
unset( $lines[3][$rand] );

$nbSentences = count( $lines[0] );
for( $i = 1; $i <= $nbSentences; $i++ ) {
    $rand = array_rand( $lines[0], 1 );
    echo $lines[0][$rand];
    unset( $lines[0][$rand] );

    $rand = array_rand( $lines[1], 1 );
    echo ' ' . $lines[1][$rand];
    unset( $lines[1][$rand] );

    $rand = array_rand( $lines[2], 1 );
    echo ' ' . $lines[2][$rand];
    unset( $lines[2][$rand] );

    $rand = array_rand( $lines[3], 1 );
    echo ' ' . $lines[3][$rand] . "\r\n\r\n";
    unset( $lines[3][$rand] );
}