<?php
/*
DÉBUT
    \\ En fonction d'un nombre d'itérations saisi, faire la somme
    \\ des entiers saisis et afficher le résultat de l'opération.
    cumul = 0
    REQUÊTE "Veuillez indiquer le nombre de valeurs à saisir : ", iteration
    POUR cpt = 1 JUSQU'À iteration INCRÉMENT 1 FAIRE
        REQUÊTE "Saisir une valeur : ", val
        cumul = cumul + val
    FINPOUR
    ÉCRIRE "Le total des", iteration, "valeurs saisies est :", cumul
FIN
*/
$cumul = 0;
$iteration = readline( "Veuillez indiquer le nombre de valeurs à saisir : " );
for( $cpt = 1; $cpt <= $iteration; $cpt++ ) {
    $val = readline( "Saisir une valeur : " );
    $cumul += $val; // $cumul = $cumul + $val;
}
echo "Le total des $iteration valeurs saisies est : $cumul";