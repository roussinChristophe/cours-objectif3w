<?php
/**
 * PCRE
 */
// if(preg_match('Notre REGEXP', 'Ce dans quoi vous cherchez')) {
//   echo 'Le mot que vous cherchez se trouve dans la chaine';
// } else {
//   echo 'Le texte ne se trouve pas dans la chaine de caractères';
// }

echo '---<br>// Délimiteurs :<br>';
if(preg_match('#guitare#', 'John aime jouer de la guitare')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}


echo '---<br>// OU : le symbole |<br>';
if(preg_match('#guitare#', 'John aime jouer de la guitare') || preg_match('#pipo#', 'John aime jouer de la guitare')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

if(preg_match('#guitare|pipo|contrebasse#', 'John aime jouer de la contrebasse')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}


echo '---<br>// Début et fin de chaîne de caractères : les symboles ^<br>';
if(preg_match('#^Timothy & John#', 'Timothy & John aiment jouer du pipo ensemble')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}
echo 'et $<br>';
if(preg_match('#pipo ensemble$#', 'Timothy & John aiment jouer du pipo ensemble')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '--<br>//Les classes de caractères : le symbole []<br>';
if(preg_match('#gr[aoi]s#', 'JB aime le gris')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '--<br>// Les intervalles : le symbole -<br>';
if(preg_match('#gr[a-zA-Z]s#', 'JB aime le grIs')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

if(preg_match('#gr[a-zA-Z0-1]s#', 'JB aime le gr1s')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

if(preg_match('#gr[a-zA-Z0-1-]s#', 'JB aime le gr-s')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '--<br>// L\'échappement de caractère : le symbole \<br>';
if(preg_match('#gr[a-zA-Z0-9\]-]s#', 'JB aime le gr]s')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '---<br>// La négation :<br>';
if(!preg_match('#guitare#', 'John aime jouer de la guitare')) {
  echo 'Pas du tout<br>';
} else {
  echo 'Effectivement !!!<br>';
}

if(preg_match('#gr[^&@,;-]s#', 'JB aime le gres')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

if(preg_match('#gr[^0-9A-Z]s#', 'JB aime le gras')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

if(preg_match('#^[^aeiouy]#', 'JB aime le gras')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '---<br>// Les quantifieurs :<br>';
echo '---<br>// Facultatif : le symbole ? représente 0 ou 1 fois<br>';
if(preg_match('#gr[aeiouy][aeiouy]s?$#', 'JB aime les grues')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}
echo '---<br>// Obligatoire : le symbole + représente 1 ou plusieurs fois<br>';
if(preg_match('#gr[aeiouy]+s$#', 'JB aime les gruuuuuuuues')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}
echo '---<br>// Facultatif ou répétitif : le symbole * représente 0 ou plusieurs fois<br>';
if(preg_match('#gr[aeiouy]*s$#', 'JB aime les gris')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '---<br>// <br>';
if(preg_match('#gr[aeiouy]{2}s#', 'JB aime les grues')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}
if(preg_match('#gr[aeiouy]{2,3}s#', 'JB aime les gruees')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}
if(preg_match('#gr[aeiouy]{2,}s#', 'JB aime les gruuees')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// Le regroupement : ()<br>';
if(preg_match('#Bla(bl[aeiuoy])+$#', 'Blablobliblu')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '---<br>// Les metacaractères<br>';
echo 'Mon délimiteur, ^$()[]{}?+*\|!.<br>';

if(preg_match('#\(#', 'Mon (texte')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

if(preg_match('#[\#;i]#', 'Mon #texte')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '---<br>// Les classes abrégées<br>';
echo '// \d pour [0-9]<br>';
if(preg_match('#^Mon nombre est : \d$#', 'Mon nombre est : 5')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// \D pour [^0-9]<br>';
if(preg_match('#^Ma lettre est : \D$#', 'Ma lettre est : r')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// \w pour [a-zA-Z0-9_]<br>';
if(preg_match('#^Mon caractère est : \w$#', 'Mon caractère est : Y')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// \W pour [^a-zA-Z0-9_]<br>';
if(preg_match('#^Mon caractère est : \W$#', 'Mon caractère est : +')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// \t pour tabulation<br>';
if(preg_match('#^\t-$#', '	-')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// \n pour nouvelle ligne<br>';
if(preg_match('#^\n-$#', '
-')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// \r pour retour chariot<br>';
if(preg_match('#^\r\n-$#', '
-')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// \s pour espace blanc (\r \n \t espace)<br>';
if(preg_match('#^\s-$#', ' -')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// \S pour non espace blanc (\r \n \t espace)<br>';
if(preg_match('#^\S-$#', 'a-')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// . pour n\'importe quel caractère sauf \n<br>';
if(preg_match('#^.-$#', '
-')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}

echo '// . pour n\'importe quel caractère avec \n<br>';
if(preg_match('#^.-$#si', '
-')) {
  echo 'Effectivement !!!<br>';
} else {
  echo 'Pas du tout<br>';
}