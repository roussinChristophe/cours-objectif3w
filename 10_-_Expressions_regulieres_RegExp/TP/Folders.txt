Version hors facultatif
^(([a-zA-Z]:|[\\\/])?[\\\/])?((\.{1,2}|\.?[^.\\\/:?"'<>|*&]*[^\\\/:?"'<>|*&][^.\\\/:?"'<>|*&]+)[\\\/])*[^\\\/:?"'<>|*&]*\.[a-zA-Z\d]+$

Version avec facultatif
^(([a-zA-Z]:|[\\\/])?[\\\/])?((\.{1,2}|\.?[^.\\\/:?"'<>|*&]*([^\\\/:?"'<>|*&][^.\\\/:?"'<>|*&]+)+)[\\\/])*[^\\\/:?"'<>|*&]*\.[a-zA-Z\d]+$